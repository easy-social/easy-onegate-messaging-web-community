"use strict";

let config    		= require("../config");
let logger    		= require("../core/logger");
let C 				= require("../core/constants");
let fs 				= require("fs");
let path 			= require("path");

// let _ 				= require("lodash");
let crypto 			= require("crypto");
let bcrypt 			= require("bcrypt-nodejs");

let db	    		= require("../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../libs/hashids")("users");
let autoIncrement 	= require("mongoose-auto-increment");

let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	},
	strict: false
};
let ObjectId 	 = Schema.ObjectId;
let validateLocalStrategyProperty = function(property) {
	return (this.provider !== "local" && !this.updated) || property.length;
};

let validateLocalStrategyPassword = function(password) {
	return this.provider !== "local" || (password && password.length >= 6);
};

let UserSchema = new Schema({
	fullName: {
		type: String,
		trim: true,
		"default": "",
		validate: [validateLocalStrategyProperty, "Please fill in your full name"]
	},
	email: {
		type: String,
		trim: true,
		unique: true,
		index: true,
		lowercase: true,
		"default": "",
		validate: [validateLocalStrategyProperty, "Please fill in your email"],
		match: [/.+\@.+\..+/, "Please fill a valid email address"]
	},
	username: {
		type: String,
		index: {unique: true},
		// unique: true,
		// index: true,
		lowercase: true,
		required: "Please fill in a username",
		trim: true,
		match: [/^[\w][\w\-\._\@]*[\w]$/, "Please fill a valid username"]
	},
	password: {
		type: String,
		"default": "",
		validate: [validateLocalStrategyPassword, "Password should be longer"]
	},
	account : {type: ObjectId, ref: "Account"},
	login_attempts: { type: Number, required: true, default: 0 },
	lock_until: { type: Number },
	verified:{type:Boolean, default: false},
	first_name: { type: String},
	// accessToken: { type: String},
	last_name: { type: String},
	title: { type: String},
    //avatar: { data: Buffer, contentType: String },
	stats :{
    	created_at : { type: Date},
    	updated_at: { type: Date},
    	created_by : { type: String },
    	updated_by: { type: String },
    	deleted: {type:Boolean, default: false}
	},
	passwordLess: {
		type: Boolean,
		default: false
	},
	passwordLessToken: {
		type: String
	},
	provider: {
		type: String,
		"default": "local"
	},
	fb_pageIds:[{
		type: String
	}
	],
	fb_pages_tasks:{

	},
	page_count: {
		type: Number,
		default: 0
	},
	limited_page :{
		type: Number,
		default: -1
	},
	account_type:{
		type: String
	},
	profile: {
		name: { type: String },
		gender: { type: String },
		picture: { type: String },
		location: { type: String },
		access_token: { type: String },
		expires: { type: String },
		fbPages: [{
			//field: 'fb_id',
			type: String,
			//ref: "Page"
		}],
		accessPages : [{
			///field: 'fb_id',
			type: String,
			//ref: "Page"
		}],
		//fbPages: {type: Array},
	},
	socialLinks: {
		facebook: { type: String, unique: true, sparse: true },
		twitter: { type: String, unique: true, sparse: true },
		google: { type: String, unique: true, sparse: true },
		github: { type: String, unique: true, sparse: true },
	},
	roles: {
		type: [
			{
				type: String,
				"enum": [
					C.ROLE_ADMIN,
					C.ROLE_USER,
					C.ROLE_GUEST
				]
			}
		],
		"default": [C.ROLE_USER]
	},
	resetPasswordToken: String,
	resetPasswordExpires: Date,

	verifyToken: {
		type: String
	},
	remoteToken: {
		type: String
	},
	apiKey: {
		type: String,
		unique: true,
		index: true,
		sparse: true
	},

	lastLogin: {
		type: Date
	},

	locale: {
		type: String
	},

	status: {
		type: Number,
		default: 1
	},

	metadata: {}

}, schemaOptions);

/**
 * Virtual `code` field instead of _id
 */
UserSchema.virtual("code").get(function() {
	return this.encodeID();
});

UserSchema.virtual("fb_pages", {
	ref: "Page", // The model to use
	localField: "fb_pageIds", // Find people where `localField`
	foreignField: "fb_id" // is equal to `foreignField`
});
UserSchema.virtual("viewPages", {
	ref: "Page", // The model to use
	localField: "profile.accessPages", // Find people where `localField`
	foreignField: "fb_id" // is equal to `foreignField`
});
/**
 * Auto increment for `_id`
 */
// UserSchema.plugin(autoIncrement.plugin, {
// 	model: "User",
// 	startAt: 1
// });

/**
 * Password hashing
 */
UserSchema.pre("save", function(next) {
	let user = this;
	if (!user.isModified("password"))
		return next();

	bcrypt.genSalt(10, function(err, salt) {
		bcrypt.hash(user.password, salt, null, function(err, hash) {
			user.password = hash;
			next();
		});
	});
});

/**
 * Password compare
 */
UserSchema.methods.comparePassword = function(password, cb) {
	bcrypt.compare(password, this.password, function(err, isMatch) {
		cb(err, isMatch);
	});
};

/**
 * Virtual field for `avatar`.
 */
UserSchema.virtual("avatar").get(function() {
	// Load picture from profile
	if (this.profile && this.profile.picture)
		return this.profile.picture;

	// Generate a gravatar picture
	if (!this.email)
		return "https://gravatar.com/avatar/?s=64&d=wavatar";

	let md5 = crypto.createHash("md5").update(this.email).digest("hex");
	return "https://gravatar.com/avatar/" + md5 + "?s=64&d=wavatar";
});

/**
 * Encode `_id` to `code`
 */
UserSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

/**
 * Decode `code` to `_id`
 */
UserSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};

/**
 * Pick is only some fields of object
 *
 * http://mongoosejs.com/docs/api.html#document_Document-toObject
 *
UserSchema.methods.pick = function(props, model) {
	return _.pick(model || this.toJSON(), props || [
		"code",
		"fullName",
		"email",
		"username",
		"roles",
		"lastLogin",
		"avatar"
	]);
};

UserSchema.method('toJSON', function() {
    var user = this.toObject();
    delete user.salt;
    delete user.hash;
    delete user.__v;
    return user;
  });
*/

/*
UserSchema.methods.gravatar = function (size, defaults) {
	if (!size)
		size = 200;

	if (!defaults)
		defaults = 'wavatar';

	if (!this.email)
		return `https://gravatar.com/avatar/?s=${size}&d=${defaults}`;

	let md5 = crypto.createHash('md5').update(this.email).digest("hex");
	return `https://gravatar.com/avatar/${md5}?s=${size}&d=${defaults}`;
};*/

let User = mongoose.model("User", UserSchema);

module.exports = User;
