let config    		= require("../config");
let logger    		= require("../core/logger");
let C 				= require("../core/constants");
let fs 				= require("fs");
let path 			= require("path");

let _ 				= require("lodash");
let crypto 			= require("crypto");
let bcrypt 			= require("bcrypt-nodejs");

let db	    		= require("../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../libs/hashids")("users");
let autoIncrement 	= require("mongoose-auto-increment");

let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	}
};

let validateLocalStrategyProperty = function(property) {
	return (this.provider !== "local" && !this.updated) || property.length;
};

let validateLocalStrategyPassword = function(password) {
	return this.provider !== "local" || (password && password.length >= 6);
};

//var Schema       = mongoose.Schema;
let ObjectId = Schema.ObjectId;

let FbTokenSchema   = new Schema({
	type: {type:String, required: true},
	userId: {type:String, required: true},
	pageId: {type:String},
	key: {type:String},
	token: {type:String},
	updated_at: { type: Date, default: new Date()},
}, schemaOptions);



let FbToken= mongoose.model("FbToken", FbTokenSchema);
module.exports = FbToken;
