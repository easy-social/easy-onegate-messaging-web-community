"use strict";

global.WEBPACK_BUNDLE = false;

require("regenerator-runtime/runtime");

const fs = require("fs");
const path = require("path");

require("./core/init");
const db = require("./core/mongo")();

const S = require("./core/services");
S.loadServices(null, db);

require("./core/agenda");
require("./libs/gracefulExit");

const servicePath = path.join(__dirname, "applogic/modules");
const services = fs.readdirSync(servicePath);

// example: các job cần chạy ra process riêng, tạo file có dạng *.schedule.js để tự import vào schedule boostrap
// schedule bootstrap sẽ ko load app express
services.forEach((s) => {
	const files = fs.readdirSync(path.join(servicePath, s));

	files.forEach((file) => {
		if (file.endsWith(".schedule.js")) {
			require(path.join(servicePath, s, file));
		}
	});
});
