/**
 * Copyright 2017-present, Facebook, Inc. All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 */

// ===== LODASH ================================================================
let {castArray} = require("lodash");
let {isEmpty} = require("lodash");

// ===== MODULES ===============================================================
let request = require("request");

//const PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN;

/**
 * Send messages in order to the Facebook graph API.
 *
 * @param {String}          endPoint - Specific endpoint to send data to
 * @param {Object|Object[]} messageDataArray Payloads to send individually
 * @param {Object}          queryParams - Query Parameters
 * @param {Object}          retries - # of times to attempt to send a message.
 * @returns {undefined}
 */
const callAPI = (endPoint, messageDataArray, queryParams = {}, retries = 5, ptoken) => {
	return new Promise((resolve, reject)=>{

	
  // Error if developer forgot to specify an endpoint to send our request to
		if (!endPoint) {
			let err = "callAPI requires you to specify an endpoint";
			console.error(err);
		//return;
			reject(err);
		}

		if (!ptoken) {
			let err = "callAPI requires you to specify a page access token.";
			console.error(err);
		//return;
			reject(err);
		}

  // // Error if we've run out of retries.
	// if (retries < 0) {
	// 	let err =  "No more retries left."
	// 	console.error(
  //    err,
  //     {endPoint, messageDataArray, queryParams}
  //   );

	// 	reject(err)
	// }

  // ensure query parameters have a PAGE_ACCESS_TOKEN value
  /* eslint-disable camelcase */
		const query = Object.assign({access_token: ptoken}, queryParams);
  /* eslint-enable camelcase */

  // ready the first message in the array for send.
		const [messageToSend, ...queue] = castArray(messageDataArray);
		request({
			uri: `https://graph.facebook.com/v2.6/me/${endPoint}`,
			qs: query,
			method: "POST",
			json: messageToSend,

		}, (error, response, body) => {
			if (!error && response.statusCode === 200) {
      // Message has been successfully received by Facebook
				console.log(
        `Successfully sent message to ${endPoint} endpoint: `,
        resolve(JSON.stringify(body))
      );

      // // continue sending payloads until queue empty
			// if (!isEmpty(queue)) {
			// 	callAPI(endPoint, queue, queryParams);
			// }
			} else {
				let err = `Failed calling Messenger API endpoint ${endPoint}`;
      // Message has not been successfully received by Facebook
				console.error(
        err,
        response.statusCode,
        response.statusMessage,
        body.error,
        queryParams
      );

      // // Retry the request
			// console.error(`Retrying Request: ${retries} left`);
			// callAPI(endPoint, messageDataArray, queryParams, retries - 1);
			}
		});
	});
};

const callMessagesAPI = (messageDataArray, queryParams = {}, ptoken) => {
	return callAPI("messages", messageDataArray, queryParams,0,ptoken);
};

const callAttachAPI = (messageDataArray, queryParams = {}, ptoken) => {
	return callAPI("message_attachments", messageDataArray, queryParams,0,ptoken);
};

const callThreadAPI = (messageDataArray, queryParams = {}) => {
	return callAPI("thread_settings", messageDataArray, queryParams);
};

module.exports = {
	callMessagesAPI,
	callThreadAPI,
	callAttachAPI
};
