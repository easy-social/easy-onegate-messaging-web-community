module.exports = {
	isPhoneNumber(message) {
		//validate vietnamese phone
		let phoneno = /(03|05|07|08|09)+([0-9]{8})\b/g;
		//var phoneno = /^\d{10}$/;
		let phone = this.messageToPhone(message);
		console.log("isPhoneNumber ", phone.match(phoneno));
		if (phone.match(phoneno)) return true;
		return false;
		//return message && !isNaN(message);
	},
	messageToPhone(message) {
		return message.replace(/\D/g, "");
	}
};
