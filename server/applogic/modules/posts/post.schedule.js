const _ = require("lodash");
const agenda = require("../../../core/agenda");
const Services = require("../../../core/services");
const { fbGraphPageCallAsync } = require("./util");

agenda.define("facebook.syncPost", async (job) => {
	console.log("data", job.attrs);

	const { pageId, sync, ptoken, after } = job.attrs.data;

	let cons = [];
	let result = {
		type: "POST",
		num_success: 0,
		num_error: 0,
		error_logs: [],
		last: {},
		date_start: new Date(),
		date_stop: new Date(),
		date_end: new Date(),
	};
	let limit = 100; // sync ? 1 : 100;

	// let profileId = job.attrs.data.profileId;
	//let model = job.attrs.data.conversationModel;
	let queryFields = {
		fields:
			"message,full_picture,created_time,from,permalink_url,shares, reactions{type,id,link,name,pic,profile_type,username,picture}" +
			", attachments,reactions.type(NONE).limit(0).summary(total_count).as(reactions_NONE),comments.limit(1){from}," +
			"reactions.type(LIKE).limit(0).summary(total_count).as(reactions_LIKE)," +
			"reactions.type(LOVE).limit(0).summary(total_count).as(reactions_LOVE)," +
			"reactions.type(WOW).limit(0).summary(total_count).as(reactions_WOW)," +
			"reactions.type(HAHA).limit(0).summary(total_count).as(reactions_HAHA)," +
			"reactions.type(SAD).limit(0).summary(total_count).as(reactions_SAD)," +
			"reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_ANGRY)," +
			"reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_THANKFUL)",
		limit,
	};

	if (after) {
		queryFields.after = after;
	}

	const postService = Services.get("posts");

	try {
		const fetchPosts = async (ptoken, pageId, queryFields) => {
			const posts = await fbGraphPageCallAsync(ptoken, [
				`/${pageId}/posts`,
				"GET",
				queryFields,
			]);

			if (posts && posts.data.length > 0) {
				await Promise.all(
					_.map(posts.data, async (c) => {
						let hasChild = false;

						const _c = {
							...c,

							fb_page: pageId,
							count_comment: 0,
							count_phone: 0,
						};
						if (c.comments) {
							hasChild = true;
							delete _c.comments;
						}

						const saved = await postService.saveOrInsert(c.id, _c);

						cons.push(saved);
						result.num_success++;
						if (hasChild) {
							// synchonizeComment(ctx, saved, job, done, next, 1);
							await agenda
								.create("facebook.syncComment", {
									postId: c.id,
									pageId,
									ptoken,
								})
								.save();
						}
					})
				);

				if (cons.length == limit) {
					// self.sendNotificationToUser(ctx, "synchronizingProcess", {
					// 	type: "success",
					// 	msg: "Posts saved",
					// });
				}

				// fetch next page
				if (posts.paging && posts.paging.next) {
					console.info("fetch next, page", pageId);
					// HINT: https://developers.facebook.com/docs/graph-api/using-graph-api/v2.2#paging
					// Không lưu trữ con trỏ. Con trỏ có thể nhanh chóng không hợp lệ nếu các mục được thêm hoặc xóa.
					await fetchPosts(ptoken, pageId, {
						...queryFields,
						after: posts.paging.cursors.after,
					});
				}

				// if (posts.paging && posts.paging.cursors && sync) {
				// 	// model[2]["after"] = posts.paging.cursors.after;
				// 	// toCall(token, model, self, limit);

				// } else {
				// 	// if (ctx) {
				// 	// 	self.sendNotificationToUser(ctx, "synchronizingProcess", {
				// 	// 		type: "success",
				// 	// 		msg: "Posts saved",
				// 	// 	});
				// 	// }
				// 	// result.date_end = new Date();
				// 	// result.last = _.last(posts.data);
				// 	// done(result);
				// 	// resolve(cons);
				// }
			} else {
				result.date_end = new Date();
				result.error_logs.push("no-data");
				// done(result);
				// if (ctx) {
				// 	self.sendNotificationToUser(ctx, "synchronizingProcess", {
				// 		type: "success",
				// 		msg: "Posts finished",
				// 	});
				// }
			}
		};

		await fetchPosts(ptoken, pageId, queryFields);
	} catch (err) {
		console.log(err);
		// result.num_error++;
		// result.error_logs.push(err);
		// result.date_end = new Date();
		// result.last = model;
		// done(result);
		// if (ctx) {
		// 	self.sendNotificationToUser(ctx, "synchronizingProcess", {
		// 		type: "error",
		// 		msg: err,
		// 	});
		// }
	}

	// return cons;
	job.remove();
});
