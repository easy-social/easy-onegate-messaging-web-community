const FB = require("fb");

const fbGraphPageCallAsync = (access_token, reqParams) =>
	new Promise((resolve, reject) => {
		FB.setAccessToken(access_token);
		FB.api(...reqParams, (response) => {
			if (!response || response.error) return reject(response);
			resolve(response);
		});
	});

module.exports = {
	fbGraphPageCallAsync,
};
