"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let db	    		= require("../../../../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../../../../libs/hashids")("devices");
let autoIncrement 	= require("mongoose-auto-increment");

let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	}
};

let TagSchema = new Schema({
	name: {
		type: String,
		trim: true
	},
	tagcode: {
		type: String,
		trim: true
	},
	color: {
		type: String,
		trim: true
	},
	fb_id: {
		type: String,
	},
	fb_page: {
		type: String,
	},
	created_by: {},
	created_by_id: {
		type: String,
	},
	createdAt: {
		type: Date
	},
	metadata: {}

}, schemaOptions);

TagSchema.virtual("code").get(function() {
	return this.encodeID();
});

TagSchema.plugin(autoIncrement.plugin, {
	model: "Tag",
	startAt: 1
});

TagSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

TagSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};

let Tag = mongoose.model("Tag", TagSchema);

module.exports = Tag;
