"use strict";

let logger = require("../../../core/logger");
let C = require("../../../core/constants");
let _ = require("lodash");
let FB = require("fb");
let Context = require("../../../core/context");
let sendApi = require("../../../libs/messenger-api-helpers/send");
let FbToken = require("../../../models/fbtoken");
let async = require("async");
let agenda = require("../../../core/agenda");
const moment = require("moment");
const { fbGraphPageCallAsync } = require("./util");

const race = (func, timeoutFnc, timeout = 30000) =>
	Promise.race([
		new Promise((resolve, reject) => {
			setTimeout(() => {
				timeoutFnc().then(resolve).catch(reject);
			}, timeout); // 30s
		}),
		func,
	]);

module.exports = {
	settings: {
		// Name of service
		name: "facebook",

		// Version (for versioned API)
		version: 1,

		// Namespace for rest and websocket requests
		namespace: "facebook",

		// Enable calling via REST
		rest: true,

		// Enable calling via websocket
		ws: true,

		// Required permission for actions
		permission: C.PERM_LOGGEDIN,
	},

	// Actions of service
	actions: {
		/**
		 * Send message to facebook using sendAPI
		 *
		 */
		sendAttachmentToFB: {
			cache: false,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					let recipientId = ctx.req.body.to;
					let messagePayloads = ctx.req.body.fbModel;
					let ptoken = ctx.app.locals.pageTokenCache.get(
						ctx.user.socialLinks.facebook + "_" + ctx.params.pageId
					);
					sendApi
						.sendAttachment(recipientId, messagePayloads, ptoken)
						.then((data) => {
							logger.log(data);
							resolve(data);
						})
						.catch((err) => {
							logger.info(err);
							reject(err);
						});
				});
			},
		},
		sendMessageToFB: {
			cache: false,
			async handler(ctx) {
				const recipientId = ctx.req.body.to;
				const messagePayloads = ctx.req.body.fbModel;
				const ptoken = ctx.app.locals.pageTokenCache.get(
					ctx.user.socialLinks.facebook + "_" + ctx.params.pageId
				);

				try {
					const data = await sendApi.sendMessage(
						recipientId,
						messagePayloads,
						ptoken
					);
					logger.log(data);
					return data;
				} catch (err) {
					logger.info(err);
					throw err;
				}
			},
		},
		/**
		 * 	Get the value of the facebook.
		 *
		 *	via REST:
		 *		GET /facebook
		 *		GET /facebook/search
		 *
		 *	via Websocket:
		 *		/facebook/search
		 *
		 *	via GraphQL:
		 *		query { facebook }
		 */
		graph: {
			cache: false,
			handler(ctx) {
				return this.graph(ctx);
			},
		},

		graphAvatar: {
			cache: true,
			handler(ctx) {
				return this.graphConversationsByPage(ctx);
			},
		},
		activePage: {
			cache: false,
			async handler(ctx) {
				const {
					pageId,
					isSynchonize,
					isSynchonizePost,
					isSynchonizeCon,
					limit,
				} = ctx.req.body;

				const profileId = ctx.user.socialLinks.facebook;
				const userId = ctx.user.id;
				const ptoken = ctx.app.locals.pageTokenCache.get(
					`${profileId}_${pageId}`
				);

				if (!pageId) {
					throw new Error(ctx.t("app:ReqisterParamsRequired"));
				}

				// await this.scheduleService.purge();

				await this.scheduleService
					.create("facebook.activePage", {
						userId,
						pageId,
						profileId,
						ptoken,
						limit,
						isSynchonize,
						isSynchonizePost,
						isSynchonizeCon,
					})
					.save();

				await race(
					new Promise((resolve) => {
						this.scheduleService.define(
							`facebook.activePage(${pageId})`,
							(job) => {
								console.log("execute", job.attrs);
								this.sendNotificationToUser(
									ctx,
									"synchronizingProcess",
									{
										type: "success",
										msg: ctx.t("app:AllDataSaved"),
										result: job.attrs.data,
									}
								);

								job.remove();
								resolve();
							}
						);
					}),
					async () =>
						this.sendNotificationToUser(
							ctx,
							"synchronizingProcess",
							{
								type: "success",
								msg: ctx.t("app:synchronzing"),
								result: {},
							}
						)
				);

				return {
					type: "success",
					msg: ctx.t("app:synchronzing"),
				};
			},
		},
		resyncPage: {
			cache: false,
			async handler(ctx) {
				const {
					pageId,
					isSynchonize,
					isSynchonizePost,
					isSynchonizeCon,
					limit,
				} = ctx.req.body;

				const profileId = ctx.user.socialLinks.facebook;
				const userId = ctx.user.id;
				const ptoken = ctx.app.locals.pageTokenCache.get(
					`${profileId}_${pageId}`
				);

				if (!pageId) {
					throw new Error(ctx.t("app:ReqisterParamsRequired"));
				}

				this.scheduleService
					.create("facebook.activePage", {
						userId,
						pageId,
						profileId,
						ptoken,
						limit,
						isSynchonize,
						isSynchonizePost,
						isSynchonizeCon,
					})
					.save();

				await race(
					new Promise((resolve) => {
						this.scheduleService.define(
							`facebook.activePage(${pageId})`,
							(job) => {
								console.log("execute", job.attrs);
								this.sendNotificationToUser(
									ctx,
									"synchronizingProcess",
									{
										type: "success",
										msg: ctx.t("app:AllDataSaved"),
										result: job.attrs.data,
									}
								);

								job.remove();
								resolve();
							}
						);
					}),
					async () =>
						this.sendNotificationToUser(
							ctx,
							"synchronizingProcess",
							{
								type: "success",
								msg: ctx.t("app:synchronzing"),
								result: {},
							}
						)
				);

				return {
					type: "success",
					msg: ctx.t("app:synchronzing"),
				};
			},
		},
		graphPageById: {
			cache: false,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					console.log("graphPageById ==============", ctx.req.body);
					//this.createJob(ctx);
					let reqParams = ctx.req.body.fbModel;
					let pageId = ctx.req.body.pageId;
					let isSynchonize = ctx.req.body.isSynchonize;
					let ptoken = ctx.app.locals.pageTokenCache.get(
						ctx.user.socialLinks.facebook + "_" + pageId
					);
					if (!pageId) {
						reject(ctx.t("app:ReqiesParramsRequired"));
					}
					if (!reqParams) {
						let conditions = "id,name,link,about,picture";
						let cModel = [
							"/?ids=" + pageId,
							"GET",
							{ fields: conditions },
						];
						reqParams = cModel;
					}
					FB.setAccessToken(
						ctx.app.locals.userTokenCache.get(
							ctx.user.socialLinks.facebook
						)
					);
					FB.api(...reqParams, (response) => {
						if (!response || response.error) {
							if (
								response &&
								(response.error.code == 104 ||
									response.error.code == 190)
							)
								this.sendNotificationToUser(
									ctx,
									"tokenExpired",
									response.error
								);

							// fbctx.broadcast("notification", bro);
							reject(response.error);
						} else {
							let locals = ctx.app.locals;
							let user_fb_id = ctx.user.socialLinks.facebook;

							//subscribe page to hook
							let queryFields = {
								subscribed_fields:
									"feed,mention, message_mention, messages, message_reactions, messaging_account_linking, messaging_checkout_updates, message_echoes, message_deliveries, messaging_game_plays, messaging_optins, messaging_optouts, messaging_payments, messaging_postbacks, messaging_pre_checkouts, message_reads, messaging_referrals, messaging_handovers, messaging_policy_enforcement, messaging_page_feedback, messaging_appointments,  publisher_subscriptions",
							};
							let pModel = [
								"/" + pageId + "/subscribed_apps",
								"POST",
								queryFields,
							];
							this.fbgraphPagePreCall(
								locals,
								user_fb_id,
								pageId,
								pModel
							).then((data) => {
								if (data.success == true) {
									if (isSynchonize) {
										let d = response[pageId];
										if (d) {
											d.author = ctx.user.id;
											d.member_count = 1;
											d.fb_memberIds = [ctx.user.id];
											d.isActive = true;
											this.pageService
												.updateOrInsert(d.id, d)
												.then((page) => {
													//Save conversation and message
													let condition =
														"conversations.limit(100)";
													let after = "";
													let condition11 =
														"messages.limit(100)";
													let after11 = "";
													let model = [
														"/" + pageId,
														"GET",
														{
															fields:
																condition +
																after +
																"{message_count," +
																condition11 +
																after11 +
																"{created_time,from,id,message," +
																"sticker,tags,attachments{id,image_data,mime_type,name,size,video_data,file_url},shares" +
																"{description,id,link,name}},link,name,snippet,updated_time,unread_count,wallpaper," +
																"subject,senders,can_reply,scoped_thread_key,id,is_subscribed }",
														},
													];

													this.fbgraphPagePreCall(
														locals,
														user_fb_id,
														pageId,
														model
													)
														.then((data) => {
															if (
																data.conversations &&
																isSynchonize
															) {
																//if(data.conversations.paging && data.conversations.paging.next){
																this.createSchedule(
																	ctx,
																	{
																		pageId: pageId,
																		profileId: user_fb_id,
																		type:
																			"CONVERSATION",
																		ptoken: ptoken,
																		condition: condition,
																		after: after,
																		after2: after11,
																		condition2: condition11,
																		con_after:
																			data
																				.conversations
																				.paging
																				.cursors
																				.after,
																		mes_after:
																			data
																				.conversations
																				.data[0]
																				.messages
																				.paging
																				.cursors
																				.after,
																	},
																	"1 minute"
																);
																//}
																async.forEach(
																	data
																		.conversations
																		.data,
																	(c) => {
																		// c.updated_time =  new Date(parseInt(c.updated_time));
																		let senders =
																			c
																				.senders
																				.data;
																		c.customerIds = [];
																		async.forEach(
																			senders,
																			(
																				s
																			) => {
																				let contactTosave = _.cloneDeep(
																					s
																				);
																				// contactTosave.fb_pages = [pageId];
																				if (
																					s.id !=
																					pageId
																				) {
																					this.contactService.updateOrInsert(
																						s.id,
																						contactTosave,
																						pageId
																					);
																					c.fb_fromId =
																						s.id;
																					c.from = s;
																					c.customerIds.push(
																						s.id
																					);
																				}
																			}
																		);
																		c.fb_page = pageId;
																		c.fb_type =
																			"INBOX";
																		this.conversationService
																			.updateOrInsert(
																				c.id,
																				c
																			)
																			.then(
																				(
																					con
																				) => {
																					async.forEach(
																						c
																							.messages
																							.data,
																						(
																							m
																						) => {
																							let id =
																								m.id;
																							m.conversation_id =
																								con.fb_id;
																							this.messageService.updateOrInsert(
																								id,
																								m
																							);
																						}
																					);
																				}
																			);
																	}
																);
															}
														})
														.catch((err) => {
															reject(err);
														});

													//Save conversation and comment
													let condition1 =
														"posts.limit(100)";
													let after1 = "";
													let condition2 = "";
													let after2 = "";
													let condition3 = "";
													let after3 = "";

													let model2 = [
														"/" + pageId,
														"GET",
														{
															fields:
																"posts.limit(100){message,full_picture,comments.limit(100){message_tags,attachment,parent,message," +
																"comments.limit(100){message_tags,attachment,parent,message,comment_count,from,created_time},comment_count,from,created_time}" +
																",created_time,from,permalink_url,shares, reactions{type,id,link,name,pic,profile_type,username,picture}" +
																", attachments,reactions.type(NONE).limit(0).summary(total_count).as(reactions_NONE)," +
																"reactions.type(LIKE).limit(0).summary(total_count).as(reactions_LIKE)," +
																"reactions.type(LOVE).limit(0).summary(total_count).as(reactions_LOVE)," +
																"reactions.type(WOW).limit(0).summary(total_count).as(reactions_WOW)," +
																"reactions.type(HAHA).limit(0).summary(total_count).as(reactions_HAHA)," +
																"reactions.type(SAD).limit(0).summary(total_count).as(reactions_SAD)," +
																"reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_ANGRY)," +
																"reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_THANKFUL)}",
														},
													];
													this.fbgraphPagePreCall(
														locals,
														user_fb_id,
														pageId,
														model2
													)
														.then((data) => {
															if (
																data.posts &&
																isSynchonize
															) {
																//if(data.posts.paging && data.posts.paging.next){
																this.createSchedule(
																	ctx,
																	{
																		pageId: pageId,
																		profileId: user_fb_id,
																		type:
																			"POST",
																		ptoken: ptoken,
																		condition: condition1,
																		after: after1,
																		condition2: condition2,
																		after2: after2,
																		condition3: condition3,
																		after3: after3,
																		po_after:
																			data
																				.posts
																				.paging
																				.cursors
																				.after,
																		com_after:
																			data
																				.posts
																				.data[0]
																				.comments
																				.paging
																				.cursors
																				.after,
																		ref_after: data
																			.posts
																			.data[0]
																			.comments
																			.data[0]
																			.comments
																			? data
																					.posts
																					.data[0]
																					.comments
																					.data[0]
																					.comments
																					.paging
																					.cursors
																					.after
																			: "",
																	},
																	"1 minute"
																);
																//}
																async.forEach(
																	data.posts
																		.data,
																	(p) => {
																		let id =
																			p.id;
																		p.fb_page = pageId;
																		this.postService.updateOrInsert(
																			id,
																			p
																		);
																		let comments =
																			p.comments;
																		if (
																			comments
																		) {
																			async.forEach(
																				comments.data,
																				(
																					c
																				) => {
																					c.fb_page = pageId;
																					c.postId =
																						p.id;
																					let childComment =
																						c.comments;
																					if (
																						c.from
																					)
																						c.customerIds = [
																							c
																								.from
																								.id,
																						];
																					this.conversationService
																						.updateOrInsertFromComment(
																							c.id,
																							c
																						)
																						.then(
																							(
																								conversation
																							) => {
																								c.conversation_id =
																									conversation.fb_id;
																								this.commentService.updateOrInsert(
																									c.id,
																									c
																								);
																								if (
																									c.from
																								) {
																									let s =
																										c.from;
																									// s.fb_pages = [pageId];
																									this.contactService.updateOrInsert(
																										s.id,
																										s,
																										pageId
																									);
																								}
																								if (
																									childComment
																								) {
																									async.forEach(
																										childComment.data,
																										(
																											cc
																										) => {
																											cc.conversation_id =
																												conversation.fb_id;
																											this.commentService.updateOrInsert(
																												cc.id,
																												cc
																											);
																											if (
																												cc.from
																											) {
																												let s =
																													cc.from;
																												// s.fb_pages = [pageId];
																												this.contactService.updateOrInsert(
																													s.id,
																													s,
																													pageId
																												);
																											}
																										}
																									);
																								}
																							}
																						);
																				}
																			);
																		}
																	}
																);
															}
														})
														.catch((err) => {
															reject(err);
														});

													this.peopleService
														.addPage(
															page.fb_id,
															d.author
														)
														.then((savedUser) => {
															resolve(page);
														})
														.catch((err) => {
															reject(err);
														});
												})
												.catch((err) => {
													reject(err);
												});
										}
									} else resolve(response);
								} else {
									logger.error("subscribed_apps:false");
								}
							});
						}
					});
				});
			},
		},
		graphPages: {
			cache: false,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					let locals = ctx.app.locals;
					let reqParams = ctx.req.body.fbModel;
					let isSynchonize = ctx.req.body.isSynchonize;
					if (!reqParams) {
						let conditions =
							"id,name,picture,accounts.limit(1000){tasks,id,name,link,picture,about,access_token,can_post,description_html,is_owned,country_page_likes,display_subtext,emails,general_info,global_brand_page_name}";
						let cModel = ["/me", "GET", { fields: conditions }];
						reqParams = cModel;
					}
					let access_token = locals.userTokenCache.get(
						ctx.user.socialLinks.facebook
					);

					FB.setAccessToken(access_token);
					FB.api(...reqParams, (response) => {
						if (!response || response.error) {
							if (
								response.error &&
								(response.error.code =
									104 || response.error.code == 190)
							) {
								this.sendNotificationToUser(
									ctx,
									"tokenExpired",
									response.error
								);
								// this.notifyModelChanges(
								// 	ctx,
								// 	"tokenExpired",
								// 	response.error
								// );
								// let nctx = Context.CreateToServiceInit(this.notificationService);
								// this.notificationService.notifyModelChanges(
								// 	ctx,
								// 	"tokenExpired",
								// 	response.error
								// );
							}
							reject(response.error);
						} else {
							if (isSynchonize) {
								_.forEach(response.accounts.data, (d) => {
									this.pageService
										.updateOrInsert(d.id, d)
										.then((page) => {
											this.peopleService.assingPageToUser(
												d.id,
												ctx.user.id
											);
											if (
												d.tasks &&
												d.tasks.indexOf("MANAGE") > -1
											) {
												this.peopleService.promotePageToUser(
													d.id,
													ctx.user.id
												);
											}
										})
										.catch((err) => {
											reject(err);
										});
								});
							}
							resolve(response.accounts);
						}
					});
				});
			},
		},
		graphFeedsByPage: {
			cache: false,
			handler(ctx) {
				return this.graphConversationsByPage(ctx);
				// return new Promise((resolve, reject) => {
				// 	let reqParams = ctx.req.body.fbModel;
				// 	let pageId = ctx.req.body.pageId;
				// 	let isSynchonize = ctx.req.body.isSynchonize;
				// 	let isRenewToken = ctx.req.body.isRenewToken;
				// 	let conversationId = ctx.req.body.conversationId;
				// 	if(isRenewToken && (!pageId || !reqParams)) reject(ctx.t("app:ReqiesParramsRequired"))
				// 	if(pageId){
				// 		if(isRenewToken){
				// 		let pmodel = ["/" + pageId, "GET", { fields: "access_token, name" }];
				// 		this.fbCall(ctx.user.profile.access_token, pmodel).then(page=>{
				// 			if(page.access_token){
				// 				this.fbCall(page.access_token, reqParams).then(result=>{
				// 					this.synchonizeDb(result, isSynchonize, pageId, conversationId)
				// 					resolve(result);
				// 				}).catch(err=>{
				// 					logger.error(err);
				// 					resolve([]);
				// 				})
				// 			}
				// 		}).catch(err=>{
				// 			logger.error(err);
				// 			resolve([]);
				// 		});
				// 		} else {
				// 			this.fbCall(undefined, reqParams).then(result=>{
				// 				console.log("post ==========", result)
				// 				this.synchonizeDb(result, isSynchonize)
				// 				resolve(result);
				// 			}).catch(err=>{
				// 				logger.error(err);
				// 				resolve([]);
				// 			})
				// 		}
				// 	}
				//
				//
				//
				// });
			},
		},
		graphPostsByPage: {
			cache: false,
			async handler(ctx) {
				try {
					const response = await this.graphConversationsByPage(ctx);
					const reqParams = ctx.req.body.fbModel;
					const {
						pageId,
						isSynchonize,
						conversationId,
					} = ctx.req.body.pageId;

					this.synchonizeDb(
						response,
						isSynchonize,
						pageId,
						conversationId
					);

					return response;
				} catch (err) {
					logger.error("fbComments", err);
					this.sendNotificationToUser(ctx, "tokenExpired", err.error);
				}
			},
		},
		graphConversationsByPage: {
			cache: false,
			handler(ctx) {
				return this.graphConversationsByPage(ctx);
			},
		},
		graphMessagesByConversation: {
			cache: false,
			handler(ctx) {
				return this.graphConversationsByPage(ctx);
			},
		},
		reSyncMessage: {
			cache: false,
			handler(ctx) {
				ctx.req.body.author = ctx.user;
				return this.syncAConversation(ctx.req.body, ctx);
			},
		},
		syncPageData: {
			cache: false,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					let resultMes = {
						conversations: {
							successCount: 0,
							failureCount: 0,
							data: {},
						},
					};
					let resultPos = {
						conversations: {
							successCount: 0,
							failureCount: 0,
							data: {},
						},
						posts: {
							successCount: 0,
							failureCount: 0,
							data: {},
						},
					};
					let pageId = ctx.req.body.pageId;

					if (!pageId) {
						reject(ctx.t("app:ReqiesParramsRequired"));
					}

					let conditions = "id,name,link,about";
					let cModel = [
						"/?ids=" + pageId,
						"GET",
						{ fields: conditions },
					];
					let locals = ctx.app.locals;
					let user_fb_id = ctx.user.socialLinks.facebook;
					this.fbgraphPagePreCall(locals, user_fb_id, pageId, cModel)
						.then((response) => {
							//subscribe page to hook
							let queryFields = {
								subscribed_fields:
									"feed,mention, message_mention, messages, message_reactions, messaging_account_linking, messaging_checkout_updates, message_echoes, message_deliveries, messaging_game_plays, messaging_optins, messaging_optouts, messaging_payments, messaging_postbacks, messaging_pre_checkouts, message_reads, messaging_referrals, messaging_handovers, messaging_policy_enforcement, messaging_page_feedback, messaging_appointments,  publisher_subscriptions",
							};
							let pModel = [
								"/" + pageId + "/subscribed_apps",
								"POST",
								queryFields,
							];
							this.fbgraphPagePreCall(
								locals,
								user_fb_id,
								pageId,
								pModel
							)
								.then((subscribed_apps_data) => {
									if (subscribed_apps_data.success == true) {
										let d = response[pageId];
										if (d) {
											d.author = ctx.user.id;
											d.member_count = 1;
											d.fb_memberIds = [ctx.user.id];
											d.isActive = true;
											this.pageService
												.updateOrInsert(d.id, d)
												.then(() => {
													this.sendNotificationToUser(
														ctx,
														"synchronizingProcess",
														{
															type: "info",
															msg: "Page saved",
														}
													);
												})
												.catch(() => {
													this.sendNotificationToUser(
														ctx,
														"synchronizingProcess",
														{
															type: "error",
															msg: "Page",
														}
													);
												});
											let model = [
												"/" + pageId,
												"GET",
												{
													fields:
														"conversations.limit(1){message_count,messages.limit(100){created_time,from,id,message," +
														"sticker,tags,attachments{id,image_data,mime_type,name,size,video_data,file_url},shares" +
														"{description,id,link,name}},link,name,snippet,updated_time,unread_count,wallpaper," +
														"subject,senders,can_reply,scoped_thread_key,id,is_subscribed }",
												},
											];
											this.syncConversations(
												pageId,
												model,
												user_fb_id,
												resultMes,
												false,
												ctx
											);
											//Save conversation and comment
											let model2 = [
												"/" + pageId,
												"GET",
												{
													fields:
														"posts.limit(1){message,full_picture,comments.limit(1){message_tags,attachment,parent,message," +
														"comments.limit(100){message_tags,attachment,parent,message,comment_count,from,created_time},comment_count,from,created_time}" +
														",created_time,from,permalink_url,shares, reactions{type,id,link,name,pic,profile_type,username,picture}" +
														", attachments,reactions.type(NONE).limit(0).summary(total_count).as(reactions_NONE)," +
														"reactions.type(LIKE).limit(0).summary(total_count).as(reactions_LIKE)," +
														"reactions.type(LOVE).limit(0).summary(total_count).as(reactions_LOVE)," +
														"reactions.type(WOW).limit(0).summary(total_count).as(reactions_WOW)," +
														"reactions.type(HAHA).limit(0).summary(total_count).as(reactions_HAHA)," +
														"reactions.type(SAD).limit(0).summary(total_count).as(reactions_SAD)," +
														"reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_ANGRY)," +
														"reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_THANKFUL)}",
												},
											];

											this.syncPostAndComments(
												pageId,
												model2,
												user_fb_id,
												resultPos,
												false,
												ctx
											);
											this.peopleService.addPage(
												d.id,
												d.author
											);
										}
										this.sendNotificationToUser(
											ctx,
											"synchronizingProcess",
											{
												type: "info",
												msg: "subscribed_apps:true",
											}
										);
										resolve("subscribed_apps:true");
									} else {
										reject("subscribed_apps:false");
									}
								})
								.catch((err) => {
									reject(err);
								});
						})
						.catch((err) => {
							reject(err);
						});
				});
			},
		},
	},

	methods: {
		async syncAConversation(obj, ctx) {
			const t = (x) => (ctx ? ctx.t(x) : x);

			const { pageId, conversation } = obj;

			let profileId = obj.author.socialLinks.facebook;
			let limit = obj.limit;
			let userId = obj.author.id;
			let ptoken = ctx.app.locals.pageTokenCache.get(
				profileId + "_" + pageId
			);

			if (!pageId) {
				throw new Error(t("app:ReqisterParamsRequired"));
			}

			race(
				new Promise((resolve) => {
					this.scheduleService.define(
						`facebook.syncMessages(conv=${conversation.fb_id})`,
						(job) => {
							console.log("execute", job.attrs);
							this.sendNotificationToUser(
								ctx,
								"synchronizingProcess",
								{
									type: "success",
									msg: t("app:AllDataSaved"),
									result: job.attrs.data,
								}
							);

							job.remove();
							resolve();
						}
					);
				}),
				async () =>
					this.sendNotificationToUser(ctx, "synchronizingProcess", {
						type: "success",
						msg: t("app:synchronzing"),
						result: {},
					})
			);

			await this.scheduleService
				.create("facebook.syncMessages", {
					pageId,
					ptoken,
					conversation: {
						id: conversation.fb_id,
						fb_page: conversation.fb_page,
					},
					limit,
				})
				.save();

			return {
				type: "success",
				msg: t("app:synchronzing"),
			};
		},

		sendNotificationToUser(ctx, ev, val) {
			let ctxs = Context.CreateToServiceInit(this.notificationService);
			ctxs.user = ctx.user;
			// ctxs.emitUser(ev, val);
			this.notificationService.broadcastByUser(ctxs, ev, val);
		},
		syncConversations(pageId, model, user_fb_id, result, isJob, context) {
			let ctx = context ? context : this.context;
			let clonedResult = _.cloneDeep(result);
			clonedResult.conversations.data = {};
			if (model.length == 0) {
				this.pageService.updateOrInsert(pageId, {
					syncCons: "completed",
					Conslogs: result,
				});
				if (context) {
					this.sendNotificationToUser(ctx, "synchronizingProcess", {
						type: "success",
						msg: "syncConversations : completed",
					});
				}
				return;
			}
			if (isJob == false) {
				if (
					clonedResult.conversations.failureCount +
						clonedResult.conversations.successCount >=
					1
				) {
					this.sendNotificationToUser(ctx, "synchronizingProcess", {
						type: "success",
						msg: "syncConversations : completed",
					});
					this.createJob(ctx, {
						type: "CONVERSATION",
						pageId,
						model,
						user_fb_id,
						result,
					});
					return;
				} else {
					this.sendNotificationToUser(ctx, "synchronizingProcess", {
						type: "progress",
						subType: "messages",
						data: clonedResult,
					});
				}
			}

			let feedService = this.context.services("feeds");
			this.fbgraphPagePreCall(
				this.context.app.locals,
				user_fb_id,
				pageId,
				model
			).then((data) => {
				if (data.conversations) {
					let c = data.conversations.data[0];
					let senders = c.senders.data;
					c.customerIds = [];
					_.forOwn(senders, (s) => {
						let contactTosave = _.cloneDeep(s);
						// contactTosave.fb_pages = [pageId];
						if (s.id != pageId) {
							this.contactService.updateOrInsert(
								s.id,
								contactTosave,
								pageId
							);
							c.fb_fromId = s.id;
							c.from = s;
							c.customerIds.push(s.id);
						}
					});
					c.fb_page = pageId;
					c.fb_type = "INBOX";
					c.hasPhone = false;
					result.conversations.data[c.id] = {
						successCount: 0,
						failureCount: 0,
						data: [],
					};
					this.conversationService
						.updateOrInsert(c.id, c)
						.then(() => {
							result.conversations.successCount++;
						})
						.catch(() => {
							result.conversations.failureCount++;
						});
					if (c.messages) {
						let count = 0;
						_.forOwn(c.messages.data, (m) => {
							m.conversation_id = c.id;
							if (c.hasPhone == false && m.from.id != pageId) {
								let phoneObj = feedService.getPhoneNumber(
									m.message
								);
								if (phoneObj.highlights.length > 0) {
									c.hasPhone = true;
									m.phones = phoneObj.phones;
									m.highlights = phoneObj.highlights;
									this.conversationService
										.updateOrInsert(c.id, c)
										.then(() => {
											result.conversations.data[
												c.id
											].success = false;
										})
										.catch((err) => {
											result.conversations.data[
												c.id
											].success = false;
											result.conversations.data[
												c.id
											].msg = err;
										});
								}
							}
							this.messageService
								.updateOrInsert(m.id, m)
								.then(() => {
									result.conversations.data[c.id]
										.successCount++;
									result.conversations.data[c.id].data.push({
										id: m.id,
										success: true,
									});
									count++;
								})
								.catch((e) => {
									result.conversations.data[c.id]
										.failureCount++;
									result.conversations.data[c.id].data.push({
										id: m.id,
										success: false,
										msg: e,
									});
									count++;
								});
						});

						let interval = setInterval(() => {
							// this.sendNotificationToUser(ctx, "synchronizingProcess", {type : 'info', msg: count+'_'+c.messages.data.length});
							if (count == c.messages.data.length) {
								clearInterval(interval);
								let returnModel = [];
								if (
									c.messages.paging &&
									c.messages.paging.next
								) {
									returnModel = [
										"/" + pageId,
										"GET",
										{
											fields:
												"conversations.limit(1){message_count,messages.limit(100).after(" +
												c.messages.paging.cursors
													.after +
												"){created_time,from,id,message," +
												"sticker,tags,attachments{id,image_data,mime_type,name,size,video_data,file_url},shares" +
												"{description,id,link,name}},link,name,snippet,updated_time,unread_count,wallpaper," +
												"subject,senders,can_reply,scoped_thread_key,id,is_subscribed }",
										},
									];
								} else {
									if (
										data.conversations.paging &&
										data.conversations.paging.next
									) {
										returnModel = [
											"/" + pageId,
											"GET",
											{
												fields:
													"conversations.limit(1).after(" +
													data.conversations.paging
														.cursors.after +
													"){message_count,messages.limit(100){created_time,from,id,message," +
													"sticker,tags,attachments{id,image_data,mime_type,name,size,video_data,file_url},shares" +
													"{description,id,link,name}},link,name,snippet,updated_time,unread_count,wallpaper," +
													"subject,senders,can_reply,scoped_thread_key,id,is_subscribed }",
											},
										];
									}
								}
								this.syncConversations(
									pageId,
									returnModel,
									user_fb_id,
									result,
									isJob,
									context
								);
							}
						}, 500);
					} else {
						let returnModel = [];
						if (
							data.conversations.paging &&
							data.conversations.paging.next
						) {
							returnModel = [
								"/" + pageId,
								"GET",
								{
									fields:
										"conversations.limit(1).after(" +
										data.conversations.paging.cursors
											.after +
										"){message_count,messages.limit(100){created_time,from,id,message," +
										"sticker,tags,attachments{id,image_data,mime_type,name,size,video_data,file_url},shares" +
										"{description,id,link,name}},link,name,snippet,updated_time,unread_count,wallpaper," +
										"subject,senders,can_reply,scoped_thread_key,id,is_subscribed }",
								},
							];
						}
						this.syncConversations(
							pageId,
							returnModel,
							user_fb_id,
							result,
							isJob,
							context
						);
					}
				}
			});
		},
		syncPostAndComments(pageId, model, user_fb_id, result, isJob, context) {
			let ctx = context ? context : this.context;
			let clonedResult = _.cloneDeep(result);
			clonedResult.conversations.data = {};
			clonedResult.posts.data = {};
			if (model.length == 0) {
				this.pageService.updateOrInsert(pageId, {
					syncPosts: "completed",
					Postslogs: result,
				});
				if (context) {
					this.sendNotificationToUser(ctx, "synchronizingProcess", {
						type: "success",
						msg: "syncPosts : completed",
					});
				}
				return;
			}
			if (isJob == false) {
				if (
					result.posts.failureCount + result.posts.successCount >=
					1
				) {
					this.sendNotificationToUser(ctx, "synchronizingProcess", {
						type: "success",
						msg: "syncPosts : completed",
					});
					this.createJob(ctx, {
						type: "POST",
						pageId,
						model,
						user_fb_id,
						result,
					});
					return;
				} else {
					this.sendNotificationToUser(ctx, "synchronizingProcess", {
						type: "progress",
						subType: "post",
						data: clonedResult,
					});
				}
			}

			let feedService = this.context.services("feeds");
			this.fbgraphPagePreCall(
				this.context.app.locals,
				user_fb_id,
				pageId,
				model
			)
				.then((data) => {
					if (data.posts) {
						let p = data.posts.data[0];
						let id = p.id;
						p.fb_page = pageId;
						result.posts.data[id] = {
							successCount: 0,
							failureCount: 0,
							data: [],
						};
						this.postService
							.updateOrInsert(id, p)
							.then(() => {
								result.posts.successCount++;
								result.posts.data[id].success = true;
							})
							.catch((err) => {
								result.posts.failureCount++;
								result.posts.data[id].success = false;
								result.posts.data[id].msg = err;
							});
						let comments = p.comments;
						if (comments) {
							let c = comments.data[0];
							c.fb_page = pageId;
							c.postId = p.id;
							if (c.from) {
								c.fb_fromId = c.from.id;
								let childComment = c.comments;
								if (c.from) c.customerIds = [c.from.id];
								let commentC = _.cloneDeep(c);
								commentC.conversation_id =
									p.id + "_" + c.fb_fromId;
								c.hasPhone = false;
								if (c.from.id != pageId) {
									let phoneObj = feedService.getPhoneNumber(
										c.message
									);
									if (phoneObj.highlights.length > 0) {
										c.hasPhone = true;
										commentC.phones = phoneObj.phones;
										commentC.highlights =
											phoneObj.highlights;
									}
								}
								if (
									!result.conversations.data[
										p.id + "_" + c.fb_fromId
									]
								) {
									result.conversations.data[
										p.id + "_" + c.fb_fromId
									] = {
										successCount: 0,
										failureCount: 0,
										data: [],
									};
								}

								this.conversationService
									.updateOrInsertFromComment(c.id, c)
									.then(() => {
										result.conversations.successCount++;
										result.conversations.data[
											p.id + "_" + c.fb_fromId
										].success = true;
									})
									.catch(() => {
										result.conversations.data[
											p.id + "_" + c.fb_fromId
										].success = false;
										result.conversations.failureCount++;
									});
								result.conversations.data[
									p.id + "_" + c.fb_fromId
								].data[c.id] = {
									successCount: 0,
									failureCount: 0,
									data: [],
								};
								this.commentService
									.updateOrInsert(commentC.id, commentC)
									.then(() => {
										result.conversations.data[
											p.id + "_" + c.fb_fromId
										].data[c.id].successCount++;
										result.conversations.data[
											p.id + "_" + c.fb_fromId
										].data[c.id].data.push({
											id: commentC.id,
											success: true,
										});
									})
									.catch((err) => {
										result.conversations.data[
											p.id + "_" + c.fb_fromId
										].data[c.id].failureCount++;
										result.conversations.data[
											p.id + "_" + c.fb_fromId
										].data[c.id].data.push({
											id: commentC.id,
											success: false,
											msg: err,
										});
									});
								let s = c.from;
								// s.fb_pages = [pageId];
								this.contactService.updateOrInsert(
									s.id,
									s,
									pageId
								);
								if (childComment) {
									let count = 0;
									_.forOwn(childComment.data, (cc) => {
										if (cc.from) {
											cc.conversation_id =
												p.id + "_" + c.fb_fromId;
											if (
												c.hasPhone == false &&
												cc.from.id != pageId
											) {
												let ccphoneObj = feedService.getPhoneNumber(
													cc.message
												);
												if (
													ccphoneObj.highlights
														.length > 0
												) {
													c.hasPhone = true;
													cc.phones =
														ccphoneObj.phones;
													cc.highlights =
														ccphoneObj.highlights;
													this.conversationService.updateOrInsert(
														c.id,
														{ hasPhone: true }
													);
												}
											}
											this.commentService
												.updateOrInsert(cc.id, cc)
												.then(() => {
													result.conversations.data[
														p.id + "_" + c.fb_fromId
													].data[c.id].successCount++;
													result.conversations.data[
														p.id + "_" + c.fb_fromId
													].data[c.id].data.push({
														id: cc.id,
														success: true,
													});
													count++;
												})
												.catch((error) => {
													result.conversations.data[
														p.id + "_" + c.fb_fromId
													].data[c.id].failureCount++;
													result.conversations.data[
														p.id + "_" + c.fb_fromId
													].data[c.id].data.push({
														id: cc.id,
														success: false,
														msg: error,
													});
													count++;
												});
											let s = cc.from;
											// s.fb_pages = [pageId];
											this.contactService.updateOrInsert(
												s.id,
												s,
												pageId
											);
										} else {
											count++;
										}
									});
									let interval = setInterval(() => {
										this.sendNotificationToUser(
											ctx,
											"synchronizingProcess",
											{
												type: "info",
												msg:
													p.message + "_" + c.message,
											}
										);
										this.sendNotificationToUser(
											ctx,
											"synchronizingProcess",
											{
												type: "info",
												msg:
													count +
													"_" +
													childComment.data.length,
											}
										);
										if (count == childComment.data.length) {
											clearInterval(interval);
											let returnModel = [];
											if (
												childComment.paging &&
												childComment.paging.next
											) {
												returnModel = [
													"/" + pageId,
													"GET",
													{
														fields:
															"posts.limit(1){message,full_picture,comments.limit(1){message_tags,attachment,parent,message," +
															"comments.limit(100).after(" +
															childComment.paging
																.cursors.after +
															"){message_tags,attachment,parent,message,comment_count,from,created_time},comment_count,from,created_time}" +
															",created_time,from,permalink_url,shares, reactions{type,id,link,name,pic,profile_type,username,picture}" +
															", attachments,reactions.type(NONE).limit(0).summary(total_count).as(reactions_NONE)," +
															"reactions.type(LIKE).limit(0).summary(total_count).as(reactions_LIKE)," +
															"reactions.type(LOVE).limit(0).summary(total_count).as(reactions_LOVE)," +
															"reactions.type(WOW).limit(0).summary(total_count).as(reactions_WOW)," +
															"reactions.type(HAHA).limit(0).summary(total_count).as(reactions_HAHA)," +
															"reactions.type(SAD).limit(0).summary(total_count).as(reactions_SAD)," +
															"reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_ANGRY)," +
															"reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_THANKFUL)}",
													},
												];
											} else {
												if (
													comments.paging &&
													comments.paging.next
												) {
													returnModel = [
														"/" + pageId,
														"GET",
														{
															fields:
																"posts.limit(1){message,full_picture,comments.limit(1).after(" +
																comments.paging
																	.cursors
																	.after +
																"){message_tags,attachment,parent,message," +
																"comments.limit(100){message_tags,attachment,parent,message,comment_count,from,created_time},comment_count,from,created_time}" +
																",created_time,from,permalink_url,shares, reactions{type,id,link,name,pic,profile_type,username,picture}" +
																", attachments,reactions.type(NONE).limit(0).summary(total_count).as(reactions_NONE)," +
																"reactions.type(LIKE).limit(0).summary(total_count).as(reactions_LIKE)," +
																"reactions.type(LOVE).limit(0).summary(total_count).as(reactions_LOVE)," +
																"reactions.type(WOW).limit(0).summary(total_count).as(reactions_WOW)," +
																"reactions.type(HAHA).limit(0).summary(total_count).as(reactions_HAHA)," +
																"reactions.type(SAD).limit(0).summary(total_count).as(reactions_SAD)," +
																"reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_ANGRY)," +
																"reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_THANKFUL)}",
														},
													];
												} else {
													if (
														data.posts.paging &&
														data.posts.paging.next
													) {
														returnModel = [
															"/" + pageId,
															"GET",
															{
																fields:
																	"posts.limit(1).after(" +
																	data.posts
																		.paging
																		.cursors
																		.after +
																	"){message,full_picture,comments.limit(1){message_tags,attachment,parent,message," +
																	"comments.limit(100){message_tags,attachment,parent,message,comment_count,from,created_time},comment_count,from,created_time}" +
																	",created_time,from,permalink_url,shares, reactions{type,id,link,name,pic,profile_type,username,picture}" +
																	", attachments,reactions.type(NONE).limit(0).summary(total_count).as(reactions_NONE)," +
																	"reactions.type(LIKE).limit(0).summary(total_count).as(reactions_LIKE)," +
																	"reactions.type(LOVE).limit(0).summary(total_count).as(reactions_LOVE)," +
																	"reactions.type(WOW).limit(0).summary(total_count).as(reactions_WOW)," +
																	"reactions.type(HAHA).limit(0).summary(total_count).as(reactions_HAHA)," +
																	"reactions.type(SAD).limit(0).summary(total_count).as(reactions_SAD)," +
																	"reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_ANGRY)," +
																	"reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_THANKFUL)}",
															},
														];
													}
												}
											}
											this.syncPostAndComments(
												pageId,
												returnModel,
												user_fb_id,
												result,
												isJob,
												context
											);
										}
									}, 500);
								} else {
									let returnModel = [];
									if (
										comments.paging &&
										comments.paging.next
									) {
										returnModel = [
											"/" + pageId,
											"GET",
											{
												fields:
													"posts.limit(1){message,full_picture,comments.limit(1).after(" +
													comments.paging.cursors
														.after +
													"){message_tags,attachment,parent,message," +
													"comments.limit(100){message_tags,attachment,parent,message,comment_count,from,created_time},comment_count,from,created_time}" +
													",created_time,from,permalink_url,shares, reactions{type,id,link,name,pic,profile_type,username,picture}" +
													", attachments,reactions.type(NONE).limit(0).summary(total_count).as(reactions_NONE)," +
													"reactions.type(LIKE).limit(0).summary(total_count).as(reactions_LIKE)," +
													"reactions.type(LOVE).limit(0).summary(total_count).as(reactions_LOVE)," +
													"reactions.type(WOW).limit(0).summary(total_count).as(reactions_WOW)," +
													"reactions.type(HAHA).limit(0).summary(total_count).as(reactions_HAHA)," +
													"reactions.type(SAD).limit(0).summary(total_count).as(reactions_SAD)," +
													"reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_ANGRY)," +
													"reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_THANKFUL)}",
											},
										];
									} else {
										if (
											data.posts.paging &&
											data.posts.paging.next
										) {
											returnModel = [
												"/" + pageId,
												"GET",
												{
													fields:
														"posts.limit(1).after(" +
														data.posts.paging
															.cursors.after +
														"){message,full_picture,comments.limit(1){message_tags,attachment,parent,message," +
														"comments.limit(100){message_tags,attachment,parent,message,comment_count,from,created_time},comment_count,from,created_time}" +
														",created_time,from,permalink_url,shares, reactions{type,id,link,name,pic,profile_type,username,picture}" +
														", attachments,reactions.type(NONE).limit(0).summary(total_count).as(reactions_NONE)," +
														"reactions.type(LIKE).limit(0).summary(total_count).as(reactions_LIKE)," +
														"reactions.type(LOVE).limit(0).summary(total_count).as(reactions_LOVE)," +
														"reactions.type(WOW).limit(0).summary(total_count).as(reactions_WOW)," +
														"reactions.type(HAHA).limit(0).summary(total_count).as(reactions_HAHA)," +
														"reactions.type(SAD).limit(0).summary(total_count).as(reactions_SAD)," +
														"reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_ANGRY)," +
														"reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_THANKFUL)}",
												},
											];
										}
									}
									this.syncPostAndComments(
										pageId,
										returnModel,
										user_fb_id,
										result,
										isJob,
										context
									);
								}
							}
						} else {
							let returnModel = [];
							if (data.posts.paging && data.posts.paging.next) {
								returnModel = [
									"/" + pageId,
									"GET",
									{
										fields:
											"posts.limit(1).after(" +
											data.posts.paging.cursors.after +
											"){message,full_picture,comments.limit(1){message_tags,attachment,parent,message," +
											"comments.limit(100){message_tags,attachment,parent,message,comment_count,from,created_time},comment_count,from,created_time}" +
											",created_time,from,permalink_url,shares, reactions{type,id,link,name,pic,profile_type,username,picture}" +
											", attachments,reactions.type(NONE).limit(0).summary(total_count).as(reactions_NONE)," +
											"reactions.type(LIKE).limit(0).summary(total_count).as(reactions_LIKE)," +
											"reactions.type(LOVE).limit(0).summary(total_count).as(reactions_LOVE)," +
											"reactions.type(WOW).limit(0).summary(total_count).as(reactions_WOW)," +
											"reactions.type(HAHA).limit(0).summary(total_count).as(reactions_HAHA)," +
											"reactions.type(SAD).limit(0).summary(total_count).as(reactions_SAD)," +
											"reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_ANGRY)," +
											"reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_THANKFUL)}",
									},
								];
							}
							this.syncPostAndComments(
								pageId,
								returnModel,
								user_fb_id,
								result,
								isJob,
								context
							);
						}
					} else {
						this.syncPostAndComments(
							pageId,
							[],
							user_fb_id,
							result,
							isJob,
							context
						);
					}
				})
				.catch((err) => {
					logger.error(err);
				});
		},
		createJob(ctx, data) {
			let ctrl = this;
			function cb() {
				ctrl.sendNotificationToUser(ctx, "synchronizingProcess", {
					type: "info",
					msg: "job : saved",
				});
			}
			let job = this.scheduleService.now("pageSync", data, cb);
		},

		createSchedule(ctx, data, time) {
			let jobName =
				"synchronizing_fb_" +
				ctx.params.pageId +
				"_" +
				ctx.user.id +
				"_" +
				data.type;
			agenda.define(jobName, (job, done) => {
				this.sychonizeDataWithJob(job, done);
			});
			const job = agenda.every(time, jobName, data);
			agenda.on("success:" + jobName, (job) => {
				console.log(`Sperify job success  ${job.attrs.name}`);
			});
			// //const job = agenda.create(jobName, data);
			// job.attrs.data = data;

			// job.save(err => {
			//  if(err) console.log(err);
			//   console.log('Job successfully saved');
			// });
		},

		sychonizeDataWithJob(job, done) {
			let name = job.attrs.name;
			let data = job.attrs.data;
			console.log("JOB Run at ==============>" + name, new Date());
			if (data.type == "CONVERSATION")
				this.synchonizeConversation(job, done);
			console.log("Data " + data);
			console.log("JOB Stop at ==============>" + name, new Date());
		},
		synchonizePage(ctx, job, done, next) {
			let pageId = job.attrs.data.pageId;
			let userId = job.attrs.data.userId;
			let token = job.attrs.data.ptoken;
			let generalSetting = {
				fb_id: pageId,
				type: "generalSetting",
				auto_create_order: false,
				auto_hide_comment: false,
				auto_hide_comment_with_mobile: false,
				auto_like: false,
				auto_next_conversation_when_enter_twice: false,
				auto_unhide_comment: false,
				auto_unhide_comment_time: {
					name: "1 ngày",
					value: 24,
				},
				description: "",
				fb_pages: [],
				fix_undisplay_photo: false,
				keep_conv_status_for_3rd_messages: false,
				notification: true,
				show_cached_avatar: false,
				sound: true,
				status: 1,
				sync_inbox_with_facebook: false,
				templates: {
					attachments: [],
				},
				unread_first: true,
			};
			let tagsSetting = {
				fb_id: pageId,
				type: "tagsSetting",
				description: "",
				fb_pages: [],
				full_tag: false,
				multi_tag: true,
				status: 1,
				tags: [
					{
						id: new Date().getTime() - 4,
						name: "Inquiry",
						backgroundcolor: "rgba(75, 85, 119, 1)",
					},
					{
						id: new Date().getTime() - 3,
						name: "Issue",
						backgroundcolor: "rgba(130, 43, 161, 1)",
					},
					{
						id: new Date().getTime() - 2,
						name: "Urgent",
						backgroundcolor: "rgba(13, 90, 255, 1)",
					},
					{
						id: new Date().getTime() - 1,
						name: "Reply Later",
						backgroundcolor: "rgba(0, 147, 68, 1)",
					},
				],
				templates: {
					attachments: [],
				},
			};
			let replySetting = {
				fb_id: pageId,
				type: "replySetting",
				active_auto_reply: false,
				auto_reply_end_time: null,
				auto_reply_start_time: null,
				auto_reply_templates: [],
				description: "",
				fb_pages: [],
				full_tag: false,
				multi_tag: false,
				status: 1,
				templates: [],
			};
			let assigningSetting = {
				fb_id: pageId,
				type: "assigningSetting",
				TypeSplit: "turn_off",
				description: "",
				fb_pages: [],
				hard_round_robin_assign_base_on_online_status: false,
				hard_round_robin_assign_to_other_user_online: false,
				hard_round_robin_only_assign_to_online_user: false,
				hard_round_robin_reassign_if_priority_online: false,
				hard_round_robin_reassign_if_unread_after_x_min: false,
				hard_round_robin_show_all_convs_if_user_not_in_list: false,
				status: 1,
				templates: {
					attachments: [],
				},
			};
			this.settingServces = ctx.services("settings");

			this.settingServces.updateOrInsertBy(
				{ fb_id: pageId, type: "generalSetting" },
				generalSetting
			);

			this.settingServces.updateOrInsertBy(
				{ fb_id: pageId, type: "tagsSetting" },
				tagsSetting
			);
			this.settingServces.updateOrInsertBy(
				{ fb_id: pageId, type: "replySetting" },
				replySetting
			);
			this.settingServces.updateOrInsertBy(
				{ fb_id: pageId, type: "assigningSetting" },
				assigningSetting
			);

			let result = {
				type: "PAGE",
				num_success: 0,
				num_error: 0,
				eror_logs: [],
				last: {},
				date_start: new Date(),
				date_stop: new Date(),
				date_end: new Date(),
			};
			let queryFields = {
				fields: "id,name,link,about,picture",
				subscribed_fields:
					"feed,mention, message_mention, messages, message_reactions, messaging_account_linking, messaging_checkout_updates, message_echoes, message_deliveries, messaging_game_plays, messaging_optins, messaging_optouts, messaging_payments, messaging_postbacks, messaging_pre_checkouts, message_reads, messaging_referrals, messaging_handovers, messaging_policy_enforcement, messaging_page_feedback, messaging_appointments,  publisher_subscriptions",
			};
			let model = [
				"/" + pageId + "/subscribed_apps",
				"POST",
				queryFields,
			];
			return new Promise((resolve, reject) => {
				this.fbgraphPageCall(token, model)
					.then((page) => {
						page.author = userId;
						page.member_count = 1;
						page.fb_memberIds = [ctx.user.id];
						page.isActive = true;
						this.pageService
							.updateOrInsert(page.id, page)
							.then((saved) => {
								result.num_success++;
								result.date_end = new Date();
								result.last = saved;
								done(result);
								resolve(saved);
							})
							.catch((err) => {
								result.date_end = new Date();
								result.last = page;
								result.num_error++;
								result.error_logs.push(err);
								done(result);
								reject(err);
							});
					})
					.catch((err) => {
						result.date_end = new Date();
						result.last = model;
						result.num_error++;
						result.error_logs.push(err);
						done(result);
						reject(err);
					});
			});
		},

		synchonizeConversation(ctx, sync, job, done, next) {
			let cons = [];
			return new Promise((resolve, reject) => {
				let pageId = job.attrs.data.pageId;
				// let profileId = job.attrs.data.profileId;
				let token = job.attrs.data.ptoken;
				let result = {
					type: "CONVERSATION",
					num_success: 0,
					num_error: 0,
					error_logs: [],
					last: {},
					date_start: new Date(),
					date_stop: new Date(),
					date_end: new Date(),
				};
				let limit = sync ? 1 : 100;
				//let model = job.attrs.data.conversationModel;
				let queryFields = {
					fields:
						"message_count,link,name,snippet,updated_time,unread_count,wallpaper,subject,senders,can_reply,scoped_thread_key,id,is_subscribed ,messages.limit(1){from}",
					limit: limit,
				};

				///if(next) queryFields.after = next;
				let model = [
					"/" + pageId + "/conversations",
					"GET",
					queryFields,
				];
				let self = this;
				toCall(token, model, self);

				function toCall(token, model, self) {
					self.fbgraphPageCall(token, model)
						.then((conversations) => {
							if (
								conversations &&
								conversations.data.length > 0
							) {
								let waits = []; // wait for saved
								//cons = cons.concat(_.map(conversations.data, c=>c.id));
								_.forIn(conversations.data, (c) => {
									c.fb_page = pageId;
									let from = _.find(c.senders.data, (s) => {
										return s.id != pageId;
									});
									if (from) {
										c.from = from;
										c.fb_fromId = from.id;
										let s = _.cloneDeep(c.from);
										let sid = s.id;
										// s.fb_pages = [pageId];
										self.contactService.updateOrInsert(
											sid,
											s,
											pageId
										);
										c.customerIds = [s.id];
										c.last_sent_by =
											c.messages.data[0].from;
										delete c.messages;
										if (
											c.unread_count > 0 &&
											moment(new Date()).diff(
												moment(c.updated_time),
												"days"
											) > 14
										) {
											c.unread_count = 0;
										}
										waits.push(
											self.conversationService
												.saveOrInsert(c.id, c)
												.then((saved) => {
													// if(cons.length < limit) {
													cons.push(saved);
													// }
													result.num_success++;
													if (sync) {
														self.synchonizeMessage(
															ctx,
															saved,
															job,
															done,
															next,
															100
														);
													}
												})
										);
									}
								});

								Promise.all(waits).then(() => {
									if (cons.length == limit && ctx) {
										self.sendNotificationToUser(
											ctx,
											"synchronizingProcess",
											{
												type: "success",
												msg: "Conversation saved",
											}
										);
										//self.sendNotificationToUser(ctx, "synchronizingProcess", {type : 'success', msg: "Page saved", });
									}
									if (
										conversations.paging &&
										conversations.paging.next &&
										sync
									) {
										model[2]["after"] =
											conversations.paging.cursors.after;

										toCall(token, model, self);
									} else {
										if (ctx) {
											self.sendNotificationToUser(
												ctx,
												"synchronizingProcess",
												{
													type: "success",
													msg: "Conversation saved",
												}
											);
											//self.sendNotificationToUser(ctx, "synchronizingProcess", {type : 'success', msg: "Page saved", });
										}
										result.date_end = new Date();
										result.last = _.last(
											conversations.data
										);
										done(result);
										resolve(cons);
									}
								});
							} else {
								result.date_end = new Date();
								result.error_logs.push("no-data");
								done(result);
								if (ctx) {
									self.sendNotificationToUser(
										ctx,
										"synchronizingProcess",
										{
											type: "success",
											msg: "Conversation finished",
										}
									);
								}
								resolve(cons);
							}
						})
						.catch((err) => {
							console.log(err);
							result.num_error++;
							result.error_logs.push(err);
							result.date_end = new Date();
							result.last = model;
							done(result);
							if (ctx) {
								self.sendNotificationToUser(
									ctx,
									"synchronizingProcess",
									{ type: "error", msg: err }
								);
							}
							resolve(cons);
						});
				}
			});
		},
		synchonizeMessage(ctx, con, job, done, next, limit) {
			let cons = [];
			return new Promise((resolve, reject) => {
				// let pageId = job.attrs.data.pageId;
				// let profileId = job.attrs.data.profileId;
				let result = {
					type: "MESSAGE",
					num_success: 0,
					num_error: 0,
					error_logs: [],
					last: {},
					date_start: new Date(),
					date_stop: new Date(),
					date_end: new Date(),
				};
				// let limit = limit ? limit : 100;
				let queryFields = {
					fields:
						"created_time,from,id,message,sticker,tags,to,attachments{id,link,name, image_data, file_url, video_data, mime_type},shares{id,description,link, name}",
					limit: limit,
				};
				let token = job.attrs.data.ptoken;
				let self = this;
				let model = ["/" + con.fb_id + "/messages", "GET", queryFields];
				toCall(token, model, self);
				function toCall(token, model, self) {
					self.fbgraphPageCall(token, model)
						.then((messages) => {
							if (messages && messages.data.length > 0) {
								let waits = []; // wait for saved

								_.forIn(messages.data, (c) => {
									c.conversation_id = con.fb_id;
									c.fb_page = con.fb_page;
									c.created_time = new Date(c.created_time);
									if (c.from.id != con.fb_page) {
										let phoneObj = self.feedService.getPhoneNumber(
											c.message
										);
										if (phoneObj.highlights.length > 0) {
											c.phones = phoneObj.phones;
											c.highlights = phoneObj.highlights;
											if (!con.hasPhone) {
												self.conversationService.updateFieldByConditions(
													ctx,
													{ fb_id: con.fb_id },
													{ hasPhone: true }
												);
											}
										}
									}
									waits.push(
										self.messageService
											.saveLocalMessage(ctx, c.from.id, c)
											.then((saved) => {
												// if(cons.length <= limit)
												cons.push(saved);
												result.num_success++;
											})
									);
								});
								Promise.all(waits).then(() => {
									if (
										messages.paging &&
										messages.paging.next
									) {
										model[2]["after"] =
											messages.paging.cursors.after;
										toCall(token, model, self);
									} else {
										result.date_end = new Date();
										result.last = _.last(messages.data);
										done(result);
										resolve(cons);
									}
								});
							} else {
								resolve(cons);
								result.error_logs.push("no-data");
								result.date_end = new Date();
								done(result);
							}
						})
						.catch((err) => {
							console.log(err);
							result.num_error++;
							result.error_logs.push(err);
							result.date_end = new Date();
							result.last = model;
							done(result);
							resolve(cons);
						});
				}
			});
		},
		synchonizePost(ctx, sync, job, done, next) {
			let cons = [];
			let result = {
				type: "POST",
				num_success: 0,
				num_error: 0,
				error_logs: [],
				last: {},
				date_start: new Date(),
				date_stop: new Date(),
				date_end: new Date(),
			};
			let limit = sync ? 1 : 100;
			return new Promise((resolve, reject) => {
				let pageId = job.attrs.data.pageId;
				// let profileId = job.attrs.data.profileId;
				//let model = job.attrs.data.conversationModel;
				let queryFields = {
					fields:
						"message,full_picture,created_time,from,permalink_url,shares, reactions{type,id,link,name,pic,profile_type,username,picture}" +
						", attachments,reactions.type(NONE).limit(0).summary(total_count).as(reactions_NONE),comments.limit(1){from}," +
						"reactions.type(LIKE).limit(0).summary(total_count).as(reactions_LIKE)," +
						"reactions.type(LOVE).limit(0).summary(total_count).as(reactions_LOVE)," +
						"reactions.type(WOW).limit(0).summary(total_count).as(reactions_WOW)," +
						"reactions.type(HAHA).limit(0).summary(total_count).as(reactions_HAHA)," +
						"reactions.type(SAD).limit(0).summary(total_count).as(reactions_SAD)," +
						"reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_ANGRY)," +
						"reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_THANKFUL)",
					limit: limit,
				};
				let token = job.attrs.data.ptoken;
				let self = this;
				let model = ["/" + pageId + "/posts", "GET", queryFields];
				toCall(token, model, self);
				function toCall(token, model, self) {
					self.fbgraphPageCall(token, model)
						.then((posts) => {
							if (posts && posts.data.length > 0) {
								let waits = []; // wait for saved

								_.forIn(posts.data, (c) => {
									let hasChild = false;
									if (c.comments) {
										hasChild = true;
										delete c.comments;
									}
									c.fb_page = pageId;
									c.count_comment = 0;
									c.count_phone = 0;
									waits.push(
										self.postService
											.saveOrInsert(c.id, c)
											.then((saved) => {
												// if(cons.length < limit)
												cons.push(saved);
												result.num_success++;
												if (hasChild) {
													self.synchonizeComment(
														ctx,
														saved,
														job,
														done,
														next,
														1
													);
												}
											})
									);
								});
								Promise.all(waits).then(() => {
									if (cons.length == limit && ctx) {
										self.sendNotificationToUser(
											ctx,
											"synchronizingProcess",
											{
												type: "success",
												msg: "Posts saved",
											}
										);
									}
									if (
										posts.paging &&
										posts.paging.next &&
										sync
									) {
										model[2]["after"] =
											posts.paging.cursors.after;
										toCall(token, model, self, limit);
									} else {
										if (ctx) {
											self.sendNotificationToUser(
												ctx,
												"synchronizingProcess",
												{
													type: "success",
													msg: "Posts saved",
												}
											);
										}
										result.date_end = new Date();
										result.last = _.last(posts.data);
										done(result);
										resolve(cons);
									}
								});
							} else {
								result.date_end = new Date();
								result.error_logs.push("no-data");
								done(result);
								if (ctx) {
									self.sendNotificationToUser(
										ctx,
										"synchronizingProcess",
										{
											type: "success",
											msg: "Posts finished",
										}
									);
								}
								resolve(cons);
							}
						})
						.catch((err) => {
							console.log(err);
							result.num_error++;
							result.error_logs.push(err);
							result.date_end = new Date();
							result.last = model;
							done(result);
							if (ctx) {
								self.sendNotificationToUser(
									ctx,
									"synchronizingProcess",
									{ type: "error", msg: err }
								);
							}
							resolve(cons);
						});
				}
			});
		},
		synchonizeComment(ctx, con, job, done, next, limit) {
			let cons = [];
			return new Promise((resolve, reject) => {
				let pageId = job.attrs.data.pageId;
				// let profileId = job.attrs.data.profileId;
				let result = {
					type: "COMMENT",
					num_success: 0,
					num_error: 0,
					error_logs: [],
					last: {},
					date_start: new Date(),
					date_stop: new Date(),
					date_end: new Date(),
				};
				// let limit = limit ? limit : 1;
				//let model = job.attrs.data.conversationModel;
				let queryFields = {
					fields:
						"message_tags,attachment,parent,message,comment_count,from,created_time,permalink_url,is_hidden,can_hide,comments.limit(1){from,can_hide}",
					limit: limit,
				};
				let token = job.attrs.data.ptoken;
				let self = this;
				let model = ["/" + con.fb_id + "/comments", "GET", queryFields];
				toCall(token, model, self);
				function toCall(token, model, self) {
					self.fbgraphPageCall(token, model)
						.then((conversations) => {
							if (
								conversations &&
								conversations.data.length > 0
							) {
								let waits = []; // wait for saved
								_.forIn(conversations.data, (c) => {
									c.fb_page = pageId;
									c.postId = con.fb_id;
									if (c.from) {
										c.fb_fromId = c.from.id;
										let s = _.cloneDeep(c.from);
										if (s.id != pageId) {
											let sid = s.id;
											// s.fb_pages = [pageId];
											self.contactService.updateOrInsert(
												sid,
												s,
												pageId
											);
										}
										c.customerIds = [s.id];
										let hasChild = false;
										if (
											c.comments &&
											c.comments.data.length > 0
										) {
											c.last_sent_by =
												c.comments.data[0].from;
											hasChild = true;
											delete c.comments;
										} else {
											c.last_sent_by = c.from;
										}
										c.updated_time = c.created_time;
										c.snippet = c.message;
										waits.push(
											self.conversationService
												.saveOrInsertFromComment(
													c.id,
													c
												)
												.then((saved) => {
													// if(cons.length < limit){
													cons.push(saved);
													// }
													let conversation = {
														snippet: c.message,
														updated_time:
															c.created_time,
													};
													result.num_success++;
													self.postService.updateOrInsertCount(
														{ fb_id: con.fb_id },
														{
															$inc: {
																count_comment: 1,
															},
														}
													);
													if (
														saved.from.id != pageId
													) {
														let phoneObj = self.feedService.getPhoneNumber(
															c.message
														);
														if (
															phoneObj.highlights
																.length > 0
														) {
															self.postService.updateOrInsertCount(
																{
																	fb_id:
																		con.fb_id,
																},
																{
																	$inc: {
																		count_phone:
																			phoneObj
																				.highlights
																				.length,
																	},
																}
															);
															c.phones =
																phoneObj.phones;
															c.highlights =
																phoneObj.highlights;
															if (!con.hasPhone) {
																conversation.hasPhone = true;
															}
														}
														conversation.$addToSet = {
															customerIds: s.id,
														};
													}
													self.conversationService.updateFieldByConditions(
														ctx,
														{ fb_id: saved.fb_id },
														conversation
													);
													c.conversation_id =
														saved.fb_id;
													c.fb_page = con.fb_page;
													c.created_time = new Date(
														c.created_time
													);
													self.messageService
														.saveLocalComment(
															c.id,
															c.from.id,
															c
														)
														.then((csaved) => {
															if (hasChild) {
																self.synchonizeReply(
																	ctx,
																	saved,
																	job,
																	done,
																	csaved,
																	next,
																	100
																);
															}
														});
												})
										);
									}
								});
								Promise.all(waits).then(() => {
									if (
										conversations.paging &&
										conversations.paging.next
									) {
										model[2]["after"] =
											conversations.paging.cursors.after;

										toCall(token, model, self);
									} else {
										result.date_end = new Date();
										result.last = _.last(
											conversations.data
										);
										done(result);
										resolve(cons);
									}
								});
							} else {
								result.date_end = new Date();
								result.error_logs.push("no-data");
								done(result);
								resolve(cons);
							}
						})
						.catch((err) => {
							console.log(err);
							result.num_error++;
							result.error_logs.push(err);
							result.date_end = new Date();
							result.last = model;
							done(result);
							resolve(cons);
						});
				}
			});
		},

		synchonizeReply(ctx, con, job, done, parent, next, limit) {
			let cons = [];
			return new Promise((resolve, reject) => {
				let pageId = job.attrs.data.pageId;
				// let profileId = job.attrs.data.profileId;
				let result = {
					type: "REPLY",
					num_success: 0,
					num_error: 0,
					error_logs: [],
					last: {},
					date_start: new Date(),
					date_stop: new Date(),
					date_end: new Date(),
				};
				// let limit = limit ? limit : 100;
				//let model = job.attrs.data.conversationModel;
				let queryFields = {
					fields:
						"message_tags,attachment,parent,message,comment_count,from,created_time,permalink_url,is_hidden,can_hide",
					limit: limit,
				};

				let token = job.attrs.data.ptoken;
				let self = this;

				let model = [
					"/" + parent.fb_id + "/comments",
					"GET",
					queryFields,
				];
				toCall(token, model, self);

				function toCall(token, model, self) {
					self.fbgraphPageCall(token, model)
						.then((messages) => {
							if (messages && messages.data.length > 0) {
								let waits = []; // wait for saved
								_.forIn(messages.data, (c) => {
									c.conversation_id = con.fb_id;
									c.fb_page = con.fb_page;
									c.created_time = new Date(c.created_time);
									let s = _.cloneDeep(c.from);
									if (s.id != con.fb_page) {
										let sid = s.id;
										// s.fb_pages = [pageId];
										self.contactService.updateOrInsert(
											sid,
											s,
											pageId
										);
									}
									let conversation = {
										snippet: c.message,
										updated_time: c.created_time,
									};
									self.postService.updateOrInsertCount(
										{ fb_id: con.fb_postId },
										{ $inc: { count_comment: 1 } }
									);
									if (c.from.id != pageId) {
										let phoneObj = self.feedService.getPhoneNumber(
											c.message
										);
										if (phoneObj.highlights.length > 0) {
											self.postService.updateOrInsertCount(
												{ fb_id: con.fb_postId },
												{
													$inc: {
														count_phone:
															phoneObj.highlights
																.length,
													},
												}
											);
											c.phones = phoneObj.phones;
											c.highlights = phoneObj.highlights;
											if (!con.hasPhone) {
												conversation.hasPhone = true;
											}
										}
										conversation.$addToSet = {
											customerIds: s.id,
										};
									}

									self.conversationService.updateFieldByConditions(
										ctx,
										{ fb_id: con.fb_id },
										conversation
									);
									waits.push(
										self.messageService
											.saveLocalComment(
												c.id,
												c.from.id,
												c
											)
											.then((saved) => {
												if (cons.length <= limit) {
													cons.push(saved);
												}
												result.num_success++;
											})
									);
								});
								Promise.all(waits).then(() => {
									if (
										messages.paging &&
										messages.paging.next
									) {
										model[2]["after"] =
											messages.paging.cursors.after;
										toCall(token, model, self);
									} else {
										result.date_end = new Date();
										result.last = _.last(messages.data);
										done(result);
										resolve(cons);
									}
								});
							} else {
								result.error_logs.push("no-data");
								result.date_end = new Date();
								done(result);
								resolve(cons);
							}
						})
						.catch((err) => {
							console.log(err);
							result.num_error++;
							result.error_logs.push(err);
							result.date_end = new Date();
							result.last = model;
							done(result);
							resolve(cons);
						});
				}
			});
		},

		hideOrUnhideComment(locals, page_id, comment_id, is_hidden) {
			return new Promise((resolve, reject) => {
				let peopleService = this.context.services("people");
				peopleService
					.getPageEditor(page_id)
					.then((user) => {
						this.fbgraphPagePreCall(
							locals,
							user.socialLinks.facebook,
							page_id,
							["/" + comment_id, "POST", { is_hidden: is_hidden }]
						)
							.then((res) => {
								resolve(res);
								// logger.debug(res);
							})
							.catch((err) => {
								reject(err);
								// logger.error(err);
							});
					})
					.catch((err) => {
						reject(err);
					});
			});
		},
		likeOrUnlikeComment(locals, page_id, comment_id, is_like) {
			return new Promise((resolve, reject) => {
				let peopleService = this.context.services("people");
				peopleService
					.getPageEditor(page_id)
					.then((user) => {
						this.fbgraphPagePreCall(
							locals,
							user.socialLinks.facebook,
							page_id,
							[
								"/" + comment_id + "/likes",
								is_like ? "POST" : "DELETE",
							]
						)
							.then((res) => {
								resolve(res);
								// logger.debug(res);
							})
							.catch((err) => {
								reject(err);
								// logger.error(err);
							});
					})
					.catch((err) => {
						reject(err);
					});
			});
		},
		synchonizeDb(result, isSynchonize, pageId, conversationId) {
			console.log("=========synchonizeDb============", result);
			if (isSynchonize) {
				if (result.conversations)
					_.forIn(result.conversations.data, (c) => {
						let senders = c.senders;
						_.forIn(senders.data, (s) => {
							let id = s.id;
							//delete s.id;
							// s.fb_pages = [pageId];
							this.contactService.updateOrInsert(id, s, pageId);
						});

						let id = c.id;
						//delete c.id;
						//delete c.messages;
						c.fb_page = pageId;
						c.fb_type = "INBOX";
						let messages = c.messages;
						if (c.senders)
							c.customerIds = _.map(c.senders.data, (s) => s.id);
						if (
							messages &&
							messages.data &&
							messages.data.length >= 1
						) {
							c.fb_fromId = messages.data[0].from.id;
							c.from = messages.data[0].from;
						}
						this.conversationService
							.updateOrInsert(id, c)
							.then((con) => {
								_.forIn(messages.data, (m) => {
									let id = m.id;
									//m.thread_id = con.fb_id;
									m.conversation_id = con.fb_id;
									//delete m.id;
									this.messageService.updateOrInsert(id, m);
								});
							})
							.catch((err) => {
								console.log(err);
							});
					});
				if (result.senders)
					_.forIn(result.senders.data, (s) => {
						let id = s.id;
						//delete s.id;
						// s.fb_pages = [pageId];
						this.contactService.updateOrInsert(id, s, pageId);
					});
				if (result.participants)
					_.forIn(result.participants.data, (s) => {
						let id = s.id;
						//delete s.id;
						// s.fb_pages = [pageId];
						this.contactService.updateOrInsert(id, s, pageId);
					});
				if (result.messages)
					_.forIn(result.messages.data, (m) => {
						let id = m.id;
						m.thread_id = conversationId;
						this.messageService.updateOrInsert(id, m);
						//logger.info(m);
						if (m.from) {
							let s = m.from;
							// s.fb_pages = [pageId];
							this.contactService.updateOrInsert(s.id, s, pageId);
						}
						if (m.to) {
							_.forEach(m.to.data, (s) => {
								// s.fb_pages = [pageId];
								this.contactService.updateOrInsert(
									s.id,
									s,
									pageId
								);
							});
						}
						// if(m.message && m.from.id !== pageId && utils.isPhoneNumber(m.message)){
						// 	this.contactService.updatePhone(m.from.id, utils.messageToPhone(m.message));
						// }
					});
				if (result.posts)
					_.forIn(result.posts.data, (p) => {
						let id = p.id;
						p.fb_page = pageId;
						this.postService.updateOrInsert(id, p);
						let comments = p.comments;
						if (comments) {
							_.forEach(comments.data, (c) => {
								c.fb_page = pageId;
								//c.fb_id = c.id;
								c.postId = p.id;
								let childComment = c.comments;

								if (c.from) c.customerIds = [c.from.id];
								this.conversationService
									.updateOrInsertFromComment(c.id, c)
									.then((conversation) => {
										c.conversation_id = conversation.fb_id;

										this.commentService.updateOrInsert(
											c.id,
											c
										);
										if (c.from) {
											let s = c.from;
											// s.fb_pages = [pageId];
											this.contactService.updateOrInsert(
												s.id,
												s,
												pageId
											);
										}
										if (childComment) {
											_.forEach(
												childComment.data,
												(cc) => {
													cc.conversation_id =
														conversation.fb_id;
													this.commentService.updateOrInsert(
														cc.id,
														cc
													);
													if (cc.from) {
														let s = cc.from;
														// s.fb_pages = [pageId];
														this.contactService.updateOrInsert(
															s.id,
															s,
															pageId
														);
													}
												}
											);
										}
									})
									.catch((err) => {
										console.log(err);
									});
							});
						}
					});
			}
		},

		async fbgraphPagePreCall(locals, userId, pageId, reqParams) {
			const access_token = locals.pageTokenCache.get(
				userId + "_" + pageId
			);

			try {
				return await this.fbgraphPageCall(access_token, reqParams);
			} catch (err) {
				if (err.error.code == 104 || err.error.code == 190) {
					const new_access_token = await this.getPageToken(
						locals,
						userId,
						pageId
					);
					const pTokenModel = {
						type: "p",
						userId: userId,
						key: userId + "_" + pageId,
						token: new_access_token,
					};

					FbToken.findOneAndUpdate(
						{
							key: pTokenModel.key,
							type: pTokenModel.type,
						}, // find a document with that filter
						pTokenModel, // document to insert when nothing was found
						{
							upsert: true,
							new: true,
							runValidators: true,
						}
					);

					return await this.fbgraphPageCall(
						new_access_token,
						reqParams
					);
				} else {
					throw err;
				}
			}
		},
		/**
		 * Interact graph api to get or post content
		 * @param  {Context} ctx   Context of request
		 * @param  {Number} value  New value
		 * @return {Promise}       Promise with the facebook value
		 */
		graph(ctx) {
			return new Promise((resolve, reject) => {
				let reqParams = ctx.req.body;
				FB.setAccessToken(ctx.user.profile.access_token);
				FB.api(...reqParams, (response) => {
					if (!response || response.error) {
						if (
							response &&
							(response.error.code == 104 ||
								response.error.code == 190)
						)
							// this.notifyModelChanges(
							// 	ctx,
							// 	"tokenExpired",
							// 	response.error
							// );
							// let fbctx = CONTEXT.CreateToServiceInit(this.context.services("notifications"));
							this.sendNotificationToUser(
								ctx,
								"tokenExpired",
								response.error
							);
						reject(response.error);
					} else {
						resolve(response);
					}
				});
			});
		},
		graphFbModel(ctx) {
			return new Promise((resolve, reject) => {
				console.log("graphFbModel==============", ctx.req.body);
				//debugger
				let reqParams = ctx.req.body.fbModel;
				let pageId = ctx.params.pageId;
				FB.setAccessToken(
					ctx.app.locals.pageTokenCache.get(
						ctx.user.socialLinks.facebook + "_" + pageId
					)
				);
				FB.api(...reqParams, (response) => {
					if (!response || response.error) {
						if (
							response &&
							(response.error.code == 104 ||
								response.error.code == 190)
						)
							this.sendNotificationToUser(
								ctx,
								"tokenExpired",
								response.error
							);
						reject(response.error);
					} else {
						resolve(response);
					}
				});
			});
		},
		async getPageToken(locals, userId, pageId) {
			const response = await fbGraphPageCallAsync(
				locals.userTokenCache.get(userId),
				["me", "GET", { fields: "accounts{id, access_token}" }]
			);

			if (!response || response.error) {
				throw response;
			}

			if (response.accounts && response.accounts.data) {
				_.forIn(response.accounts.data, (object) => {
					locals.pageTokenCache.set(
						userId + "_" + object.id,
						object.access_token
					);
				});
			}

			return locals.pageTokenCache.get(userId + "_" + pageId);
		},

		fbgraphPageCall(access_token, reqParams) {
			return new Promise((resolve, reject) => {
				FB.setAccessToken(access_token);
				FB.api(...reqParams, (response) => {
					if (!response || response.error) {
						reject(response);
					} else {
						resolve(response);
					}
				});
			});
		},

		async graphConversationsByPage(ctx) {
			const { locals } = ctx.app;
			const { conversationId, pageId, isSynchonize } = ctx.req.body;
			const reqParams = ctx.req.body.fbModel;

			try {
				const response = await this.fbgraphPagePreCall(
					locals,
					ctx.user.socialLinks.facebook,
					pageId,
					reqParams
				);

				this.synchonizeDb(
					response,
					isSynchonize,
					pageId,
					conversationId
				);

				return response;
			} catch (err) {
				if (err && (err.error.code == 104 || err.error.code == 190)) {
					// this.notifyModelChanges(
					// 	ctx,
					// 	"tokenExpired",
					// 	err.error
					// );
					// let fbctx = CONTEXT.CreateToServiceInit(this.context.services("notifications"));
					this.sendNotificationToUser(ctx, "tokenExpired", err.error);
				}

				throw err;
			}
		},
	},

	/**
	 * Initialize this service. It will be called when server load this service.
	 * The `ctx` contains the references of `app` and `db`
	 * @param  {Context} ctx   Context of initialization
	 */
	init(ctx) {
		// Call when start the service
		//logger.info("Initialize facebook service!");
		this.peopleService = ctx.services("people");
		this.contactService = ctx.services("contacts");
		this.messageService = ctx.services("messages");
		this.pageService = ctx.services("pages");
		this.postService = ctx.services("posts");
		this.conversationService = ctx.services("conversations");
		this.notificationService = ctx.services("notifications");
		this.commentService = ctx.services("comments");
		this.feedService = ctx.services("feeds");
		this.context = ctx;
		this.scheduleService = agenda;
		let ctrl = this;
		// this.scheduleService.define("pageSync", (job, done) =>{
		// 	try {
		// 		let data = job.attrs.data;
		// 		if(data.type == 'CONVERSATION'){
		// 			ctrl.syncConversations(data.pageId, data.model, data.user_fb_id, data.result, true);
		// 		}else if(data.type == 'POST'){
		// 			ctrl.syncPostAndComments(data.pageId, data.model, data.user_fb_id, data.result, true);
		// 		}
		// 	} catch (error) {
		// 		logger.error("Job running exception!");
		// 		logger.error(error);
		// 		return done(error);
		// 	}
		// });
	},

	/**
	 * Websocket options
	 */
	socket: {
		// Namespace of socket
		//nsp: "/facebook",

		// Fired after a new socket connected
		afterConnection(socket, io) {
			//logger.info("facebook afterConnection");
			// We sent the facebook last value to the client
			// socket.emit("/messsage/changed", store.total);
		},
	},

	/**
	 * Define GraphQL queries, types, mutations.
	 * This definitions enable to access this service via graphql
	 */
	graphql: {
		query: `
		total: Int
		`,

		types: "",

		mutation: `
		totalCreate(value: Int!): Int
		totalReset: Int
		totalIncrement: Int
		totalDecrement: Int
		`,

		resolvers: {
			Query: {
				total: "find",
			},

			Mutation: {
				totalCreate: "create",
				totalReset: "reset",
				totalIncrement: "increment",
				totalDecrement: "decrement",
			},
		},
	},
};

/*
## GraphiQL test ##

# Get value of facebook
query getfacebook {
  facebook
}

# Save a new facebook value
mutation savefacebook {
  facebookCreate(value: 12)
}

# Reset the facebook
mutation resetfacebook {
  facebookReset
}

# Increment the facebook
mutation incrementfacebook {
  facebookIncrement
}

# Decrement the facebook
mutation decrementfacebook {
  facebookDecrement
}


*/
