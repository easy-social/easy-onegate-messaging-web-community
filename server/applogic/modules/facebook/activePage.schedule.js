const agenda = require("../../../core/agenda");
const Services = require("../../../core/services");
const { fbGraphPageCallAsync } = require("./util");

agenda.define("facebook.activePage", async (job) => {
	console.log("data", job.attrs);

	const {
		pageId,
		userId,
		ptoken,
		isSynchonizeCon,
		isSynchonizePost,
		isSynchonize,
	} = job.attrs.data;

	// sync page
	const syncPage = async () => {
		const settingService = Services.get("settings");

		settingService.updateOrInsertBy(
			{
				fb_id: pageId,
				type: "generalSetting",
			},
			{
				fb_id: pageId,
				type: "generalSetting",
				auto_create_order: false,
				auto_hide_comment: false,
				auto_hide_comment_with_mobile: false,
				auto_like: false,
				auto_next_conversation_when_enter_twice: false,
				auto_unhide_comment: false,
				auto_unhide_comment_time: {
					name: "1 ngày",
					value: 24,
				},
				description: "",
				fb_pages: [],
				fix_undisplay_photo: false,
				keep_conv_status_for_3rd_messages: false,
				notification: true,
				show_cached_avatar: false,
				sound: true,
				status: 1,
				sync_inbox_with_facebook: false,
				templates: {
					attachments: [],
				},
				unread_first: true,
			}
		);

		settingService.updateOrInsertBy(
			{
				fb_id: pageId,
				type: "tagsSetting",
			},
			{
				fb_id: pageId,
				type: "tagsSetting",
				description: "",
				fb_pages: [],
				full_tag: false,
				multi_tag: true,
				status: 1,
				tags: [
					{
						id: new Date().getTime() - 4,
						name: "Inquiry",
						backgroundcolor: "rgba(75, 85, 119, 1)",
					},
					{
						id: new Date().getTime() - 3,
						name: "Issue",
						backgroundcolor: "rgba(130, 43, 161, 1)",
					},
					{
						id: new Date().getTime() - 2,
						name: "Urgent",
						backgroundcolor: "rgba(13, 90, 255, 1)",
					},
					{
						id: new Date().getTime() - 1,
						name: "Reply Later",
						backgroundcolor: "rgba(0, 147, 68, 1)",
					},
				],
				templates: {
					attachments: [],
				},
			}
		);

		settingService.updateOrInsertBy(
			{ fb_id: pageId, type: "replySetting" },
			{
				fb_id: pageId,
				type: "replySetting",
				active_auto_reply: false,
				auto_reply_end_time: null,
				auto_reply_start_time: null,
				auto_reply_templates: [],
				description: "",
				fb_pages: [],
				full_tag: false,
				multi_tag: false,
				status: 1,
				templates: [],
			}
		);
		settingService.updateOrInsertBy(
			{ fb_id: pageId, type: "assigningSetting" },
			{
				fb_id: pageId,
				type: "assigningSetting",
				TypeSplit: "turn_off",
				description: "",
				fb_pages: [],
				hard_round_robin_assign_base_on_online_status: false,
				hard_round_robin_assign_to_other_user_online: false,
				hard_round_robin_only_assign_to_online_user: false,
				hard_round_robin_reassign_if_priority_online: false,
				hard_round_robin_reassign_if_unread_after_x_min: false,
				hard_round_robin_show_all_convs_if_user_not_in_list: false,
				status: 1,
				templates: {
					attachments: [],
				},
			}
		);

		const result = {
			type: "PAGE",
			num_success: 0,
			num_error: 0,
			error_logs: [],
			last: {},
			date_start: new Date(),
			date_stop: new Date(),
			date_end: new Date(),
		};

		try {
			const model = [
				`/${pageId}/subscribed_apps`,
				"POST",
				{
					fields: "id,name,link,about,picture",
					subscribed_fields:
						"feed,mention, message_mention, messages, message_reactions, messaging_account_linking, messaging_checkout_updates, message_echoes, message_deliveries, messaging_game_plays, messaging_optins, messaging_optouts, messaging_payments, messaging_postbacks, messaging_pre_checkouts, message_reads, messaging_referrals, messaging_handovers, messaging_policy_enforcement, messaging_page_feedback, messaging_appointments,  publisher_subscriptions",
				},
			];

			const pageService = Services.get("pages");
			const page = await fbGraphPageCallAsync(ptoken, model);

			try {
				const saved = await pageService.updateOrInsert(page.id, {
					...page,
					author: userId,
					member_count: 1,
					isActive: true,
					fb_memberIds: [userId],
				});

				++result.num_success;
				result.date_end = new Date();
				result.last = saved;
			} catch (err) {
				result.date_end = new Date();
				result.last = page;
				++result.num_error;
				result.error_logs.push(err);
				throw err;
			}
		} catch (err) {
			result.date_end = new Date();
			result.last = null;
			++result.num_error;
			result.error_logs.push(err);

			console.log("--------------------------------");
			console.log("err", err);
			console.log("--------------------------------");
			throw err;
		}

		return result;
	};

	// sync conv
	const syncConv = async () => {
		if (!isSynchonize) return undefined;
		if (!isSynchonizeCon) return undefined;

		agenda
			.create("facebook.syncConversation", {
				pageId,
				ptoken,
				sync: true,
			})
			.save();

		// wait
		const result = await Promise.race([
			() =>
				new Promise((resolve) =>
					setTimeout(
						() =>
							resolve({
								type: "CONVERSATION",
								num_success: 0,
								num_error: 0,
								error_logs: [],
								last: {},
								executing: true,
								date_start: new Date(),
								date_stop: new Date(),
								date_end: new Date(),
							}),
						30000
					)
				), // 30s
			() =>
				new Promise((resolve) => {
					agenda.define(
						`facebook.syncConversation(page=${pageId})`,
						(job) => {
							job.remove();
							resolve(job.attrs.data);
						}
					);
				}),
		]);

		return result;
	};

	// sync post
	const syncPost = async () => {
		if (!isSynchonize) return;
		if (!isSynchonizePost) return;

		await agenda
			.create("facebook.syncPost", {
				pageId,
				ptoken,
			})
			.save();
	};

	const syncPageResult = await syncPage();
	const [syncConvResult] = await Promise.all([syncConv(), syncPost()]);

	await agenda
		.create(`facebook.activePage(${pageId})`, {
			PAGE: syncPageResult,
			CONVERSATION: syncConvResult,
		})
		.save();

	job.remove();
});
