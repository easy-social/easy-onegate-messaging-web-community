"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let _ 				= require("lodash");

let db	    		= require("../../../../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../../../../libs/hashids")("messages");
let autoIncrement 	= require("mongoose-auto-increment");
let ObjectId = Schema.ObjectId;
let MessageSchema = require("../../comment/models/comment");
// let schemaOptions = {
// 	timestamps: true,
// 	toObject: {
// 		virtuals: true
// 	},
// 	toJSON: {
// 		virtuals: true
// 	},
// 	strict: false
// };

// let MessageSchema = new Schema({
// 	fb_id: {
// 		type: String,
// 		trim: true,
// 		unique: true
// 	},

// 	title: {
// 		type: String,
// 		trim: true
// 	},
// 	content: {
// 		type: String,
// 		trim: true
// 	},
// 	author: {
// 		type: String,
// 		//required: "Please fill in an author ID",
// 		ref: "User"
// 	},
// 	views: {
// 		type: Number,
// 		default: 0
// 	},
// 	voters: [{
// 		type: String,
// 		ref: "User"
// 	}],
// 	votes: {
// 		type: Number,
// 		default: 0
// 	},
// 	editedAt: {
// 		type: Date
// 	},
// 	metadata: {}

// }, schemaOptions);

// MessageSchema.virtual("code").get(function() {
// 	return this.encodeID();
// });

// // MessageSchema.plugin(autoIncrement.plugin, {
// // 	model: "Message",
// // 	startAt: 1
// // });

// MessageSchema.methods.encodeID = function() {
// 	return hashids.encodeHex(this._id);
// };

// MessageSchema.methods.decodeID = function(code) {
// 	return hashids.decodeHex(code);
// };

/*
MessageSchema.static("getByID", function(id) {
	let query;
	if (_.isArray(id)) {
		query = this.collection.find({ _id: { $in: id} });
	} else
		query = this.collection.findById(id);

	return query
		.populate({
			path: "author",
			select: ""
		})
});*/

let Message = mongoose.model("Message", MessageSchema);

module.exports = Message;
