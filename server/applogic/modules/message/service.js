"use strict";

let logger = require("../../../core/logger");
let C = require("../../../core/constants");
let _ = require("lodash");
let Message = require("../../modules/comment/models/comment")("Message");
let Attachment = require("./models/attachments");
const formidable = require("formidable");
let {storage, config} = require("../../../core/firebaseService");
let fs = require("fs");
let request = require("request");
let path = require("path");

module.exports = {
	settings: {
		name: "messages",
		version: 1,
		namespace: "messages",
		rest: true,
		ws: true,
		graphql: true,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: Message,

		modelPropFilter: null, //"code title content author votes voters views createdAt editedAt",

		modelPopulates: {
			author: "people",
			voters: "people"
		}
	},

	actions: {
		hideOrUnhideComment: {
			cache: false,
			handler(ctx) {
				return this.facebookService.hideOrUnhideComment(ctx.app.locals, ctx.params.pageId, ctx.params.commentId, ctx.params.is_hidden);
			}
		},
		likeOrUnlikeComment: {
			cache: false,
			handler(ctx) {
				return this.facebookService.likeOrUnlikeComment(ctx.app.locals, ctx.params.pageId, ctx.params.commentId, ctx.params.is_like);
			}
		},
		updateConversation: {
			handler(ctx) {

				return this.conversationService.assigEmployee(ctx.params.conversationId, ctx.params);

			}
		} ,
		loadAttachFiles: {
			handler(ctx) {

				let filter = {is_removed: false};
				if(ctx.params.condition)
					filter = _.assign(filter,ctx.params.condition);
				let query = Attachment.find(filter);

				return ctx
					.queryPageSort(query)
					.exec()
					.then(docs => {
						return this.toJSON(docs);
					})
					.then(json => {
						return this.populateModels(json);
					});
			}

		},
		upload : {
			handler(ctx) {
				let ctrl = this;
				return new Promise((resolve, reject) => {
					let fs = require("fs");
					// var dir = __dirname + '../../../../../data';
					// if (!fs.existsSync(dir)){
					// 	fs.mkdirSync(dir);
					// }
					let form = new formidable.IncomingForm();
					form.multiples = false;
					//form.uploadDir = __dirname + '../../../../../data';
					form.keepExtensions = true;
					let page = {};
					let promises = [];
					let savedFiles = [];
					let errs = [];
					form.parse(ctx.req,  (err, fields, files) => {
						if(err) reject(err);
						 console.log(".parse", fields);
						 page = fields;
						 //const bucket = storage.bucket(config.storageBucket);
						 _.forEach(files, file=>{


							let uploaded = _.cloneDeep(file);
							let filter = {name: file.name, fb_page: page.id};
							let fileName = path.parse(uploaded.path).base;
							let publicUrl = `https://storage.googleapis.com/${config.storageBucket}/${fileName}`;
							uploaded.gurl = publicUrl;
							uploaded.localurl = file.path;
							uploaded.url = publicUrl;
							uploaded.author	= ctx.user.id;
							uploaded.is_removed = false;
							uploaded.fb_page = page.id;
							if(page.folder && page.folder!='undefined')
								uploaded.$push = {folder_ids: page.folder}
							delete uploaded._writeStream;

							//uploaded.page_id  = page.id;
							promises.push(
								Attachment.findOneAndUpdate(filter, uploaded, { upsert: true, new: true}).exec().then(docs=>{
									console.log("saved " + docs.name);
									savedFiles.push({attachment_id:docs.id, url: publicUrl});

								//resolve(this.toJSON(docs));
								}).catch(err=>{
									console.log("save err " + err);
									errs.push(err);
								}));
							 //file.page_Id = page.Id;
						 });

						//resolve(util.inspect({fields: fields, files: files}));
						return Promise.all(promises).then(()=>{
							if(errs.length > 0) reject(errs);
							resolve(savedFiles);
						});
					});

					// form.on('fileBegin', function (name, file){
					// 	console.log("fileBegin ",name);
					// 	file.path = __dirname + '../../../../../data/' + file.name;
					// });

					form.on("file", function (name, file){
						let imageName = path.parse(file.path).base;


						const bucket = storage.bucket(config.storageBucket);
						bucket.upload(file.path, (err, uploaded)=>{
							if (err) {
								console.log(err);
							} else {
								const publicUrl = `https://storage.googleapis.com/${bucket.name}/${imageName}`;
										 bucket.file(imageName).makePublic(()=>{
											console.log(publicUrl);
										 });
							}

						});

					});

					// form.on('end', ()=>{
					// 	console.log("all file saved");
					// 	return Promise.all(promises).then(()=>{
					// 		if(errs.length > 0) reject(errs);
					// 		resolve(savedFiles);
					// 	})
					// })

				});
			}
		},
		updateFile: {
			handler(ctx) {
				// return new Promise((resolve, reject) => {
					// let filter = {name: ctx.params.name, fb_page: ctx.params.fb_page};
					// let uploaded = ctx.params.condition
					// Attachment.update(filter, uploaded, { upsert: true, new: false}).exec().then(docs=>{
					// 	return this.toJSON(docs);
					// }).then(json => {
					// 	return this.populateModels(json);
					// });
					var model = {}
					if (ctx.params.is_favorite != null)
						model.is_favorite = ctx.params.is_favorite;

					if (ctx.params.folder != null && ctx.params.addFolder) {
						model.$addToSet = {folder_ids: ctx.params.folder};
					} else if(ctx.params.folder != null) {
						model.$pullAll = {folder_ids: [ctx.params.folder]};
					}

					model.editedAt = Date.now();
					return Attachment
						.findByIdAndUpdate(ctx.params._id, model, { upsert: true, new: true})
						.exec()
						.then(doc => {

							return this.toJSON(doc);
						})
						.then(json => {
							return this.populateModels(json);
						})
						.then(json => {
							this.notifyModelChanges(ctx, "updated", json);
							return json;
						});
				// });
			}
		},
		download : {
			handler(ctx) {
				return new Promise((resolve, reject) => {
					Attachment.findById().exec().then(docs=>{
						this.toJSON(docs);
					}).then(json =>{
						resolve(json);
					}).catch(err=>{
						reject(err);
					});
				});
			}
		},
		removeFile : {
			//permission: C.PERM_OWNER,
			handler(ctx) {
				let filter = {};
				if(ctx.params.condition)
					filter = _.assign(filter,ctx.params.condition);
				let query = Attachment.findOne(filter);
				return 	query
					.exec()
					.then(doc => {
						doc.is_removed = true;
						doc.edited_by = ctx.user.id;
						doc.editedAt = Date.now();
						return doc.save();

					}).then(doc=>{
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					})
					.then(json => {
						this.notifyModelChanges(ctx, "removed", json);
						return json;
					});
			}
		},
		send: {
			handler(ctx) {
				return new Promise((resolve, reject) => {
					if (
						ctx.req.body &&
						ctx.req.body.fbModel &&
						ctx.req.body.pageId
					) {
						this.facebookService
							.fbgraphPagePreCall(ctx.app.locals, ctx.user.socialLinks.facebook, ctx.req.body.pageId, ctx.req.body.fbModel)
							.then(msgout => {
								resolve({id: msgout.message_id});
							})
							.catch(err => {
								reject(err);
							});
					} else {
						reject(ctx.t("app:NoContentToSend"));
					}
				});
			}
		},
		sendAndSave: {
			handler(ctx) {
				return new Promise((resolve, reject) => {
					if (
						ctx.req.body &&
						ctx.req.body.fbModel &&
						ctx.req.body.pageId
					) {
						let fbModel = _.cloneDeep(ctx.req.body.fbModel);

						//let file  = 'C:/projects/mantis-cloud/data/khoa-hoc-nau-an-chay-04.jpg';
						let ptoken = ctx.app.locals.pageTokenCache.get(
						ctx.user.socialLinks.facebook + "_" + ctx.req.body.pageId
						);
						let fbUrl = "https://graph.facebook.com/v7.0" + fbModel[0] + "?access_token="+ ptoken;
						this.sendImageMessage(ctx.req.body.recipient, ctx.req.body.text, undefined, fbUrl)
							.then(msgout => {
								let msg = _.cloneDeep(ctx.req.body.message);
								msg.fb_id = msgout.id?msgout.id:msgout.message_id;
								msg.author = ctx.user.id;
								msg.delivery = "sent";
								msg.created_by = ctx.req.body.created_by
								resolve(msg);
								this.createLocalMessage(ctx, msg.from.id, msg)
									.then(lastmsg => {
									})
									.catch(err => {
										logger.error(err);
									});
							})
							.catch(err => {
								if(err && err.message == "(#10903) This user cant reply to this activity" && ctx.req.body.recipient.comment_id){
									this.commentService.updateOrInsert(ctx.req.body.recipient.comment_id, {can_inbox: false}, );
									reject(err);
								} else {
									reject(err);
								}
							});
					} else {
						reject(ctx.t("app:NoContentToSend"));
					}
				});
			}
		},
		sendAndSaveComment: {
			handler(ctx) {
				return new Promise((resolve, reject) => {
					if (
						ctx.req.body &&
						ctx.req.body.fbModel &&
						// ctx.req.body.message &&
						ctx.req.body.pageId
					) {
						this.facebookService
							.fbgraphPagePreCall(ctx.app.locals, ctx.user.socialLinks.facebook, ctx.req.body.pageId, ctx.req.body.fbModel)
							.then(msgout => {
								if(ctx.req.body.message){
									let msg = _.cloneDeep(ctx.req.body.message);
									msg.fb_id = msgout.id;
									msg.author = ctx.user.id;
									msg.created_by = ctx.req.body.created_by
									msg.delivery = "sent";
									resolve(msg);
									this.createLocalComment(ctx, msg.from.id, msg)
										.then(lastmsg => {
											resolve(lastmsg);
										})
										.catch(err => {
											// reject(err);
										});
								}else{
									resolve(msgout);
								}
							})
							.catch(err => {
								reject(err);
							});
					} else {
						reject(ctx.t("app:NoContentToSend"));
					}
				});
			}

		},
		facebook: {
			handler(ctx) {
				return new Promise((resolve, reject) => {
					this.facebookService
						.graph(ctx)
						.then(result => {
							if (result && result.data) {
								_.forEach(result.data, message => {
									this.updateOrInsert(
										ctx,
										message.id,
										message
									);
								});
							}
							resolve(result);
						})
						.catch(err => {
							reject(err);
						});
				});
				//return this.facebookService.graph(ctx);
			}
		},
		facebookExt: {
			handler(ctx) {
				return new Promise((resolve, reject) => {
					this.facebookService
						.graphFbModel(ctx)
						.then(result => {
							if (result && result.data) {
								_.forEach(result.data, message => {
									console.log(
										"message fb ======================",
										message,
										ctx.req.body
									);
									this.updateOrInsert(
										ctx,
										message.id,
										message
									);
								});
							}
							resolve(result);
						})
						.catch(err => {
							reject(err);
						});
				});
			}
		},
		sendMediaMessage: {
			cache: false,
			permission: C.PERM_LOGGEDIN,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					if (
						ctx.req.body &&
						ctx.req.body.fbModel &&
						ctx.req.body.pageId
					) {
						let fbModel = _.cloneDeep(ctx.req.body.fbModel);

						//let file  = 'C:/projects/mantis-cloud/data/khoa-hoc-nau-an-chay-04.jpg';
						let ptoken = ctx.app.locals.pageTokenCache.get(
						ctx.user.socialLinks.facebook + "_" + ctx.req.body.pageId
						);
						let fbUrl = "https://graph.facebook.com/v7.0" + fbModel[0] + "?access_token="+ ptoken;
						Attachment.findById({_id: ctx.params.attachment_id}).exec().then(docs=>{
							let file = docs.path;
							if (!fs.existsSync(file)){
								reject(ctx.t("app:NoFileToSend"));
							}
							this.sendImageMessage(ctx.req.body.recipient, ctx.req.body.message, file, fbUrl).then(msgout=>{
								let msg =  _.cloneDeep(msgout);
								msg.fb_id = msgout.message_id;
								msg.author = ctx.user.id;
								msg.delivery = "sent";
								msg.from = ctx.req.body.from;
								resolve(msg);
								this.createLocalMessage(ctx, msg.from.id, msg)
									.then(lastmsg => {
										// resolve(lastmsg);
									})
									.catch(err => {
										// reject(err);
									});
							})
							.catch(err => {
								reject(err);
							});

						}).catch(err=>{
							reject(err);
						});
					} else {
						reject(ctx.t("app:NoContentToSend"));
					}
				});
				// 	let promises = [];
				// 	let responses = [];
				// 	let attachments = ctx.req.body.fbModel.files;
				// 	for(let i = 0; i<attachments.length; i++){
				// 		let attachment = attachments[i];
				// 		let buffstr = attachment.content.split(';base64,')[1];
				// 		let buffer = Buffer.from(buffstr, 'base64');
				// 		const client = MessengerClient.connect( ctx.app.locals.pageTokenCache.get(
				// 			ctx.user.socialLinks.facebook +
				// 			"_" +
				// 			ctx.params.pageId));
				// 		promises.push(client.sendImage(ctx.params.recipient, buffer, { filename: attachment.name }).then(data=>{
				// 			logger.debug(data);
				// 			responses.push({name: attachment.name, data: data});
				// 		}).catch(err=>{
				// 			logger.error(err);
				// 			responses.push({name: attachment.name, err: err});
				// 		}));
				// 	}
				// if (promises.length > 0) {
				// 	return Promise.all(promises).then(() => {
				// 		return responses;
				// 	});
				// }
			}
		},
		sendMediaComment: {
			cache: false,
			permission: C.PERM_LOGGEDIN,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					if (
						ctx.req.body &&
						ctx.req.body.fbModel &&
						ctx.req.body.pageId
					) {
						let fbModel = _.cloneDeep(ctx.req.body.fbModel);

						//let file  = 'C:/projects/mantis-cloud/data/khoa-hoc-nau-an-chay-04.jpg';
						let ptoken = ctx.app.locals.pageTokenCache.get(
						ctx.user.socialLinks.facebook + "_" + ctx.params.pageId
						);
						let fbUrl = "https://graph.facebook.com/v7.0" + fbModel[0] + "?access_token="+ ptoken;
						Attachment.findById({_id: ctx.params.attachment_id}).exec().then(docs=>{
							let file = docs.path;
							if (!fs.existsSync(file)){
								reject(ctx.t("app:NoFileToSend"));
							}
							this.sendImageComment(file, ctx.req.body.message, fbUrl).then(msgout=>{
								let msg =  _.cloneDeep(msgout);
								msg.fb_id = msgout.id;
								msg.author = ctx.user.id;
								msg.delivery = "sent";
								msg.from = ctx.req.body.from;
								resolve(msg);
								this.createLocalComment(ctx, msg.from.id, msg)
									.then(lastmsg => {
										// resolve(lastmsg);
									})
									.catch(err => {
										// reject(err);
									});
							})
							.catch(err => {
								reject(err);
							});

						}).catch(err=>{
							reject(err);
						});

					} else {
						reject(ctx.t("app:NoContentToSend"));
					}

			  });
			}
		},
		find: {
			cache: false,
			handler(ctx) {
				if(ctx.params.msg_type=="messages"){
					return this.findMessagesBy(ctx);
				}else if(ctx.params.msg_type=="comments"){
					let commentsService = this.context.services("comments");
					return commentsService.findCommentsBy(ctx);
				}
			}
		},

		// return a model by ID
		get: {
			cache: true, // if true, we don't increment the views!
			permission: C.PERM_PUBLIC,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:PostNotFound"));

				return this.collection
					.findByIdAndUpdate(ctx.modelID, {
						$inc: { views: 1 }
					})
					.exec()
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					});
			}
		},

		create: {
			handler(ctx) {
				this.validateParams(ctx, true);

				let message = new Message({
					title: ctx.params.title,
					content: ctx.params.content,
					author: ctx.user.id
				});

				return message
					.save()
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					})
					.then(json => {
						this.notifyModelChanges(ctx, "created", json);
						return json;
					});
			}
		},

		update: {
			permission: C.PERM_OWNER,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:MessageNotFound"));
				this.validateParams(ctx);

				return this.collection
					.findById(ctx.modelID)
					.exec()
					.then(doc => {
						if (ctx.params.title != null)
							doc.title = ctx.params.title;

						if (ctx.params.content != null)
							doc.content = ctx.params.content;

						doc.editedAt = Date.now();
						return doc.save();
					})
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					})
					.then(json => {
						this.notifyModelChanges(ctx, "updated", json);
						return json;
					});
			}
		},

		remove: {
			permission: C.PERM_OWNER,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:MessageNotFound"));

				return Message.remove({ _id: ctx.modelID })
					.then(() => {
						return ctx.model;
					})
					.then(json => {
						this.notifyModelChanges(ctx, "removed", json);
						return json;
					});
			}
		},

		vote(ctx) {
			ctx.assertModelIsExist(ctx.t("app:MessageNotFound"));

			return this.collection
				.findById(ctx.modelID)
				.exec()
				.then(doc => {
					// Check user is on voters
					if (doc.voters.indexOf(ctx.user.id) !== -1)
						throw ctx.errorBadRequest(
							C.ERR_ALREADY_VOTED,
							ctx.t("app:YouHaveAlreadyVotedThisMessage")
						);
					return doc;
				})
				.then(doc => {
					// Add user to voters
					return this.collection.findByIdAndUpdate(
						doc.id,
						{
							$addToSet: { voters: ctx.user.id },
							$inc: { votes: 1 }
						},
						{ new: true }
					);
				})
				.then(doc => {
					return this.toJSON(doc);
				})
				.then(json => {
					return this.populateModels(json);
				})
				.then(json => {
					this.notifyModelChanges(ctx, "voted", json);
					return json;
				});
		},

		unvote(ctx) {
			ctx.assertModelIsExist(ctx.t("app:MessageNotFound"));

			return this.collection
				.findById(ctx.modelID)
				.exec()
				.then(doc => {
					// Check user is on voters
					if (doc.voters.indexOf(ctx.user.id) == -1)
						throw ctx.errorBadRequest(
							C.ERR_NOT_VOTED_YET,
							ctx.t("app:YouHaveNotVotedThisMessageYet")
						);
					return doc;
				})
				.then(doc => {
					// Remove user from voters
					return this.collection.findByIdAndUpdate(
						doc.id,
						{ $pull: { voters: ctx.user.id }, $inc: { votes: -1 } },
						{ new: true }
					);
				})
				.then(doc => {
					return this.toJSON(doc);
				})
				.then(json => {
					return this.populateModels(json);
				})
				.then(json => {
					this.notifyModelChanges(ctx, "unvoted", json);
					return json;
				});
		}
	},

	methods: {
		findMessagesBy(ctx){
			return new Promise((resolve, reject)=>{
				let filter = ctx.params.filter;
				let mLimit = ctx.params.limit ? ctx.params.limit : 100;
				let offset = ctx.params.offset ? ctx.params.offset : 0;
				let order = ctx.params.order ? ctx.params.order : "-created_time";

				let options = {limit: mLimit, sort: order, skip: offset};

				if (ctx.params.filter == "my") filter.author = ctx.user.id;
				else if (ctx.params.author != null) {
					filter.author = this.personService.decodeID(
						ctx.params.author
					);
				}
				this.collection.find(filter, null, options).exec((err, docs) => {
					if(!err){
						resolve(docs);
					}else{
						reject(err);
					}
				});
			});
		},
		handleWebhook(messagingEvent, page_id, _ctx) {
			if (messagingEvent.message) {
				let fbService = this.context.services("notifications");
				let fbctx = _ctx.CreateToServiceInit(fbService);
				// let mids = _.map(messagingEvent.message.mid, id=>"m_"+id);
				this.updateBy(fbctx, {fb_id: {$in:[messagingEvent.message.mid]}}, {delivery: "delivered"}).then(mes=>{
					let bro = {method: "messageChanged", value: mes};
					fbctx.broadcast("notification", bro);
				});
			}
		},
		updateBy(ctx, searchObj, updateParams, options){

			return this.collection
				.update(
					searchObj,
					updateParams,
					options
				)
				.exec()
				.then(jsons => {
					return this.populateModels(jsons);
				})
				.then(jsons => {
					this.notifyModelChanges(ctx, "updated", jsons);
					return jsons;
				})
				.catch(err => {
					ctx.assertModelIsExist(
						ctx.t("app:ConversationUpdateErrorFound")
					);
					return err;
				});
		},
		sendMessage(ctx,fromId, pageId, fbModel, message) {

			return new Promise((resolve, reject) => {

				this.facebookService
							.fbgraphPagePreCall(ctx.app.locals, fromId, pageId, fbModel)
							.then(msgout => {
								let msg = {};
								msg.fb_id = msgout.id?msgout.id:msgout.message_id;
								//msg.author = ctx.user.id;
								msg.delivery = "sent";
								resolve(msg);
								// this.createLocalMessage(ctx, msg.from.id, msg)
								// 	.then(lastmsg => {
								// 	})
								// 	.catch(err => {
								// 		logger.error(err);
								// });
							})
							.catch(err => {
								reject(err);
							});

			});

		},
		sendImageMessage(recipientId, message, file_loc, endpoint){
			//let fs = require('fs');
			try {
				let recip = {};
				if(typeof recipientId == "string"){
					recip = {
						id : recipientId
					}
				} else {
					recip = recipientId
				}
				if(file_loc != undefined) {
					let readStream = fs.createReadStream(file_loc);
					let messageData = {
						recipient : recip,
						message : {
							// text: message,
							attachment : {
								type : "image",
								payload :{is_reusable: true	}
							}
						},
						filedata:readStream
					};
					return this.callSendAPIAttach(messageData, endpoint);
				} else {
					let messageData = {
						recipient : recip,
						message : {
							text: message,
						},
					};
					return this.callSendAPIAttach(messageData, endpoint);
				}
			} catch (error) {
				return Promise.reject(error);
			}

		},

		callSendAPIAttach(messageData, endpoint){
			return new Promise((resolve, reject)=>{
				//var endpoint = fbUrl
				let r = request.post(endpoint, function(err, httpResponse, body) {
					if (err) {reject(err);}
					try {
							body = JSON.parse(body)
							if(body.error) reject(body.error);
							console.log("attach to facebook successfull >> \n", body); //facebook always return 'ok' message, so you need to read error in 'body.error' if any
							// research back the content
							resolve(body);
					} catch (e) {
							reject(e)
					}
				});
				let form = r.form();
				if(messageData.recipient)
					form.append("recipient", JSON.stringify(messageData.recipient));
				if(messageData.message)
					form.append("message", JSON.stringify(messageData.message));
				if(messageData.filedata) {
					form.append("filedata", messageData.filedata); //no need to stringify!
				}
			});
		},

		sendImageComment(file_loc, message, endpoint){
			try {
				let fs = require("fs");
				let readStream = fs.createReadStream(file_loc);
				let messageData = {
					message : message,
					filedata:readStream
				};
				return this.callSendAPICommnet(messageData, endpoint);
			} catch (error) {
			   return Promise.reject(error);
			}

		},

		callSendAPICommnet(messageData, endpoint){
			return new Promise((resolve, reject)=>{
				let r = request.post(endpoint, function(err, httpResponse, body) {
					if (err) {reject(err);}
					// if(httpResponse) {
					// 	console.log("comment sent >> \n", httpResponse)
					// 	resolve(httpResponse)
					// }
					if(body) {
						try {
								body = JSON.parse(body)
								if(body.error) reject(body.error);
								console.log("comment file to facebook successfull >> \n", body); //facebook always return 'ok' message, so you need to read error in 'body.error' if any
								// research back the content
								resolve(body);
						} catch (e) {
								reject(e)
						}
					}
				});
				let form = r.form();
				//form.append('recipient', JSON.stringify(messageData.recipient));
				if(messageData.message)
					form.append("message", messageData.message);
				form.append("source", messageData.filedata); //no need to stringify!
			});


		},

		sendAttachmentToFB(recipientId, messagePayloads, ptoken) {

			return new Promise((resolve, reject) => {
					//let recipientId = ctx.req.body.to;
					//let messagePayloads = ctx.req.body.fbModel;
					// let ptoken = ctx.app.locals.pageTokenCache.get(
					// 	ctx.user.socialLinks.facebook + "_" + ctx.params.pageId
					// );
					// sendApi
					// 	.sendAttachment(recipientId, messagePayloads, ptoken)
					// 	.then(data => {
					// 		logger.log(data);
					// 		resolve(data);
					// 	})
					// 	.catch(err => {
					// 		logger.info(err);
					// 		reject(err);
					// 	});
			});
		},
		createLocalMessage(ctx, senderId, message) {
			if (ctx && ctx.user) {
				message.author = ctx.user.id;
			}
			let options = {
				upsert: true,
				new: true,
				setDefaultsOnInsert: true
			};
			let object = _.cloneDeep(message);
			if(object.id) {
				delete object.id;
				object.fb_id = message.id;
			}
			return this.collection
				.findOneAndUpdate(
					{ fb_id: message.fb_id },
					{ $set: object },
					options
				)
				.exec()
				.then(doc => {
					return this.toJSON(doc);
				})
				.then(json => {
					return this.populateModels(json);
				})

				.catch(err => {
					logger.error(err);
					return Promise.reject(err);
				});
		},
		saveLocalMessage(ctx, senderId, message) {
			if (ctx && ctx.user) {
				message.author = ctx.user.id;
			}
			return this.updateOrInsert(message.id, message);
		},
		createLocalComment(ctx, senderId, message) {
			let object = _.cloneDeep(message);
			if (ctx && ctx.user) {
				object.author = ctx.user.id;
			}
			return this.commentService.updateOrInsert(object.fb_id, object);
		},
		saveLocalComment(ctx, senderId, message) {
			let object = _.cloneDeep(message);
			if (ctx && ctx.user) {
				object.author = ctx.user.id;
			}
			return this.commentService.updateOrInsert(object.id, object);
		},
		updateOrInsert(id, model, cb) {
			return new Promise((resolve, reject) => {
				let options = {
					upsert: true,
					new: true,
					setDefaultsOnInsert: true
				};
				let object = _.cloneDeep(model);
			 	delete object.id;
				object.fb_id = id;
				this.collection
					.findOneAndUpdate({ fb_id: id }, { $set: object }, options)
					.exec()
					.then(result => {
						if (cb) cb(undefined, result);
						resolve(this.toJSON(result));
					})
					.catch(err => {
						console.error(
							"save mesasge error ===================",
							err
						);
						if (cb) cb(err, undefined);
						reject(err);
					});
			});
		},
		/**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 *
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		validateParams(ctx, strictMode) {
			if (strictMode || ctx.hasParam("title"))
				ctx.validateParam("title")
					.trim()
					.notEmpty(ctx.t("app:MessageTitleCannotBeEmpty"))
					.end();

			if (strictMode || ctx.hasParam("content"))
				ctx.validateParam("content")
					.trim()
					.notEmpty(ctx.t("app:MessageContentCannotBeEmpty"))
					.end();

			if (ctx.hasValidationErrors())
				throw ctx.errorBadRequest(
					C.ERR_VALIDATION_ERROR,
					ctx.validationErrors
				);
		},
		searchBy(condition, fields){
			return this.collection.find(condition, fields).exec();
		}
	},

	/**
	 * Check the owner of model
	 *
	 * @param {any} ctx	Context of request
	 * @returns	{Promise}
	 */
	ownerChecker(ctx) {
		return new Promise((resolve, reject) => {
			ctx.assertModelIsExist(ctx.t("app:MessageNotFound"));

			if (ctx.model.author.code == ctx.user.code || ctx.isAdmin())
				resolve();
			else reject();
		});
	},

	init(ctx) {
		// Fired when start the service
		this.facebookService = ctx.services("facebook");
		this.personService = ctx.services("people");
		this.contactService = ctx.services("contacts");
		this.commentService = ctx.services("comments");
		this.conversationService = ctx.services("conversations");
		this.folderService = ctx.services("folders");
		this.context = ctx;
		// Add custom error types
		C.append(["ALREADY_VOTED", "NOT_VOTED_YET"], "ERR");
	},

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		}
	},

	graphql: {
		query: `
		Messages(limit: Int, offset: Int, sort: String): [Message]
		Message(code: String): Message
		`,

		types: `
			type Message {
				code: String!
				title: String
				content: String
				author: Person!
				views: Int
				votes: Int,
				voters(limit: Int, offset: Int, sort: String): [Person]
				createdAt: Timestamp
				createdAt: Timestamp
			}
		`,

		mutation: `
		MessageCreate(title: String!, content: String!): Message
		MessageUpdate(code: String!, title: String, content: String): Message
		MessageRemove(code: String!): Message

		MessageVote(code: String!): Message
		MessageUnvote(code: String!): Message
		`,

		resolvers: {
			Query: {
				Messages: "find",
				Message: "get"
			},

			Mutation: {
				MessageCreate: "create",
				MessageUpdate: "update",
				MessageRemove: "remove",
				MessageVote: "vote",
				MessageUnvote: "unvote"
			}
		}
	}
};

/*
## GraphiQL test ##

# Find all posts
query getPosts {
  posts(sort: "-createdAt -votes", limit: 3) {
    ...postFields
  }
}

# Create a new post
mutation createPost {
  postCreate(title: "New post", content: "New post content") {
    ...postFields
  }
}

# Get a post
query getPost($code: String!) {
  post(code: $code) {
    ...postFields
  }
}

# Update an existing post
mutation updatePost($code: String!) {
  postUpdate(code: $code, content: "Modified post content") {
    ...postFields
  }
}

# vote the post
mutation votePost($code: String!) {
  postVote(code: $code) {
    ...postFields
  }
}

# unvote the post
mutation unVotePost($code: String!) {
  postUnvote(code: $code) {
    ...postFields
  }
}

# Remove a post
mutation removePost($code: String!) {
  postRemove(code: $code) {
    ...postFields
  }
}



fragment postFields on Post {
    code
    title
    content
    author {
      code
      fullName
      username
      avatar
    }
    views
    votes
  	voters {
  	  code
  	  fullName
  	  username
  	  avatar
  	}
}

*/
