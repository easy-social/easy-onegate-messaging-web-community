"use strict";

let logger = require("../../../core/logger");
let C = require("../../../core/constants");

let _ = require("lodash");
let {storage, config} = require("../../../core/firebaseService");
let mongoose = require('mongoose');

// let ContactService = require("../contacts/service");
// let ConversationService = require("../conversations/service");
// let CommentService = require("../comment/service");

module.exports = {
	settings: {
		name: "feeds",
		version: 1,
		namespace: "feeds",
		rest: true,
		ws: true,
		graphql: false,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: null,

		modelPropFilter: null,
			// "code type address name description status lastCommunication createdAt updatedAt"
		modelPopulates: {
			"fb_page": "page"
		}
	},

	actions: {
	},

	methods: {
		handleWebhook (change, page_id, _ctx) {

			switch (change.value.item){
			case "comment":
				if(change.value && change.value.from){
					let phoneObj = {
						phones: [],
						highlights: [],
					};
					if(change.value.message){
						phoneObj = this.getPhoneNumber(change.value.message);
					}
					let PostService = this.context.services("posts");
					let FacebookService = this.context.services("facebook");
					let PageService = this.context.services("pages");
					let NotificationService = this.context.services("notifications");
					let ContactService = this.context.services("contacts");
					let ConversationService = this.context.services("conversations");
					let CommentService = this.context.services("comments");
					let notictx = _ctx.CreateToServiceInit(NotificationService);

					PostService.findByOneId(change.value.post_id).then(res => {
						if(res.length == 0) {
							let model = [
								"/" + change.value.post_id,
								"GET",
								{
									fields: "message,full_picture,created_time,from,permalink_url,shares, reactions{type,id,link,name,pic,profile_type,username,picture}, attachments,reactions.type(NONE).limit(0).summary(total_count).as(reactions_NONE),reactions.type(LIKE).limit(0).summary(total_count).as(reactions_LIKE),reactions.type(LOVE).limit(0).summary(total_count).as(reactions_LOVE),reactions.type(WOW).limit(0).summary(total_count).as(reactions_WOW),reactions.type(HAHA).limit(0).summary(total_count).as(reactions_HAHA),reactions.type(SAD).limit(0).summary(total_count).as(reactions_SAD),reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_ANGRY),reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_THANKFUL)"
								}
							];
							PageService.getPageByFbId(page_id, "author").then(data => {
								if(data && data.length >= 1){
									FacebookService.fbgraphPagePreCall(this.context.app.locals, data[0].author.socialLinks.facebook, page_id, model).then(res => {
										PostService.updateOrInsert(change.value.post_id, res);
									})

								}
							})
						}
					}).catch(err=>{
						logger.error(err);
					});

					if(["hide", "remove", "edited", "unhide"].indexOf(change.value.verb) > -1){
						let comment = {
							fb_id: change.value.comment_id,
							verb: change.value.verb,
						};
						if(change.value.verb == "hide"){
							comment.is_hidden = true;
						} else if(change.value.verb == "unhide"){
							comment.is_hidden = false;
						}
						this.updateOrInsertConversationAndComment(undefined, comment, notictx, undefined, CommentService);
						return;
					} else {
						let settingService = this.context.services("settings");
						settingService.search({fb_id: page_id, type: "generalSetting", auto_hide_comment : true}).then(settings=>{
							if(settings && settings.length > 0 && change.value.from.id !=page_id){
								//hide comment
								FacebookService.hideOrUnhideComment(this.context.app.locals, page_id, change.value.comment_id, true);
							}
						})

						let conversationObj =   {
							updated_time: new Date(change.value.post.updated_time),
							snippet: change.value.message,
							last_interacted_by: change.value.from.id
						};

						let commentObj = {
							attachment:{media:{image:{src:change.value.photo}}, type: "photo"},
							verb: change.value.verb,
							message: change.value.message,
							fb_id: change.value.comment_id,
							phones: phoneObj.phones,
							highlights: phoneObj.highlights,
							from: change.value.from,
							updated_time: new Date(parseInt(change.value.created_time)*1000),
							is_hidden: change.value.is_hidden ? change.value.is_hidden : false,
							can_hide: change.value.can_hide,
							fb_page: page_id
						};
						if(commentObj.verb == "add"){
							commentObj.created_time = commentObj.updated_time;
							if(change.value.from.id !=page_id){
								conversationObj.extraUpdate = {$inc: {unread_count: 1}};
							}

						}
						if(change.value.parent_id == change.value.post.id){
							conversationObj.fb_fromId = change.value.from.id;
							conversationObj.from= change.value.from;
							conversationObj.fb_id= change.value.post.id + "_" +change.value.from.id;
							conversationObj.fb_page= page_id;
							conversationObj.hasPhone= phoneObj.highlights.length > 0;
							conversationObj.fb_type = "COMMENT";
							conversationObj.fb_postId= change.value.post.id;
							conversationObj.last_sent_by = change.value.from;
							if(conversationObj.extraUpdate){
								conversationObj.extraUpdate.$addToSet = {customerIds : change.value.from.id};
							}else{
								conversationObj.extraUpdate = {$addToSet : {customerIds : change.value.from.id}};
							}
							commentObj.permalink_url = change.value.post.permalink_url + (change.value.post.permalink_url.slice(-1) !== "/" ? "/" : "") + "?comment_id=" + change.value.comment_id;
							commentObj.conversation_id = change.value.post.id + "_" +change.value.from.id;
							this.updateOrInsertConversationAndComment(conversationObj, commentObj, notictx, ConversationService, CommentService).then(conData => {
								if(conData[0] && conData[0].customerIds) {
									conData[0].customerIds.forEach(cus => {
										if(cus != page_id) {
											let params = {
												sent_by_page: change.value.from.id == page_id,
												fb_contact_id: cus,
												fb_conversation_id: conversationObj.fb_id,
												type: "comment",
												fb_page_id: page_id,
												fb_id: commentObj.fb_id,
												user_id: conData[1] && conData[1].created_by && conData[1].created_by.uid ? mongoose.Types.ObjectId(conData[1].created_by.uid) : undefined,
												created_time: commentObj.created_time,
												phone_count: phoneObj && phoneObj.phones && phoneObj.phones.length > 0 ? phoneObj.phones.length : 0
											}
											let statistics = this.context.services("statistics");
											statistics.createCount(notictx, params)
										}
									})
								}
							});
						}else{
							commentObj.parent = {
								id: change.value.parent_id
							};
							commentObj.fb_page = page_id;
							commentObj.permalink_url = change.value.post.permalink_url + (change.value.post.permalink_url.slice(-1) !== "/" ? "/" : "") + "?comment_id=" + change.value.parent_id + "&reply_comment_id="+change.value.comment_id;
							CommentService.find(change.value.parent_id).then(data=>{
								if(data.length){
									commentObj.conversation_id = data[0].conversation_id;
									conversationObj.fb_id = data[0].conversation_id;
									conversationObj.last_sent_by = change.value.from;
									this.updateOrInsertConversationAndComment(conversationObj, commentObj, notictx, ConversationService, CommentService).then(conData => {
										if(conData[0] && conData[0].customerIds) {
											conData[0].customerIds.forEach(cus => {
												if(cus != page_id) {
													let params = {
														sent_by_page: change.value.from.id == page_id,
														fb_contact_id: cus,
														fb_conversation_id: conversationObj.fb_id,
														type: "comment",
														fb_page_id: page_id,
														fb_id: commentObj.fb_id,
														user_id: conData[1] && conData[1].created_by && conData[1].created_by.uid ? mongoose.Types.ObjectId(conData[1].created_by.uid) : undefined,
														created_time: commentObj.created_time,
														phone_count: phoneObj && phoneObj.phones && phoneObj.phones.length > 0 ? phoneObj.phones.length : 0
													}
													let statistics = this.context.services("statistics");
													statistics.createCount(notictx, params)
												}
											})
										}
									});
								}
							}).catch(err=>{
								logger.error(err);
							});
						}
						if(change.value.from.id != page_id){
							PageService.getPageByFbId(page_id, "author").then(data => {
								if(data && data.length >= 1){
									ContactService.checkAvatar(change.value.from, data[0]).then(cont => {
										if(!cont.pic_profile) {
											FacebookService.fbgraphPagePreCall(this.context.app.locals, data[0].author.socialLinks.facebook, page_id, ["/" + change.value.from.id , "GET",
											])
												.then(data3 => {
													if(data3.profile_pic){
														const fs = require('fs');
														const bucket = storage.bucket(config.storageBucket);
														var imageName = "pfa" + change.value.from.id;
														var http = require('https');
														var fileToDownload = data3.profile_pic;
														var remoteWriteStream = bucket.file(imageName).createWriteStream();
														var request = http.get(fileToDownload, (response) => {
															response.pipe(remoteWriteStream)
															.on('error', (err) => {})
															.on('finish', () => {
																const publicUrl = `https://storage.googleapis.com/${bucket.name}/${imageName}`;
																		 bucket.file(imageName).makePublic(()=>{
																			ContactService.updateOrInsert(change.value.from.id, {pic_profile: publicUrl}, page_id);
																			data3.profile_pic = publicUrl;
																			let bro = {method: "contactChanged", value: data3};
																			NotificationService.broadcast("notification", bro);
																		 });
																// The file upload is complete.
															});
														});
													}
												}).catch(err =>
													console.log(err)
												)
										}
									})
								}
							})
							ContactService.updateOrInsert(change.value.from.id, change.value.from, page_id);
							this.autoReplyMessage(_ctx, page_id, change.value);
						}
					}
				}
				break;
			case "reaction":
					// if(change.value && change.value.from){
                    //
					// 	let commentObj = {
					// 		verb: change.value.verb,
					// 		created_time: new Date(parseInt(change.value.created_time)),
					// 		message: change.value.message,
					// 	}
					// 	ContactService.updateOrInsert(change.value.from.id, change.value.from, page_id);
					// 	let options = {
					// 						$inc: {views: 1}, $addToSet: {recent_seen_userIds: ctx.user.id}
					// 					}
					// 	CommentService.update(change)
					// }
				break;
			case "like":
				break;
			case "status":
			case "photo":
			case "video":
			case "post":
				let PostService = this.context.services("posts");
				let NotificationService = this.context.services("notifications");
				let ConversationService = this.context.services("conversations");
				let CommentService = this.context.services("comments");
				let notictx = _ctx.CreateToServiceInit(NotificationService);
				let model = _.cloneDeep(change.value);
				delete model.post_id;
				PostService.updateOrInsert(change.value.post_id, model);
				if(change.value.from.id != page_id){
					let phoneObj = {
						phones: [],
						highlights: [],
					};
					if(change.value.message){
						phoneObj = this.getPhoneNumber(change.value.message);
					}
					let conversationObj = {
						customerIds: [change.value.from.id],
						fb_fromId: change.value.from.id,
						from: change.value.from,
						fb_postId: change.value.post_id,
						fb_id: change.value.post_id + "_" +change.value.from.id,
						updated_time: new Date(parseInt(change.value.created_time)*1000),
						snippet: change.value.message,
						last_interacted_by: change.value.from.id,
						fb_page: page_id,
						hasPhone: phoneObj.highlights.length > 0,
						fb_type : "COMMENT"
					};
					this.updateOrInsertConversationAndComment(conversationObj, undefined, notictx, ConversationService, CommentService);
					this.autoReplyMessage(_ctx, page_id, change.value);
				}
				break;
			default:
				break;
			}
		},
		autoReplyMessage(_ctx, page_id, message){
			let messageService = this.context.services("messages");
			let msctx = _ctx.CreateToServiceInit(messageService);
			let settingService = this.context.services("settings");
			settingService.search({fb_id: page_id, type: "replySetting", active_auto_reply : true}).then(settings=>{
				if(settings && settings.length > 0) {
					let setting = settings[0];
					let auto_reply_start_time_setting = new Date(setting.auto_reply_start_time);
					let auto_reply_end_time_setting = new Date(setting.auto_reply_end_time);
					if(auto_reply_start_time_setting > auto_reply_end_time_setting){
						return;
					}
					let now = new Date();
					let auto_reply_start_time = _.cloneDeep(now);
					auto_reply_start_time.setHours(auto_reply_start_time_setting.getHours());
					auto_reply_start_time.setMinutes(auto_reply_start_time_setting.getMinutes());
					auto_reply_start_time.setSeconds(auto_reply_start_time_setting.getSeconds());
					//
					let auto_reply_end_time = _.cloneDeep(now);
					auto_reply_end_time.setHours(auto_reply_end_time_setting.getHours());
					auto_reply_end_time.setMinutes(auto_reply_end_time_setting.getMinutes());
					auto_reply_end_time.setSeconds(auto_reply_end_time_setting.getSeconds());

					if( setting.auto_reply_templates
						&& setting.auto_reply_templates.length > 0
						&& auto_reply_start_time <= now
						&& auto_reply_end_time >= now
					) {

						let phoneObj = this.getPhoneNumber(message.message);
						let msg = undefined;
						if(phoneObj.highlights.length > 0){
							msg = _.find(setting.auto_reply_templates, {Condition: "hasPhone"}).Message;
						}else{
							msg = _.find(setting.auto_reply_templates, {Condition: "hasNoPhone"}).Message;
						}
						if(msg){
							let peopleService = this.context.services("people");
							peopleService.getPageEditor(page_id).then(user=>{
								let text =  msg;
								let to = message.from;
								if(text && to) {
									text = text.replace("#{FULL_NAME}", to.name).replace("#{LAST_NAME}", to.last_name ? to.last_name : to.name).replace("#{FIRST_NAME}",to.first_name ? to.first_name : to.name);
									let fbModel = [
										"/me/messages",
										"POST",
										{
											recipient: {
												id: message.from.id
											},
											message: {
												text: text,
											}
										}
									];
									messageService.sendMessage(msctx, user.socialLinks.facebook, page_id, fbModel).then(sent=>{
										console.log(sent);
									}).catch(err=>{
										console.log(err);
									});
								}
							}).catch(err=>{
								console.log(err);
							});
						}
					}
				}
			}).catch(err=>{
				console.log(err);
			});
		},
		getPhoneNumber(message) {
			//validate vietnamese phone
			let highlights = [];
			let result = {
				highlights: [],
				phones: []
			};
			let mes = message;
			//let phoneno = /([+84|0][1-9])+([0-9]{8})\b/g;
			let phoneno = /([+84|0][9|8|7|5|3]|01[2|6|8|9])+([0-9]{8})\b/g;
			let phones = mes.match(phoneno);
			if(phones !=null){
				for(let i = 0; i < phones.length ; i++){
					mes = mes.replaceAll (phones[i], "");
				}
				highlights = highlights.concat(phones);
			}

			mes = this.replaceLetters(mes);
			let phonesToCheck = mes.split(" ");

			for(let i = 0; i < phonesToCheck.length ; i++){
				let phone = `${phonesToCheck[i]}`.replaceAll (/\D/g, "");
				let matchedPhone = phone.match(phoneno);

				if(matchedPhone !=null && matchedPhone.length ==1 && matchedPhone[0].length == phone.length){
					if(phones!=null){
						phones = phones.concat(matchedPhone);
					}else{
						phones = matchedPhone;
					}
					highlights.push(phonesToCheck[i]);
				}

			}

			if(phones !=null)
				result.phones = phones;
			if(highlights !=null)
				result.highlights = highlights;

			return result;
		},
		replaceLetters(str) {
			str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "");
			str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "");
			str = str.replace(/ì|í|ị|ỉ|ĩ/g, "");
			str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "");
			str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "");
			str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "");
			str = str.replace(/đ/g, "");
			str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "");
			str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "");
			str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "");
			str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "");
			str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "");
			str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "");
			str = str.replace(/Đ/g, "");
			str = str.replace(/[a-zA-Z,]/g, "");
			str = str.trim();
			return str;
		},
		updateOrInsertConversationAndComment(conversationObj, commentObj, notictx, ConversationService, CommentService){
			return new Promise((resolve, reject)=>{
				let waits = [];
					if(conversationObj){
						waits.push(
						ConversationService.updateOrInsertByKeys(conversationObj.fb_id, conversationObj)
						.then(data=>{
							let bro = {method: "conversationChanged", value: data};
							notictx.broadcast("notification", bro);
							return data
						}).catch(err=>{
							logger.error(err);
							// reject();
						})
					);
					}
				// })

				if(commentObj){
					waits.push(
						CommentService.updateOrInsert(commentObj.fb_id, commentObj)
					.then(data=>{
						let bro = {method: "commentChanged", value: data};
						notictx.broadcast("notification", bro);
						return data
					}).catch(err=>{
						logger.error(err);
					})
				);
				}
					Promise.all(waits).then((dat)=>{
						resolve(dat)
					}).catch(err=>{
						reject();
					})
		})
	}
	},


	init(ctx) {
		this.context = ctx;
	},

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		}
	},

};
