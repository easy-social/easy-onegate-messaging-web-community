const _ = require("lodash");
const agenda = require("../../../core/agenda");
const Services = require("../../../core/services");
const { fbGraphPageCallAsync } = require("./util");

agenda.define("facebook.syncComment", async (job) => {
	const { pageId, postId, ptoken } = job.attrs.data;
	const contactService = Services.get("contacts");
	const conversationService = Services.get("conversations");
	const postService = Services.get("posts");
	const feedService = Services.get("feeds");
	const messageService = Services.get("messages");

	// let profileId = job.attrs.data.profileId;
	// const result = {
	// 	type: "COMMENT",
	// 	num_success: 0,
	// 	num_error: 0,
	// 	error_logs: [],
	// 	last: {},
	// 	date_start: new Date(),
	// 	date_stop: new Date(),
	// 	date_end: new Date(),
	// };
	// let limit = limit ? limit : 1;
	//let model = job.attrs.data.conversationModel;
	const queryFields = {
		fields:
			"message_tags,attachment,parent,message,comment_count,from,created_time,permalink_url,is_hidden,can_hide,comments.limit(1){from,can_hide}",
		limit: 100,
	};

	const model = ["/" + postId + "/comments", "GET"];

	const toCall = async (ptoken, model, queryFields) => {
		const conversations = await fbGraphPageCallAsync(ptoken, [
			...model,
			queryFields,
		]);

		if (conversations && conversations.data.length > 0) {
			await Promise.all(
				conversations.data.map(async (c) => {
					c.postId = postId;
					c.fb_page = pageId;

					if (c.from) {
						c.fb_fromId = c.from.id;

						const s = _.cloneDeep(c.from);
						if (s.id != pageId) {
							const sid = s.id;
							contactService.updateOrInsert(sid, s, pageId);
						}

						c.customerIds = [s.id];
						let hasChild = false;
						if (c.comments && c.comments.data.length > 0) {
							c.last_sent_by = c.comments.data[0].from;
							hasChild = true;
							delete c.comments;
						} else {
							c.last_sent_by = c.from;
						}

						c.updated_time = c.created_time;
						c.snippet = c.message;

						const saved = await conversationService.saveOrInsertFromComment(
							c.id,
							c
						);

						let conversation = {
							snippet: c.message,
							updated_time: c.created_time,
						};

						postService.updateOrInsertCount(
							{
								fb_id: postId,
							},
							{
								$inc: {
									count_comment: 1,
								},
							}
						);

						if (saved.from.id != pageId) {
							const phoneObj = feedService.getPhoneNumber(
								c.message
							);

							if (phoneObj.highlights.length > 0) {
								postService.updateOrInsertCount(
									{
										fb_id: postId,
									},
									{
										$inc: {
											count_phone:
												phoneObj.highlights.length,
										},
									}
								);

								c.phones = phoneObj.phones;
								c.highlights = phoneObj.highlights;

								if (phoneObj.highlights.length) {
									conversation.hasPhone = true;
								}
							}

							conversation.$addToSet = {
								customerIds: s.id,
							};
						}

						conversationService.updateFieldByConditions(
							null,
							{
								fb_id: saved.fb_id,
							},
							conversation
						);
						c.conversation_id = saved.fb_id;
						c.fb_page = pageId;
						c.created_time = new Date(c.created_time);

						try {
							const csaved = await messageService.saveLocalComment(
								c.id,
								c.from.id,
								c
							);
							if (hasChild) {
								await agenda
									.create("facebook.syncReply", {
										pageId,
										postId,
										parentId: postId,
										conversation: saved,
										ptoken,
										csaved,
									})
									.save();
							}
						} catch (e) {
							//
							console.warn("error on sync", e);
						}
						///
					}
				})
			);

			if (conversations.paging && conversations.paging.next) {
				toCall(token, model, {
					...queryFields,
					after: conversations.data.cursors.after,
				});
			}
		}

		// console.log(err);
		// 		result.num_error++;
		// 		result.error_logs.push(err);
		// 		result.date_end = new Date();
		// 		result.last = model;
		// 		done(result);
		// 		resolve(cons);
	};

	await toCall(ptoken, model, queryFields);
});

// synchonizeComment(ctx, con, job, done, next, limit) {
// 	let cons = [];
// 	return new Promise((resolve, reject) => {
// 		let pageId = job.attrs.data.pageId;
// 		// let profileId = job.attrs.data.profileId;
// 		let result = {
// 			type: "COMMENT",
// 			num_success: 0,
// 			num_error: 0,
// 			error_logs: [],
// 			last: {},
// 			date_start: new Date(),
// 			date_stop: new Date(),
// 			date_end: new Date(),
// 		};
// 		// let limit = limit ? limit : 1;
// 		//let model = job.attrs.data.conversationModel;
// 		let queryFields = {
// 			fields:
// 				"message_tags,attachment,parent,message,comment_count,from,created_time,permalink_url,is_hidden,can_hide,comments.limit(1){from,can_hide}",
// 			limit: limit,
// 		};
// 		let token = job.attrs.data.ptoken;
// 		let self = this;
// 		let model = ["/" + con.fb_id + "/comments", "GET", queryFields];
// 		toCall(token, model, self);
// 		function toCall(token, model, self) {
// 			self.fbgraphPageCall(token, model)
// 				.then((conversations) => {
// 					if (
// 						conversations &&
// 						conversations.data.length > 0
// 					) {
// 						let waits = []; // wait for saved
// 						_.forIn(conversations.data, (c) => {
// 							c.fb_page = pageId;
// 							c.postId = con.fb_id;
// 							if (c.from) {
// 								c.fb_fromId = c.from.id;
// 								let s = _.cloneDeep(c.from);
// 								if (s.id != pageId) {
// 									let sid = s.id;
// 									// s.fb_pages = [pageId];
// 									self.contactService.updateOrInsert(
// 										sid,
// 										s,
// 										pageId
// 									);
// 								}
// 								c.customerIds = [s.id];
// 								let hasChild = false;
// 								if (
// 									c.comments &&
// 									c.comments.data.length > 0
// 								) {
// 									c.last_sent_by =
// 										c.comments.data[0].from;
// 									hasChild = true;
// 									delete c.comments;
// 								} else {
// 									c.last_sent_by = c.from;
// 								}
// 								c.updated_time = c.created_time;
// 								c.snippet = c.message;
// 								waits.push(
// 									self.conversationService
// 										.saveOrInsertFromComment(
// 											c.id,
// 											c
// 										)
// 										.then((saved) => {
// 											// if(cons.length < limit){
// 											cons.push(saved);
// 											// }
// 											let conversation = {
// 												snippet: c.message,
// 												updated_time:
// 													c.created_time,
// 											};
// 											result.num_success++;
// 											self.postService.updateOrInsertCount(
// 												{ fb_id: con.fb_id },
// 												{
// 													$inc: {
// 														count_comment: 1,
// 													},
// 												}
// 											);
// 											if (
// 												saved.from.id != pageId
// 											) {
// 												let phoneObj = self.feedService.getPhoneNumber(
// 													c.message
// 												);
// 												if (
// 													phoneObj.highlights
// 														.length > 0
// 												) {
// 													self.postService.updateOrInsertCount(
// 														{
// 															fb_id:
// 																con.fb_id,
// 														},
// 														{
// 															$inc: {
// 																count_phone:
// 																	phoneObj
// 																		.highlights
// 																		.length,
// 															},
// 														}
// 													);
// 													c.phones =
// 														phoneObj.phones;
// 													c.highlights =
// 														phoneObj.highlights;
// 													if (!con.hasPhone) {
// 														conversation.hasPhone = true;
// 													}
// 												}
// 												conversation.$addToSet = {
// 													customerIds: s.id,
// 												};
// 											}
// 											self.conversationService.updateFieldByConditions(
// 												ctx,
// 												{ fb_id: saved.fb_id },
// 												conversation
// 											);
// 											c.conversation_id =
// 												saved.fb_id;
// 											c.fb_page = con.fb_page;
// 											c.created_time = new Date(
// 												c.created_time
// 											);
// 											self.messageService
// 												.saveLocalComment(
// 													c.id,
// 													c.from.id,
// 													c
// 												)
// 												.then((csaved) => {
// 													if (hasChild) {
// 														self.synchonizeReply(
// 															ctx,
// 															saved,
// 															job,
// 															done,
// 															csaved,
// 															next,
// 															100
// 														);
// 													}
// 												});
// 										})
// 								);
// 							}
// 						});
// 						Promise.all(waits).then(() => {
// 							if (
// 								conversations.paging &&
// 								conversations.paging.next
// 							) {
// 								model[2]["after"] =
// 									conversations.paging.cursors.after;

// 								toCall(token, model, self);
// 							} else {
// 								result.date_end = new Date();
// 								result.last = _.last(
// 									conversations.data
// 								);
// 								done(result);
// 								resolve(cons);
// 							}
// 						});
// 					} else {
// 						result.date_end = new Date();
// 						result.error_logs.push("no-data");
// 						done(result);
// 						resolve(cons);
// 					}
// 				})
// 				.catch((err) => {
// 					console.log(err);
// 					result.num_error++;
// 					result.error_logs.push(err);
// 					result.date_end = new Date();
// 					result.last = model;
// 					done(result);
// 					resolve(cons);
// 				});
// 		}
// 	});
// },
