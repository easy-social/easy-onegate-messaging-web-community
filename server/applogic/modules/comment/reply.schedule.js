const _ = require("lodash");
const agenda = require("../../../core/agenda");
const Services = require("../../../core/services");
const { fbGraphPageCallAsync } = require("./util");

agenda.define("facebook.syncReply", async (job) => {
	const feedService = Services.get("feeds");
	const postService = Services.get("posts");
	const messageService = Services.get("messages");
	const conversationService = Services.get("conversations");
	const contactService = Services.get("contacts");

	const { pageId, ptoken, parentId, conversation } = job.attrs.data;

	const queryFields = {
		fields:
			"message_tags,attachment,parent,message,comment_count,from,created_time,permalink_url,is_hidden,can_hide",
		limit: 100,
	};

	const model = [`/${parentId}/comments`, "GET"];

	const toCall = async (ptoken, model, queryFields) => {
		try {
			const messages = await fbGraphPageCallAsync(ptoken, [
				...model,
				queryFields,
			]);

			if (messages && messages.data.length > 0) {
				await Promise.all(
					messages.data.map(async (c) => {
						c.conversation_id = conversation.fb_id;
						c.fb_page = conversation.fb_page;
						c.created_time = new Date(c.created_time);
						let s = _.cloneDeep(c.from);
						if (s && s.id != conversation.fb_page) {
							let sid = s.id;
							// s.fb_pages = [pageId];
							contactService.updateOrInsert(sid, s, pageId);
						}

						const conv = {
							...conversation,
							snippet: c.message,
							updated_time: c.created_time,
						};
						postService.updateOrInsertCount(
							{ fb_id: conv.fb_postId },
							{ $inc: { count_comment: 1 } }
						);

						if (c.from.id != pageId) {
							let phoneObj = feedService.getPhoneNumber(
								c.message
							);
							if (phoneObj.highlights.length > 0) {
								postService.updateOrInsertCount(
									{ fb_id: conv.fb_postId },
									{
										$inc: {
											count_phone:
												phoneObj.highlights.length,
										},
									}
								);
								c.phones = phoneObj.phones;
								c.highlights = phoneObj.highlights;
								if (!conv.hasPhone) {
									conv.hasPhone = true;
								}
							}
							conv.$addToSet = {
								customerIds: s.id,
							};
						}

						conversationService.updateFieldByConditions(
							null,
							{ fb_id: conversation.fb_id },
							conversation
						);

						messageService.saveLocalComment(c.id, c.from.id, c);
						// .then((saved) => {
						// 	// if (cons.length <= limit) {
						// 	// 	cons.push(saved);
						// 	// }
						// 	// result.num_success++;
						// })
					})
				);

				if (messages.paging && messages.paging.next) {
					await toCall(ptoken, [
						...model,
						{
							...queryFields,
							after: messages.paging.cursors.after,
						},
					]);
				}
				// else {
				// 	result.date_end = new Date();
				// 	result.last = _.last(messages.data);
				// 	done(result);
				// 	resolve(cons);
				// }
			}
			//  else {
			// 	result.error_logs.push("no-data");
			// 	result.date_end = new Date();
			// 	done(result);
			// 	resolve(cons);
			// }
		} catch (err) {
			console.log(err);
			// result.num_error++;
			// result.error_logs.push(err);
			// result.date_end = new Date();
			// result.last = model;
			// done(result);
			// resolve(cons);
		}
	};

	await toCall(ptoken, model, queryFields);
});

// synchonizeReply(ctx, con, job, done, parent, next, limit) {
// 	let cons = [];
// 	return new Promise((resolve, reject) => {
// 		let pageId = job.attrs.data.pageId;
// 		// let profileId = job.attrs.data.profileId;
// 		let result = {
// 			type: "REPLY",
// 			num_success: 0,
// 			num_error: 0,
// 			error_logs: [],
// 			last: {},
// 			date_start: new Date(),
// 			date_stop: new Date(),
// 			date_end: new Date(),
// 		};
// 		// let limit = limit ? limit : 100;
// 		//let model = job.attrs.data.conversationModel;
// 		let queryFields = {
// 			fields:
// 				"message_tags,attachment,parent,message,comment_count,from,created_time,permalink_url,is_hidden,can_hide",
// 			limit: limit,
// 		};

// 		let token = job.attrs.data.ptoken;
// 		let self = this;

// 		let model = [
// 			"/" + parent.fb_id + "/comments",
// 			"GET",
// 			queryFields,
// 		];
// 		toCall(token, model, self);

// 		function toCall(token, model, self) {
// 			self.fbgraphPageCall(token, model)
// 				.then((messages) => {
// 					if (messages && messages.data.length > 0) {
// 						let waits = []; // wait for saved
// 						_.forIn(messages.data, (c) => {
// 							c.conversation_id = con.fb_id;
// 							c.fb_page = con.fb_page;
// 							c.created_time = new Date(c.created_time);
// 							let s = _.cloneDeep(c.from);
// 							if (s.id != con.fb_page) {
// 								let sid = s.id;
// 								// s.fb_pages = [pageId];
// 								self.contactService.updateOrInsert(
// 									sid,
// 									s,
// 									pageId
// 								);
// 							}
// 							let conversation = {
// 								snippet: c.message,
// 								updated_time: c.created_time,
// 							};
// 							self.postService.updateOrInsertCount(
// 								{ fb_id: con.fb_postId },
// 								{ $inc: { count_comment: 1 } }
// 							);
// 							if (c.from.id != pageId) {
// 								let phoneObj = self.feedService.getPhoneNumber(
// 									c.message
// 								);
// 								if (phoneObj.highlights.length > 0) {
// 									self.postService.updateOrInsertCount(
// 										{ fb_id: con.fb_postId },
// 										{
// 											$inc: {
// 												count_phone:
// 													phoneObj.highlights
// 														.length,
// 											},
// 										}
// 									);
// 									c.phones = phoneObj.phones;
// 									c.highlights = phoneObj.highlights;
// 									if (!con.hasPhone) {
// 										conversation.hasPhone = true;
// 									}
// 								}
// 								conversation.$addToSet = {
// 									customerIds: s.id,
// 								};
// 							}

// 							self.conversationService.updateFieldByConditions(
// 								ctx,
// 								{ fb_id: con.fb_id },
// 								conversation
// 							);
// 							waits.push(
// 								self.messageService
// 									.saveLocalComment(
// 										c.id,
// 										c.from.id,
// 										c
// 									)
// 									.then((saved) => {
// 										if (cons.length <= limit) {
// 											cons.push(saved);
// 										}
// 										result.num_success++;
// 									})
// 							);
// 						});
// 						Promise.all(waits).then(() => {
// 							if (
// 								messages.paging &&
// 								messages.paging.next
// 							) {
// 								model[2]["after"] =
// 									messages.paging.cursors.after;
// 								toCall(token, model, self);
// 							} else {
// 								result.date_end = new Date();
// 								result.last = _.last(messages.data);
// 								done(result);
// 								resolve(cons);
// 							}
// 						});
// 					} else {
// 						result.error_logs.push("no-data");
// 						result.date_end = new Date();
// 						done(result);
// 						resolve(cons);
// 					}
// 				})
// 				.catch((err) => {
// 					console.log(err);
// 					result.num_error++;
// 					result.error_logs.push(err);
// 					result.date_end = new Date();
// 					result.last = model;
// 					done(result);
// 					resolve(cons);
// 				});
// 		}
// 	});
// },
