"use strict";

let logger = require("../../../core/logger");
let config = require("../../../config");
let C = require("../../../core/constants");

let _ = require("lodash");

let Setting = require("./models/setting");
let CONTEXT = require("../../../core/context");

module.exports = {
	settings: {
		name: "settings",
		version: 1,
		namespace: "settings",
		rest: true,
		ws: true,
		graphql: true,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: Setting,

		modelPropFilter: null,
		modelPopulates: {
			author: "people"
		}

	},

	actions: {
		find: {
			cache: false,
			handler(ctx) {
				let filter = ctx.params.condition;

				if (ctx.params.filter == "my") filter.author = ctx.user.id;
				else if (ctx.params.author != null) {
					filter.author = this.personService.decodeID(
						ctx.params.author
					);
				}

				let query = this.collection.find(filter);

				return ctx
					.queryPageSort(query)
					.exec()
					.then(docs => {
						//console.log(docs)
						return this.toJSON(docs);
					})
					.then(json => {
						console.log(json);
						return this.populateModels(json);
					});
			}
		},

		// return a model by ID
		get: {
			cache: true,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:SettingNotFound"));
				return Promise.resolve(ctx.model);
			}
		},
		updateOrInsert: {
			handler(ctx) {
				let fb_id = ctx.params.fb_id;
				let type = ctx.params.type;
				let object = _.cloneDeep(ctx.params);
				delete object.id;
				delete object._id;
				delete object.__v;
				if(fb_id==undefined || type == undefined){
					return {err:true, msg: "pageId or setting type is invalid"};
				}
				return this.updateOrInsertBy({fb_id, type}, object);
			}
		},
		create: {
			cache: false,
			handler(ctx) {
				this.validateParams(ctx, true);
				console.log(ctx.params);
				let setting = new Setting(ctx.params);
				setting.author = ctx.user.id;
				return setting
					.save()
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					})
					.then(json => {
						this.notifyModelChanges(ctx, "created", json);
						return json;
					});
			}
		},

		update: {
			cache: true,
			handler(ctx, id) {
				ctx.assertModelIsExist(ctx.t("app:SettingNotFound"));
				// this.validateParams(ctx);
				let modelID = id ? id : ctx.modelID;
				let params = ctx.params;
				return this.collection
					.findById(modelID)
					.exec()
					.then(doc => {
						delete params.modelID;
						_.forOwn(params, function(value, key) {
							if (value != null) {
								doc[key] = value;
							}
						});

						return doc.save();
					})
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					})
					.then(json => {
						this.notifyModelChanges(ctx, "updated", json);
						return json;
					});
			}
		},

		updateBy: {
			cache: true,
			handler(ctx, keys, isAnd) {
				// ctx.assertModelIsExist(ctx.t("app:SettingNotFound"));
				this.validateParams(ctx);
				let searchObj = {};
				if (isAnd) {
					_.forEach(keys, function(key) {
						searchObj[key] = ctx.params[key];
					});
				} else {
					let conditions = { $or: [] };
					_.forEach(keys, function(key) {
						let obj = [];
						obj[key] = ctx.params[key];
						conditions.$or.push(obj);
					});
				}
				return this.collection.find(searchObj, function(
					err,
					existingUsers
				) {
					if (existingUsers.length == 0) {
						let data = { success: false, msg: "Setting not found" };
						return this.notifyModelChanges(ctx, "Error", data);
					} else if (existingUsers.length == 1) {
						return this.update(ctx);
					} else {
						let data = {
							success: false,
							msg: "More than 1 to update",
							data: existingUsers
						};
						return this.notifyModelChanges(ctx, "Error", data);
					}
				});
			}
		},
		remove: {
			cache: true,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:SettingNotFound"));

				return Setting.remove({ _id: ctx.modelID })
					.then(() => {
						return ctx.model;
					})
					.then(json => {
						this.notifyModelChanges(ctx, "removed", json);
						return json;
					});
			}
		}
	},

	methods: {
		// sendNotificationToUser(ev, val){
		// 	var ctx = Context.CreateToServiceInit(this.notificationService);
  //           this.notificationService.broadcastBackToSender(ctx, ev, val);
		// },
		search(query, fields) {
			return new Promise((resolve, reject) => {
				if (!query) reject("Missing query");
				this.collection
					.find(query)
					.exec()
					.then(result => {
						resolve(this.toJSON(result));
						//resolve(result);
					})
					.catch(err => {
						reject(err);
					});
			});
		},
		updateOrInsertByKeys(key, object, ctx) {
			return new Promise((resolve, reject) => {
				let options = {
					upsert: true,
					new: true,
					setDefaultsOnInsert: true,
					overwrite: false
				};

				this.collection
					.findOneAndUpdate({ fb_id: key }, { $set: object }, options)
					.exec()
					.then(docs => {
						if (ctx) {
							this.facebookService.sendNotificationToUser(ctx, "settingsChanged", docs);
						}
						resolve(this.toJSON(docs));
					})
					.catch(err => {
						reject(err);
					});
			});
		},
		updateOrInsertBy(objectSearch, object, ctx) {
			return new Promise((resolve, reject) => {
				let options = {
					upsert: true,
					new: true,
					setDefaultsOnInsert: true,
					overwrite: false
				};

				this.collection
					.findOneAndUpdate(objectSearch, { $set: object }, options)
					.exec()
					.then(docs => {
						// if (ctx) {
						// 	this.facebookService.sendNotificationToUser("settingsChanged", docs);
						// }
						let fbctx = CONTEXT.CreateToServiceInit(this.notificationService);
						this.notificationService.broadcast(fbctx, "settingsChanged", docs);
						// var bro = {method: "settingsChanged", value: docs};
						// fbctx.broadcast("notification", bro);
						resolve(this.toJSON(docs));
					})
					.catch(err => {
						reject(err);
					});
			});
		},
		updateOrInsert(id, model, cb) {
			let options = {
				upsert: true,
				new: true,
				setDefaultsOnInsert: true
			};
			return this.collection
				.findOneAndUpdate({ _id: id }, { $set: model }, options)
				.exec()
				.then(result => {
					cb(undefined, result);
				})
				.catch(err => {
					cb(err, undefined);
				});
		},
		/**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 *
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		validateParams(ctx, strictMode) {
			if (strictMode || ctx.hasParam("name"))
				ctx.validateParam("name")
					.trim()
					.notEmpty(ctx.t("app:SettingNameCannotBeBlank"))
					.end();

			if (strictMode || ctx.hasParam("status"))
				ctx.validateParam("status").isNumber();

			ctx.validateParam("description")
				.trim()
				.end();
			ctx.validateParam("address")
				.trim()
				.end();
			ctx.validateParam("type")
				.trim()
				.end();

			if (ctx.hasValidationErrors())
				throw ctx.errorBadRequest(
					C.ERR_VALIDATION_ERROR,
					ctx.validationErrors
				);
		}
	},

	init(ctx) {
		// Fired when start the service
		this.context = ctx;
		this.notificationService = ctx.services("notifications");
		this.facebookService = ctx.services("facebook");
	},

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		}
	},

	graphql: {
		query: `
			Settings(limit: Int, offset: Int, sort: String): [Setting]
			Setting(code: String): Setting
		`,

		types: `
			type Setting {
				code: String!
				address: String
				type: String
				name: String
				description: String
				status: Int
				lastCommunication: Timestamp
			}
		`,

		mutation: `
		SettingCreate(name: String!, address: String, type: String, description: String, status: Int): Setting
		SettingUpdate(code: String!, name: String, address: String, type: String, description: String, status: Int): Setting
		SettingRemove(code: String!): Setting
		`,

		resolvers: {
			Query: {
				Settings: "find",
				Setting: "get"
			},

			Mutation: {
				SettingCreate: "create",
				SettingUpdate: "update",
				SettingRemove: "remove"
			}
		}
	}
};

/*
## GraphiQL test ##

# Find all devices
query getDevices {
  devices(sort: "lastCommunication", limit: 5) {
    ...deviceFields
  }
}

# Create a new device
mutation createDevice {
  deviceCreate(name: "New device", address: "192.168.0.1", type: "raspberry", description: "My device", status: 1) {
    ...deviceFields
  }
}

# Get a device
query getDevice($code: String!) {
  device(code: $code) {
    ...deviceFields
  }
}

# Update an existing device
mutation updateDevice($code: String!) {
  deviceUpdate(code: $code, address: "127.0.0.1") {
    ...deviceFields
  }
}

# Remove a device
mutation removeDevice($code: String!) {
  deviceRemove(code: $code) {
    ...deviceFields
  }
}

fragment deviceFields on Device {
    code
    address
    type
    name
    description
    status
    lastCommunication
}

*/
