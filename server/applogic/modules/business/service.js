"use strict";

let logger = require("../../../core/logger");
let config = require("../../../config");
let C = require("../../../core/constants");

let _ = require("lodash");

let Contact = require("../contacts/models/contact");

module.exports = {
	settings: {
		name: "business",
		version: 1,
		namespace: "business",
		rest: true,
		ws: true,
		graphql: false,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: Contact,

		modelPropFilter: null,
			// "code type address name description status lastCommunication createdAt updatedAt"
		modelPopulates: {			 
			"fb_page": "page"
		}
	},

	actions: {
		search: {
			cache: false,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					let filter = {};
				//console.log(ctx.sort);
					if(ctx.params && ctx.params.filter){
						if(ctx.params.filter === "all")
							filter["fb_pages"] = {$in:ctx.user.profile.accessPages};
						else if(ctx.params.filter === "my")
							filter["assistants"] = {$in: [ctx.user.id]};
						else if(ctx.params.filter.indexOf("=") > -1){
							let fs = ctx.params.filter.split("=");
							filter[fs[0]] = fs[1];
						} else if(ctx.params.filter.indexOf(":") > -1) {
							let fs = ctx.params.filter.split(":");
							if(fs[1] == "true")
								filter[fs[0]] =  { $gt: -Infinity };
						} 
					} 
				//console.log(filter)
					let query = this.collection.find(filter);
					ctx.queryPageSort(query).exec().then( (docs) => {
					//console.log(filter, docs.length)
						resolve(this.toJSON(docs));
					}).catch(err=>{
						logger.error(err);
						reject(err);
					});
				});
			}
		},
		find: {
			cache: false,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					let filter = ctx.params ? ctx.params : {};
					let currentUser = ctx.user;
					if (
					currentUser &&
					currentUser.provider === "facebook" &&
					currentUser.passwordLess
				) {
						let cModel = [
							"/me",
							"GET",
						{ fields: "id,accounts{id,access_token}" }
						];
						this.pageService
						.callToFb(cModel, currentUser.profile.access_token)
						.then(pages => {
							logger.info("contact pages loaded", pages);
							if (pages.accounts && pages.accounts.data) {
								logger.info(filter);
								let pageIds = _.map(
									pages.accounts.data,
									"id"
								);
								filter = _.assign(filter, {fb_pages: {$in : pageIds}});
								//filter.fb_pages = ;
								logger.info(filter);
								let query = Contact.find(filter);
								ctx
									.queryPageSort(query)
									.exec()
									.then(docs => {
										console.log(
											"filter doc by pages ",
											docs
										);
										resolve(this.toJSON(docs));
									}).catch(err => {
										ctx.assertModelIsExist(
											ctx.t("app:ContactNotFound")
										);
										resolve(ctx.model);
									});
								//filter = _.assign(filter, {fb_pages: {$in : []}})
							}
						})
						.catch(err => {
							console.log(err);
							ctx.assertModelIsExist(
								ctx.t("app:ContactNotFound")
							);
							resolve(ctx.model);
						});

					//filter = _.assign(filter, {})
					}else {
						ctx.assertModelIsExist(ctx.t("app:ContactNotFound"));
						resolve(ctx.model);
					}
				// let query = Contact.find(filter);
				// return ctx.queryPageSort(query).exec().then( (docs) => {
				// 	console.log('docs ',docs)
				// 	return this.toJSON(docs);
				// });

				});
			}
		},

		// return a model by ID
		get: {
			cache: true,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:ContactNotFound"));
				return Promise.resolve(ctx.model);
			}
		},

		create: {
			cache: true,
			handler(ctx) {
				this.validateParams(ctx, true);
				let contact = new Contact(ctx.params);
				return contact
					.save()
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					})
					.then(json => {
						this.notifyModelChanges(ctx, "created", json);
						return json;
					});
			}
		},

		update: {
			cache: true,
			handler(ctx, id) {
				ctx.assertModelIsExist(ctx.t("app:ContactNotFound"));
				// this.validateParams(ctx);
				let modelID = id ? id : ctx.modelID;
				let params = ctx.params;
				let docs = {};
				_.forOwn
						(params, function(value, key) {
							if (value != null && key != "modelID" && key.indexOf("_") == -1) {
								//console.log(key, ": " , value)
								docs[key] = value;
							}
						});
				let options = {
					upsert: true,
					new: true,
					setDefaultsOnInsert: true
				};
				//console.log('prepare to save================', modelID, _.cloneDeep(docs))
				return this.collection
					.findOneAndUpdate({_id: modelID},{ $set: docs }, options)
					.exec()
					// .then(doc => {
					// 	console.log('search result================', doc)
					// 	delete params.modelID;
					// 	let docs = this.toJSON(doc)
					// 	_.forOwn
					// 	(params, function(value, key) {
					// 		if (value != null) {
					// 			console.log(key, ": " , value)
					// 			docs[key] = value;
					// 		}
					// 	});
					// 	console.log('prepare to save================', _.cloneDeep(docs))
					// 	return docs.save();
					// })
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					})
					.then(json => {
						this.notifyModelChanges(ctx, "updated", json);
						return json;
					}).catch(err=>{
						console.log(err);
						ctx.assertModelIsExist(ctx.t("app:ContactUpdateErrorFound"));
					});
			}
		},

		updateBy: {
			cache: true,
			handler(ctx, keys, isAnd) {
				// ctx.assertModelIsExist(ctx.t("app:ContactNotFound"));
				this.validateParams(ctx);
				let searchObj = {};
				if (isAnd) {
					_.forEach(keys, function(key) {
						searchObj[key] = ctx.params[key];
					});
				} else {
					let conditions = { $or: [] };
					_.forEach(keys, function(key) {
						let obj = [];
						obj[key] = ctx.params[key];
						conditions.$or.push(obj);
					});
				}
				return this.collection.find(searchObj, function(
					err,
					existingUsers
				) {
					if (existingUsers.length == 0) {
						let data = { success: false, msg: "Contact not found" };
						return this.notifyModelChanges(ctx, "Error", data);
					} else if (existingUsers.length == 1) {
						return this.update(ctx);
					} else {
						let data = {
							success: false,
							msg: "More than 1 to update",
							data: existingUsers
						};
						return this.notifyModelChanges(ctx, "Error", data);
					}
				});
			}
		},
		remove: {
			cache: true,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:ContactNotFound"));

				return Contact.remove({ _id: ctx.modelID })
					.then(() => {
						return ctx.model;
					})
					.then(json => {
						this.notifyModelChanges(ctx, "removed", json);
						return json;
					});
			}
		}
	},

	methods: {
		updatePhone(fbId, phone){
			return new Promise((resolve, reject)=>{
				let options = {
					upsert: true,
					new: true,
					setDefaultsOnInsert: true,	
					overwrite: false			 
				};				 
				this.collection
					.findOneAndUpdate(
						{ fb_id: fbId },
						{ $addToSet: {phones: phone}},
						options
					)
					.exec()
					.then(result => {					 
						resolve(this.toJSON(result));
					})
					.catch(err => {
						 reject(err);
					});
			});
			
		},
		updateOrInsert(id, model, cb) {
			return new Promise((resolve, reject)=>{
				let options = {
					upsert: true,
					new: true,
					setDefaultsOnInsert: true,	
					overwrite: false			 
				};
				let contact = _.cloneDeep(model);
			
				contact.fb_id = id;
				this.collection
				.findOneAndUpdate(
					{ fb_id: id },
					{ $set: contact },
					options
				)
				.exec()
				.then(result => {
					if(cb)
						cb(undefined, result);
					resolve(this.toJSON(result));
				})
				.catch(err => {
					if(cb)					
						cb(err, undefined);
					reject(err);
				});
			});
		},
		/**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 *
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		validateParams(ctx, strictMode) {
			if (strictMode || ctx.hasParam("name"))
				ctx.validateParam("name")
					.trim()
					.notEmpty(ctx.t("app:ContactNameCannotBeBlank"))
					.end();

			if (strictMode || ctx.hasParam("status"))
				ctx.validateParam("status").isNumber();

			ctx.validateParam("description")
				.trim()
				.end();
			ctx.validateParam("address")
				.trim()
				.end();
			ctx.validateParam("type")
				.trim()
				.end();

			if (ctx.hasValidationErrors())
				throw ctx.errorBadRequest(
					C.ERR_VALIDATION_ERROR,
					ctx.validationErrors
				);
		}
	},

	init(ctx) {
		// Fired when start the service
		this.pageService = ctx.services("pages");
	},

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		}
	},

	graphql: {
		query: `
			contacts(limit: Int, offset: Int, sort: String): [Contact]
			contact(code: String): Contact
		`,

		types: `
			type Contact {
				code: String!
				address: String
				type: String
				name: String
				description: String
				status: Int
				lastCommunication: Timestamp
			}
		`,

		mutation: `
		contactCreate(name: String!, address: String, type: String, description: String, status: Int): Contact
		contactUpdate(code: String!, name: String, address: String, type: String, description: String, status: Int): Contact
		contactRemove(code: String!): Contact
		`,

		resolvers: {
			Query: {
				contacts: "find",
				contact: "get"
			},

			Mutation: {
				contactCreate: "create",
				contactUpdate: "update",
				contactRemove: "remove"
			}
		}
	}
};

/*
## GraphiQL test ##

# Find all devices
query getDevices {
  devices(sort: "lastCommunication", limit: 5) {
    ...deviceFields
  }
}

# Create a new device
mutation createDevice {
  deviceCreate(name: "New device", address: "192.168.0.1", type: "raspberry", description: "My device", status: 1) {
    ...deviceFields
  }
}

# Get a device
query getDevice($code: String!) {
  device(code: $code) {
    ...deviceFields
  }
}

# Update an existing device
mutation updateDevice($code: String!) {
  deviceUpdate(code: $code, address: "127.0.0.1") {
    ...deviceFields
  }
}

# Remove a device
mutation removeDevice($code: String!) {
  deviceRemove(code: $code) {
    ...deviceFields
  }
}

fragment deviceFields on Device {
    code
    address
    type
    name
    description
    status
    lastCommunication
}

*/
