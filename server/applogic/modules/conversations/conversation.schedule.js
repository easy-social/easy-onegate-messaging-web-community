// sync conversation
const _ = require("lodash");
const moment = require("moment");
const agenda = require("../../../core/agenda");
const Services = require("../../../core/services");

const { fbGraphPageCallAsync } = require("./util");

agenda.define("facebook.syncConversation", async (job) => {
	// TODO: maybe expired => need update <div className=""></div>
	const { pageId, ptoken, sync, after } = job.attrs.data; // sync = need sync all ?
	const conversationService = Services.get("conversations");
	const contactService = Services.get("contacts");

	const result = {
		type: "CONVERSATION",
		num_success: 0,
		num_error: 0,
		error_logs: [],
		last: {},
		date_start: new Date(),
		date_stop: new Date(),
		date_end: new Date(),
	};

	const limit = 1000;

	const queryFields = {
		fields:
			"message_count,link,name,snippet,updated_time,unread_count,wallpaper,subject,senders,can_reply,scoped_thread_key,id,is_subscribed ,messages.limit(1){from}",
		limit,
	};

	if (after) {
		queryFields.after = after;
	}

	const model = [`/${pageId}/conversations`, "GET", queryFields];

	try {
		const conversations = await fbGraphPageCallAsync(ptoken, model);
		if (!conversations || conversations.data.length === 0) {
			result.date_end = new Date();
			result.error_logs.push("no-data");
			// clean missing
			await agenda
				.create(`facebook.syncConversation(page=${pageId})`, {
					CONVERSATION: result,
				})
				.save();

			job.remove();
			return [];
		}

		await Promise.all(
			conversations.data.map(async (c) => {
				c.fb_page = pageId;
				const from = c.senders.data.find((s) => s.id != pageId);
				if (from) {
					c.from = from;
					c.fb_fromId = from.id;
					const s = _.cloneDeep(c.from);

					await contactService.updateOrInsert(s.id, s, pageId);

					c.customerIds = [s.id];
					c.last_sent_by = c.messages.data[0].from;
					delete c.messages;

					if (
						c.unread_count > 0 &&
						moment().diff(moment(c.updated_time), "days") > 14
					) {
						c.unread_count = 0;
					}

					try {
						const saved = await conversationService.saveOrInsert(
							c.id,
							c
						);
						++result.num_success;

						// TODO: sync messages in conversation ...
						agenda
							.create("facebook.syncMessages", {
								//
								ptoken,
								pageId,
								conversation: c,
								sync: true, // TODO: need optimize
							})
							.save();
						return saved;
					} catch (err) {
						console.log("error on save conversation", err);
					}
				}

				return null;
			})
		);

		// pingback
		await agenda.purge();
		await agenda.create(`facebook.syncConversation(page=${pageId})`, {
			CONVERSATION: result,
		});

		// sync next page in background
		if (conversations.paging && conversations.paging.next && sync) {
			await agenda
				.create("facebook.syncConversation", {
					ptoken,
					pageId,
					after: conversations.paging.cursors.after,
				})
				.save();
		} else {
			// last of page ?
			result.date_end = new Date();
			result.last = conversations.data.pop();
		}
	} catch (err) {
		console.warn(err);
		result.num_error++;
		result.error_logs.push(err);
		result.date_end = new Date();
		result.last = model;

		await agenda.purge();
		await agenda.create(`facebook.syncConversation(page=${pageId})`, {
			CONVERSATION: result,
		});
	}

	job.remove();
});
