// sync messages in a conversation

const _ = require("lodash");
const moment = require("moment");
const agenda = require("../../../core/agenda");
const Services = require("../../../core/services");

const { fbGraphPageCallAsync } = require("./util");

agenda.define("facebook.syncMessages", async (job) => {
	const { page_id, ptoken, conversation, after } = job.attrs.data;

	const limit = 100;

	const feedService = Services.get("feeds");
	const conversationService = Services.get("conversations");
	const messageService = Services.get("messages");

	const result = {
		type: "MESSAGE",
		num_success: 0,
		num_error: 0,
		error_logs: [],
		last: {},
		date_start: new Date(),
		date_stop: new Date(),
		date_end: new Date(),
	};

	let queryFields = {
		fields:
			"created_time,from,id,message,sticker,tags,to,attachments{id,link,name, image_data, file_url, video_data, mime_type},shares{id,description,link, name}",
		limit,
	};

	if (after) {
		queryFields.after = after;
	}

	const model = [`/${conversation.id}/messages`, "GET", queryFields];

	try {
		const messages = await fbGraphPageCallAsync(ptoken, model);
		await Promise.all(
			messages.data.map(async (c) => {
				c.conversation_id = conversation.id;
				c.fb_page = conversation.fb_page;
				c.created_time = new Date(c.created_time);

				if (c.from.id !== conversation.fb_page) {
					const phoneObj = feedService.getPhoneNumber(
						c.message || ""
					);

					if (phoneObj.highlights.length) {
						c.phones = phoneObj.phones;
						c.highlights = phoneObj.highlights;

						if (!conversation.hasPhone) {
							await conversationService.updateFieldByConditions(
								null,
								{
									fb_id: conversation.fb_id,
								},
								{
									hasPhone: true,
								}
							);
						}
					}
				}

				await messageService.saveLocalMessage(null, c.from.id, c);
				++result.num_success;
			})
		);

		if (messages.pagin && messages.pagin.after) {
			await agenda
				.create("facebook.syncMessages", {
					ptoken,
					page_id,
					conversation,
					after: messages.pagin.cursors.after,
				})
				.save();
		} else {
			result.date_end = new Date();
			result.last = _.last(messages.data);

			await agenda.purge();
			// ping back

			await agenda
				.create(
					`facebook.syncMessages(conv=${conversation.id})`,
					result
				)
				.save();
		}
	} catch (err) {
		console.warn("err on save mess", err);
	}

	job.remove();
});
