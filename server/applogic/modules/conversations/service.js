"use strict";

let logger = require("../../../core/logger");
// let config = require("../../../config");
let C = require("../../../core/constants");

let _ = require("lodash");
let utils = require("../../libs/serviceUtil");
let Conversation = require("./models/conversation");
let FB = require("fb");
let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let ObjectId = Schema.ObjectId;
let CONTEXT = require("../../../core/context");
let { storage, config } = require("../../../core/firebaseService");
module.exports = {
	settings: {
		name: "conversations",
		version: 1,
		namespace: "conversations",
		rest: true,
		ws: true,
		graphql: true,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: Conversation,

		modelPropFilter: null, //"_id code message sni title content author votes voters views createdAt editedAt",

		modelPopulates: {
			author: "people",
			voters: "people",
		},
	},

	actions: {
		search: {
			handler(ctx) {
				return new Promise((resolve, reject) => {
					let messageService = this.context.services("messages");
					let commentService = this.context.services("comments");
					let errors = [];
					let results = [];
					let waits = [];
					let query = {};
					let filter = ctx.params.filter;
					let mLimit = ctx.params.limit ? ctx.params.limit : 100;
					let offset = ctx.params.offset ? ctx.params.offset : 0;
					let options = { sort: { created_time: -1 } };
					let conditions = ctx.params.condition;
					let fields = "conversation_id message";
					query = this.collection.find(filter, null, {
						sort: { created_time: -1 },
					});
					query
						// .populate({path: "comments", options, match: conditions})
						// .populate({path: "messages", options, match: conditions})
						// .populate(
						// 	"recent_seen_users"
						// )
						// .populate("customers")
						// .populate("post")
						// .populate("assignees")
						.exec()
						.then((docs) => {
							resolve(docs);
						})
						.catch((err) => {
							reject(err);
						});
					// MyModel.find({ name: /john/i }, null, { skip: 10 })
					if (filter) {
						waits.push(
							query
								.exec()
								.then((docs) => {
									results = _.concat(results, docs);
								})
								.catch((err) => {
									errors.push(err);
								})
						);
					}
					var mCondition = {};
					Object.keys(conditions).forEach((key) => {
						if (key == "fb_fromId") {
							mCondition["from.id"] = conditions[key];
						} else {
							mCondition[key] = conditions[key];
						}
						// console.log(key, conditions[key]);
					});
					waits.push(
						messageService
							.searchBy(mCondition, fields)
							.then((docs) => {
								results = _.concat(results, docs);
							})
							.catch((err) => {
								errors.push(err);
							})
					);
					waits.push(
						commentService
							.searchBy(conditions, fields)
							.then((docs) => {
								results = _.concat(results, docs);
							})
							.catch((err) => {
								errors.push(err);
							})
					);
					Promise.all(waits).then(() => {
						let rs = _.keyBy(results, "conversation_id");
						let ids = _.uniq(_.keys(rs));
						let qr = this.collection.find({ fb_id: { $in: ids } });
						ctx.queryPageSort(qr)
							.exec()
							.then((docs) => {
								_.forIn(docs, (doc) => {
									if (rs[doc.fb_id]) {
										doc.snippet = rs[doc.fb_id].message;
									}
								});
								resolve(docs);
							})
							.catch((err) => {
								reject(err);
							});

						// .populate({path: "comments", options, match: conditions})
						// .populate({path: "messages", options, match: conditions})
						// .populate(
						// 	"recent_seen_users"
						// )
						// .populate("customers")
						// .populate("post")
						// .populate("assignees")
						// .exec().then(docs=>{
						// 		resolve(docs);
						// 	}).catch(err=>{
						// 		reject(err);
						// 	});
						// 	//resolve(results);
						// }).catch(err=>{
						// 	reject(err);
						//
					});
				});
			},
		},
		fb: {
			handler(ctx) {
				return this.context.services("facebook").graph(ctx);
			},
		},
		fbConversations: {
			cache: false,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					this.context
						.services("facebook")
						.graph(ctx)
						.then((response) => {
							let fbconversations = response.conversations;
							let pageId = response.id;
							let data = [];
							if (fbconversations) {
								data = fbconversations.data;
								if (data) {
									data.forEach((d) => {
										d.fb_page = pageId;
										this.updateOrInsert(d.id, d);
									});
									//TODO return conversations offline and optain the fb_id
									resolve(fbconversations);
								}
							} else {
								resolve({ data: data });
							}
						})
						.catch((err) => {
							logger.error(err);
						});
				});
			},
		},
		find: {
			cache: false,
			handler(ctx) {
				// let filter = {};
				//
				// if (ctx.params.filter && ctx.params.filter.viewMode == "my")
				// 	filter.author = ctx.user.id;
				// else if (ctx.params.author != null) {
				// 	filter.author = this.context.services("people").decodeID(
				// 		ctx.params.author
				// 	);
				// }
				// if (ctx.params.condition)
				// 	filter = _.assign(filter, ctx.params.condition);
				// let query = this.collection.find(filter);
				//
				// return ctx
				// 	.queryPageSort(query)
				// 	.populate("recent_seen_users")
				// 	.populate("customers")
				// 	.populate("assignees")
				// 	.exec()
				// 	.then(docs => {
				// 		return this.toJSON(docs);
				// 	})
				// 	.then(json => {
				// 		return this.populateModels(json);
				// 	});
				return new Promise((resolve, reject) => {
					let filter = ctx.params.filter;
					let mLimit = ctx.params.limit ? ctx.params.limit : 100;
					let offset = ctx.params.offset ? ctx.params.offset : 0;
					let order = ctx.params.order
						? ctx.params.order
						: "-created_time";

					let options = { limit: mLimit, sort: order, skip: offset };

					if (ctx.params.filter == "my") filter.author = ctx.user.id;
					else if (ctx.params.author != null) {
						filter.author = this.personService.decodeID(
							ctx.params.author
						);
					}
					this.collection
						.find(filter, null, options)
						.populate("recent_seen_users")
						.populate("customers")
						.populate("assignees")
						.exec((err, docs) => {
							if (!err) {
								resolve(docs);
							} else {
								reject(err);
							}
						});
				});
			},
		},

		findBy: {
			cache: true,
			handler(ctx) {
				let keys,
					isAnd = undefined;
				if (ctx.params.conditions) {
					keys = ctx.params.conditions.keys
						? ctx.params.conditions.keys
						: [];
					isAnd = ctx.params.conditions.isAnd
						? ctx.params.conditions.isAnd
						: true;
				}
				let mLimit = ctx.params.limit ? ctx.params.limit : 100;
				let offset = ctx.params.offset ? ctx.params.offset : 0;
				let order = ctx.params.order
					? ctx.params.order
					: "created_time";
				let options = { limit: mLimit, sort: order, skip: offset };

				this.validateParams(ctx);
				let searchObj = {};
				if (isAnd) {
					_.forEach(keys, function (key) {
						searchObj[key] = ctx.params[key];
					});
				} else {
					let conditions = { $or: [] };
					_.forEach(keys, function (key) {
						let obj = [];
						obj[key] = ctx.params[key];
						conditions.$or.push(obj);
					});
				}
				return this.collection
					.find(searchObj)
					.populate({ path: "comments", options })
					.populate({ path: "messages", options })
					.populate("recent_seen_users")
					.populate("customers")
					.populate("post")
					.populate("assignees")
					.exec()
					.then((json) => {
						return this.populateModels(json);
					})
					.catch((err) => {
						ctx.assertModelIsExist(
							ctx.t("app:ContactSearchErrorFound")
						);
					});
			},
		},
		// return a model by ID
		get: {
			cache: false, // if true, we don't increment the views!
			permission: C.PERM_PUBLIC,
			handler(ctx) {
				//ctx.assertModelIsExist(ctx.t("app:ConversationNotFound"));
				let filter = ctx.params.filter;
				let mLimit = ctx.params.limit ? ctx.params.limit : 1;
				let offset = ctx.params.offset ? ctx.params.offset : 0;
				let options = {
					limit: mLimit,
					sort: { created_time: -1 },
					skip: offset,
				};
				return this.collection
					.findByIdAndUpdate(
						filter.id,
						{
							$inc: { views: 1 },
							$set: { unread_count: 0 },
							$addToSet: { recent_seen_userIds: ctx.user.id },
							last_seen_time: new Date(),
						},
						{ new: true }
					)
					.populate({ path: "comments", options })
					.populate({ path: "messages", options })
					.populate("recent_seen_users")
					.populate("customers")
					.populate("post")
					.populate("assignees")
					.exec()
					.then((doc) => {
						let fbctx = CONTEXT.CreateToServiceInit(
							this.context.services("notifications")
						);
						let objecToSend = this.toJSON(doc);
						objecToSend.keepPosition = true;
						let bro = {
							method: "conversationChanged",
							value: objecToSend,
						};
						fbctx.broadcast("notification", bro);
						return this.toJSON(doc);
					})
					.then((json) => {
						return this.populateModels(json);
					});
			},
		},

		create: {
			handler(ctx) {
				this.validateParams(ctx, true);

				let conversation = new Conversation({
					title: ctx.params.title,
					content: ctx.params.content,
					author: ctx.user.id,
				});

				return conversation
					.save()
					.then((doc) => {
						return this.toJSON(doc);
					})
					.then((json) => {
						return this.populateModels(json);
					})
					.then((json) => {
						this.notifyModelChanges(ctx, "created", json);
						return json;
					});
			},
		},
		updateOrInsert: {
			handler(ctx) {
				let key = ctx.params.fb_id;
				let object = _.cloneDeep(ctx.params);
				delete object.fb_id;
				delete object.id;
				if (key == undefined) {
					return { err: true, msg: "Not have id use" };
				}
				return this.updateOrInsertByKeys(key, object);
			},
		},

		update: {
			permission: C.PERM_OWNER,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:ConversationNotFound"));
				this.validateParams(ctx);

				return this.collection
					.findById(ctx.modelID)
					.exec()
					.then((doc) => {
						if (ctx.params.title != null)
							doc.title = ctx.params.title;

						if (ctx.params.content != null)
							doc.content = ctx.params.content;

						doc.editedAt = Date.now();
						return doc.save();
					})
					.then((doc) => {
						return this.toJSON(doc);
					})
					.then((json) => {
						return this.populateModels(json);
					})
					.then((json) => {
						this.notifyModelChanges(ctx, "updated", json);
						return json;
					});
			},
		},
		updateBy: {
			cache: false,
			handler(ctx) {
				let keys,
					isAnd = undefined;
				if (ctx.params.conditions) {
					keys = ctx.params.conditions.keys
						? ctx.params.conditions.keys
						: [];
					isAnd = ctx.params.conditions.isAnd
						? ctx.params.conditions.isAnd
						: true;
				}
				this.validateParams(ctx);
				let updateParams = {};
				if (ctx.params.operations) {
					updateParams = ctx.params.operations;
				}
				let searchObj = {};
				if (isAnd) {
					_.forEach(keys, function (key) {
						searchObj[key] = ctx.params[key];
					});
				} else {
					searchObj = { $or: [] };
					_.forEach(keys, function (key) {
						let obj = [];
						obj[key] = ctx.params[key];
						searchObj.$or.push(obj);
					});
				}
				return this.updateBy(
					ctx,
					searchObj,
					updateParams,
					undefined,
					ctx.params.push
				);
			},
		},

		remove: {
			permission: C.PERM_OWNER,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:ConversationNotFound"));

				return Conversation.remove({ _id: ctx.modelID })
					.then(() => {
						return ctx.model;
					})
					.then((json) => {
						this.notifyModelChanges(ctx, "removed", json);
						return json;
					});
			},
		},

		vote(ctx) {
			ctx.assertModelIsExist(ctx.t("app:ConversationNotFound"));

			return this.collection
				.findById(ctx.modelID)
				.exec()
				.then((doc) => {
					// Check user is on voters
					if (doc.voters.indexOf(ctx.user.id) !== -1)
						throw ctx.errorBadRequest(
							C.ERR_ALREADY_VOTED,
							ctx.t("app:YouHaveAlreadyVotedThisPost")
						);
					return doc;
				})
				.then((doc) => {
					// Add user to voters
					return Conversation.findByIdAndUpdate(
						doc.id,
						{
							$addToSet: { voters: ctx.user.id },
							$inc: { votes: 1 },
						},
						{ new: true }
					);
				})
				.then((doc) => {
					return this.toJSON(doc);
				})
				.then((json) => {
					return this.populateModels(json);
				})
				.then((json) => {
					this.notifyModelChanges(ctx, "voted", json);
					return json;
				});
		},

		unvote(ctx) {
			ctx.assertModelIsExist(ctx.t("app:ConversationNotFound"));

			return this.collection
				.findById(ctx.modelID)
				.exec()
				.then((doc) => {
					// Check user is on voters
					if (doc.voters.indexOf(ctx.user.id) == -1)
						throw ctx.errorBadRequest(
							C.ERR_NOT_VOTED_YET,
							ctx.t("app:YouHaveNotVotedThisConversationYet")
						);
					return doc;
				})
				.then((doc) => {
					// Remove user from voters
					return Conversation.findByIdAndUpdate(
						doc.id,
						{ $pull: { voters: ctx.user.id }, $inc: { votes: -1 } },
						{ new: true }
					);
				})
				.then((doc) => {
					return this.toJSON(doc);
				})
				.then((json) => {
					return this.populateModels(json);
				})
				.then((json) => {
					this.notifyModelChanges(ctx, "unvoted", json);
					return json;
				});
		},
	},

	methods: {
		handleWebhook(change, page_id, _ctx) {
			let fbService = this.context.services("facebook");
			let noService = this.context.services("notifications");
			let pageService = this.context.services("pages");
			let messageService = this.context.services("messages");
			let feedService = this.context.services("feeds");
			let contactService = this.context.services("contacts");
			let fbctx = _ctx.CreateToServiceInit(fbService);
			let noctx = _ctx.CreateToServiceInit(noService);
			let thread_id =
				change.sender.id != page_id
					? change.sender.id
					: change.recipient.id;
			pageService
				.getPageByFbId(page_id, "author")
				.then((data) => {
					if (data && data.length == 1) {
						fbService
							.fbgraphPagePreCall(
								fbctx.app.locals,
								data[0].author.socialLinks.facebook,
								page_id,
								[
									"/" + page_id + "/conversations",
									"GET",
									{
										fields:
											"message_count,link,name,snippet,updated_time,unread_count,wallpaper,subject,senders,can_reply,scoped_thread_key,id,is_subscribed" +
											(change.postback ||
											(change.referral &&
												change.referral.source == "ADS")
												? ",messages.limit(1){message,from,to}"
												: ""),
										user_id: thread_id,
									},
								]
							)
							.then((data2) => {
								if (data2) {
									let c = data2.data[0];
									let id = c.id;
									c.fb_page = page_id;
									c.fb_type = "INBOX";
									let phoneObj = undefined;
									if (
										change.postback ||
										(change.referral &&
											change.referral.source == "ADS")
									) {
										let from = c.messages.data[0].from;
										if (c.senders)
											c.customerIds = _.map(
												c.senders.data,
												(s) => {
													if (s.id != page_id) {
														contactService
															.checkAvatar(
																s,
																data[0]
															)
															.then((cont) => {
																if (
																	!cont.pic_profile
																) {
																	fbService
																		.fbgraphPagePreCall(
																			fbctx
																				.app
																				.locals,
																			data[0]
																				.author
																				.socialLinks
																				.facebook,
																			page_id,
																			[
																				"/" +
																					s.id,
																				"GET",
																			]
																		)
																		.then(
																			(
																				data3
																			) => {
																				if (
																					data3.profile_pic
																				) {
																					const fs = require("fs");
																					const bucket = storage.bucket(
																						config.storageBucket
																					);
																					var imageName =
																						"pfa" +
																						s.id;
																					var http = require("https");
																					var fileToDownload =
																						data3.profile_pic;
																					var remoteWriteStream = bucket
																						.file(
																							imageName
																						)
																						.createWriteStream();
																					var request = http.get(
																						fileToDownload,
																						(
																							response
																						) => {
																							response
																								.pipe(
																									remoteWriteStream
																								)
																								.on(
																									"error",
																									(
																										err
																									) => {}
																								)
																								.on(
																									"finish",
																									() => {
																										const publicUrl = `https://storage.googleapis.com/${bucket.name}/${imageName}`;
																										bucket
																											.file(
																												imageName
																											)
																											.makePublic(
																												() => {
																													contactService.updateOrInsert(
																														s.id,
																														{
																															pic_profile: publicUrl,
																														}
																													);
																													data3.profile_pic = publicUrl;
																													let bro = {
																														method:
																															"contactChanged",
																														value: data3,
																													};
																													noctx.broadcast(
																														"notification",
																														bro
																													);
																												}
																											);
																										// The file upload is complete.
																									}
																								);
																						}
																					);
																				}
																			}
																		)
																		.catch(
																			(
																				err
																			) =>
																				console.log(
																					err
																				)
																		);
																}
															});
														contactService.updateOrInsert(
															s.id,
															s,
															page_id
														);
														c.fb_fromId = s.id;
														c.from = s;
													}
													if (s.id == from.id) {
														c.last_sent_by = s;
													}
													return s.id;
												}
											);
									} else if (change.message) {
										if (c.senders)
											c.customerIds = _.map(
												c.senders.data,
												(s) => {
													if (s.id != page_id) {
														contactService
															.checkAvatar(
																s,
																data[0]
															)
															.then((cont) => {
																if (
																	!cont.pic_profile
																) {
																	fbService
																		.fbgraphPagePreCall(
																			fbctx
																				.app
																				.locals,
																			data[0]
																				.author
																				.socialLinks
																				.facebook,
																			page_id,
																			[
																				"/" +
																					s.id,
																				"GET",
																			]
																		)
																		.then(
																			(
																				data3
																			) => {
																				if (
																					data3.profile_pic
																				) {
																					const fs = require("fs");
																					const bucket = storage.bucket(
																						config.storageBucket
																					);
																					var imageName =
																						"pfa" +
																						s.id;
																					var http = require("https");
																					var fileToDownload =
																						data3.profile_pic;
																					var remoteWriteStream = bucket
																						.file(
																							imageName
																						)
																						.createWriteStream();
																					var request = http.get(
																						fileToDownload,
																						(
																							response
																						) => {
																							response
																								.pipe(
																									remoteWriteStream
																								)
																								.on(
																									"error",
																									(
																										err
																									) => {}
																								)
																								.on(
																									"finish",
																									() => {
																										const publicUrl = `https://storage.googleapis.com/${bucket.name}/${imageName}`;
																										bucket
																											.file(
																												imageName
																											)
																											.makePublic(
																												() => {
																													contactService.updateOrInsert(
																														s.id,
																														{
																															pic_profile: publicUrl,
																														},
																														page_id
																													);
																													data3.profile_pic = publicUrl;
																													let bro = {
																														method:
																															"contactChanged",
																														value: data3,
																													};
																													noctx.broadcast(
																														"notification",
																														bro
																													);
																												}
																											);
																										// The file upload is complete.
																									}
																								);
																						}
																					);
																				}
																			}
																		)
																		.catch(
																			(
																				err
																			) =>
																				console.log(
																					err
																				)
																		);
																}
															});
														contactService.updateOrInsert(
															s.id,
															s,
															page_id
														);
														c.fb_fromId = s.id;
														c.from = s;
													}
													if (
														s.id == change.sender.id
													) {
														c.last_sent_by = s;
													}
													return s.id;
												}
											);
									}
									if (change.message) {
										fbService
											.fbgraphPagePreCall(
												fbctx.app.locals,
												data[0].author.socialLinks
													.facebook,
												page_id,
												[
													"/" + change.message.mid,
													"GET",
													{
														fields:
															"created_time,from,id,message,sticker,tags,attachments{id,image_data,mime_type,name,size,video_data,file_url},shares{description,id,link,name}",
													},
												]
											)
											.then((messages) => {
												let message = messages;
												message.fb_page = page_id;
												message.conversation_id = c.id;
												message.created_time = new Date(
													message.created_time
												);
												if (
													change.message &&
													change.message.text &&
													change.sender.id != page_id
												) {
													phoneObj = feedService.getPhoneNumber(
														change.message.text
													);
													if (
														phoneObj.highlights
															.length > 0
													) {
														c.hasPhone = true;
														message.phones =
															phoneObj.phones;
														message.highlights =
															phoneObj.highlights;
													}
												}
												messageService
													.updateOrInsert(
														message.id,
														message
													)
													.then((mes) => {
														let bro = {
															method:
																"messageChanged",
															value: mes,
														};
														noctx.broadcast(
															"notification",
															bro
														);
														let params = {
															sent_by_page:
																change.sender
																	.id ==
																page_id,
															fb_contact_id:
																change.sender
																	.id ==
																page_id
																	? change
																			.recipient
																			.id
																	: change
																			.sender
																			.id,
															fb_conversation_id:
																c.id,
															type: "message",
															fb_page_id: page_id,
															fb_id: messages.id,
															user_id:
																mes &&
																mes.created_by &&
																mes.created_by
																	.uid
																	? mongoose.Types.ObjectId(
																			mes
																				.created_by
																				.uid
																	  )
																	: undefined,
															created_time:
																messages.created_time,
															phone_count:
																message.phones &&
																message.phones
																	.length > 0
																	? message
																			.phones
																			.length
																	: 0,
														};
														let statistics = this.context.services(
															"statistics"
														);
														statistics.createCount(
															fbctx,
															params
														);
													})
													.catch((err) => {});
												if (change.read) {
													c.unread_count = 0;
													c.isUnread = 0;
												}

												//update
												// this.updateBy(fbctx, {fb_id:page_id+'_'+c.fb_fromId},{fb_id:change.value.thread_id}).then(data=>{
												// 	noctx.broadcast("conversationChanged", con);
												// }).catch(err=>{});
												this.findOne(
													page_id + "_" + c.fb_fromId
												)
													.then((existThread) => {
														if (existThread) {
															c.fb_id = id;
															// searchId = page_id+'_'+c.fb_fromId;
															this.updateBy(
																fbctx,
																{
																	fb_id:
																		page_id +
																		"_" +
																		c.fb_fromId,
																},
																c
															)
																.then(
																	(data) => {
																		let bro = {
																			method:
																				"conversationChanged",
																			value: data,
																		};
																		noctx.broadcast(
																			"notification",
																			bro
																		);
																	}
																)
																.catch(
																	(err) => {}
																);
															messageService
																.updateBy(
																	fbctx,
																	{
																		conversation_id:
																			page_id +
																			"_" +
																			c.fb_fromId,
																	},
																	{
																		fb_id:
																			c.id,
																	}
																)
																.then(
																	(data) => {
																		// noctx.broadcast("conversationChanged", data);
																	}
																)
																.catch(
																	(err) => {}
																);
														} else {
															this.updateOrInsert(
																id,
																c,
																undefined,
																{
																	limit: 1,
																	new: true,
																}
															).then((con) => {
																let bro = {
																	method:
																		"conversationChanged",
																	value: con,
																};
																noctx.broadcast(
																	"notification",
																	bro
																);
															});
														}
													})
													.catch((err) => {
														logger.error(
															"Conversation/service/handleWebhook/err2"
														);
														logger.error(err);
													});
											});
									} else if (
										change.postback ||
										(change.referral &&
											change.referral.source == "ADS")
									) {
										if (change.read) {
											c.unread_count = 0;
											c.isUnread = 0;
										}

										//update
										// this.updateBy(fbctx, {fb_id:page_id+'_'+c.fb_fromId},{fb_id:change.value.thread_id}).then(data=>{
										// 	noctx.broadcast("conversationChanged", con);
										// }).catch(err=>{});
										this.findOne(
											page_id + "_" + c.fb_fromId
										)
											.then((existThread) => {
												if (existThread) {
													c.fb_id = id;
													// searchId = page_id+'_'+c.fb_fromId;
													if (
														change.referral &&
														change.referral.ad_id &&
														change.referral.ad_id !=
															""
													) {
														let up = {
															$set: c,
															$addToSet: {
																ads_ids:
																	change
																		.referral
																		.ad_id,
															},
														};
														// c.$addToSet = {"ads_ids": change.referral.ad_id};
													} else {
														let up = c;
													}
													this.updateBy(
														fbctx,
														{
															fb_id:
																page_id +
																"_" +
																c.fb_fromId,
														},
														up
													)
														.then((data) => {
															let bro = {
																method:
																	"conversationChanged",
																value: data,
															};
															noctx.broadcast(
																"notification",
																bro
															);
														})
														.catch((err) => {});
													messageService
														.updateBy(
															fbctx,
															{
																conversation_id:
																	page_id +
																	"_" +
																	c.fb_fromId,
															},
															{ fb_id: c.id }
														)
														.then((data) => {
															// noctx.broadcast("conversationChanged", data);
														})
														.catch((err) => {});
												} else {
													let up = {};
													if (
														change.referral &&
														change.referral.ad_id &&
														change.referral.ad_id !=
															""
													) {
														up = {
															$set: c,
															$addToSet: {
																ads_ids:
																	change
																		.referral
																		.ad_id,
															},
														};
														// console.log(change.referral.ad_id)
														// up.$addToSet = {"ads_ids": change.referral.ad_id}
														// c.$addToSet = {"ads_ids": change.referral.ad_id};
													} else {
														up = c;
													}
													this.updateOrInsert(
														id,
														up,
														undefined,
														{ limit: 1, new: true }
													).then((con) => {
														let bro = {
															method:
																"conversationChanged",
															value: con,
														};
														noctx.broadcast(
															"notification",
															bro
														);
													});
												}
											})
											.catch((err) => {
												logger.error(
													"Conversation/service/handleWebhook/err2"
												);
												logger.error(err);
											});
										c.fb_id = id;
										fbService.syncAConversation({
											pageId: page_id,
											author: data[0].author,
											conversation: c,
											isSynchonize: true,
											limit: 100,
										});
									} else if (change.read) {
										if (change.sender.id !== page_id) {
											c.read_watermark = new Date(
												change.read.watermark
											);
										}
										if (
											(c.last_sent_by == page_id &&
												change.sender.id !== page_id) ||
											(c.last_sent_by != page_id &&
												change.sender.id == page_id)
										) {
											c.unread_count = 0;
											c.isUnread = 0;
										}
										this.findOne(c.id)
											.then((existThread) => {
												if (existThread) {
													c.fb_id = id;
													// searchId = page_id+'_'+c.fb_fromId;
													this.updateBy(
														fbctx,
														{ fb_id: c.id },
														c
													)
														.then(() => {
															// let bro = {method: "conversationChanged", value: data[0]};
															// noctx.broadcast("notification", bro);
														})
														.catch((err) => {});
												}
											})
											.catch((err) => {
												logger.error(
													"Conversation/service/handleWebhook/err2"
												);
												logger.error(err);
											});
									}
								}
							})
							.catch((err) => {
								logger.error(
									"Conversation/service/handleWebhook"
								);
								logger.error(err);
							});
					}
				})
				.catch((error) => {
					logger.error(error);
				});
		},
		updateBy(ctx, searchObj, updateParams, options, push_back) {
			/*return this.collection
				.update(
					searchObj,
					updateParams,
					options
				)
				.exec()
				.then(jsons => {
					return this.populateModels(jsons);
				})
				.then(jsons => {
					if(push_back){
						let fbctx = CONTEXT.CreateToServiceInit(this.context.services("notifications"));
						jsons.forEach(item=>{
							let objecToSend = this.toJSON(item);
							objecToSend.keepPosition = true
							noctx.broadcast("conversationChanged", objecToSend);
						});
					}
					// this.notifyModelChanges(ctx, "updated", jsons);
					return jsons;
				})
				.catch(err => {
					ctx.assertModelIsExist(
						ctx.t("app:ConversationUpdateErrorFound")
					);
					return err;
				});*/
			return this.collection.find(
				searchObj,
				(err, existingConversations) => {
					if (existingConversations.length == 0) {
						return [];
					} else if (existingConversations.length > 0) {
						let promises = [];
						let results = [];
						options = options
							? options
							: {
									// upsert: true,
									new: true,
									setDefaultsOnInsert: true,
							  };

						existingConversations.forEach((item) => {
							promises.push(
								this.collection
									.findByIdAndUpdate(
										item.id,
										updateParams,
										options
									)
									.populate("recent_seen_users")
									.populate("customers")
									// .populate("post")
									.populate("assignees")
									.exec()
									.then((json) => {
										if (push_back) {
											let noctx = CONTEXT.CreateToServiceInit(
												this.context.services(
													"notifications"
												)
											);
											let objecToSend = this.toJSON(json);
											objecToSend.keepPosition = true;
											let bro = {
												method: "conversationChanged",
												value: objecToSend,
											};
											noctx.broadcast(
												"notification",
												bro
											);
										} else {
											let noctx = CONTEXT.CreateToServiceInit(
												this.context.services(
													"notifications"
												)
											);
											let bro = {
												method: "conversationChanged",
												value: this.toJSON(json),
											};
											noctx.broadcast(
												"notification",
												bro
											);
										}
										results.push(json);
									})
									.catch((err) => {
										results.push(err);
									})
							);
						});

						if (promises.length > 0) {
							Promise.all(promises).then((response) => {
								return results;
							});
						}
						if (
							updateParams &&
							updateParams.$addToSet &&
							updateParams.$addToSet.tags
						) {
							// let settingService = this.context.services("settings");
							// settingService.search({
							// 		fb_id: searchObj.fb_page,
							// 		type: {
							// 			$in: ['tagsSetting']
							// 		},
							// }).then(data => {
							// 	if(data && data[0] && data[0].tags){
							// 		let taginfo = data[0].tags.find((tg) => {return tg.id === updateParams.$addToSet.tags})
							// 		if(taginfo) {
							let tagsService = this.context.services("tags");
							tagsService.createCount(ctx, {
								// name: taginfo.name,
								code: updateParams.$addToSet.tags,
								// color: taginfo.backgroundcolor,
								fb_page: searchObj.fb_page,
								fb_id: searchObj.fb_id,
								// created_time: new Date(),
								// created_by_id: ctx.user.id
							});
							// 		}
							// 	}
							// }).catch(err => {
							// 	// toast.error(this._("Error"));
							// 	console.log(err);
							// });
						} else if (
							updateParams &&
							updateParams.$pull &&
							updateParams.$pull.tags
						) {
							let tagsService = this.context.services("tags");
							tagsService.removeCount(ctx, {
								tagcode: updateParams.$pull.tags,
								fb_id: searchObj.fb_id,
							});
						}
					} else {
						let data = {
							success: false,
							msg: "Unknown",
							data: existingConversations,
						};
						return this.notifyModelChanges(ctx, "Error", data);
					}
				}
			);
		},

		// ctx is unused ?
		updateFieldByConditions(ctx, conditions, fields, options) {
			return this.collection
				.update(conditions, fields, options)
				.exec()
				.then((docs) => {
					console.log(docs);
				})
				.catch((err) => {
					console.error(err);
				});
		},
		findOne(key) {
			return new Promise((resolve, reject) => {
				this.collection
					.findOne({ fb_id: key })
					.then((data) => {
						resolve(data);
					})
					.catch((err) => {
						reject(err);
					});
			});
		},
		updateOrInsertByKeys(key, object, ctx) {
			return new Promise((resolve, reject) => {
				let options = {
					upsert: true,
					new: true,
					setDefaultsOnInsert: true,
					overwrite: false,
				};
				let command = { $set: object };

				if (object.extraUpdate) {
					command = _.assign(command, object.extraUpdate);
					delete command.$set.extraUpdate;
				}

				this.collection
					.findOneAndUpdate({ fb_id: key }, command, options)
					.exec()
					.then((docs) => {
						if (ctx) {
							this.notifyModelChanges(
								ctx,
								"conversationChanged",
								docs
							);
						}
						resolve(this.toJSON(docs));
					})
					.catch((err) => {
						reject(err);
					});
			});
		},
		updatePhoneToContact(cfbId, model) {
			return new Promise((resolve, reject) => {
				if (model.messages) {
					_.forEach(model.messages.data, (cm) => {
						//console.log("update phone number ==============", cm, cm.from, cm.message)
						if (
							cm.from &&
							cm.message &&
							utils.isPhoneNumber(cm.message)
						) {
							let phone = utils.messageToPhone(cm.message);
							this.context
								.services("contacts")
								.updatePhone(cm.from.id, phone)
								.then((result) => {
									resolve(result);
								})
								.catch((err) => {
									reject(err);
								});
						}
					});
				}
			});
		},
		updateOrInsert(id, model, cb, populatedOptions) {
			return new Promise((resolve, reject) => {
				let object = model.$set
					? _.cloneDeep(model.$set)
					: _.cloneDeep(model);
				object.fb_id = id;
				delete object.messages;
				delete object.id;
				let options = {
					upsert: true,
					new: true,
					setDefaultsOnInsert: true,
					overwrite: false,
				};
				object.fb_type = "INBOX";
				let field = {};
				if (model.$set) {
					field = _.cloneDeep(model);
					field.$set = object;
				} else {
					field = { $set: object };
				}
				this.collection
					.findOneAndUpdate({ fb_id: id }, field, options)
					.populate("comments")
					.populate({ path: "messages", options: populatedOptions })
					.populate(
						"recent_seen_users",
						"_id, fullName, username, profile.name, profile.picture, fb_id"
					)
					.populate({
						path: "customers",
						select: "fb_id name email",
					})
					.populate("post")
					.populate("assignees")
					.exec()
					.then((docs) => {
						if (cb) cb(undefined, docs);
						resolve(this.toJSON(docs));
					})
					.catch((err) => {
						if (cb) cb(err, undefined);
						reject(err);
					});
			});
		},
		saveOrInsert(id, model, cb, populatedOptions) {
			return new Promise((resolve, reject) => {
				let object = _.cloneDeep(model);
				object.fb_id = id;
				delete object.messages;
				delete object.id;
				let options = {
					upsert: true,
					new: true,
					setDefaultsOnInsert: true,
					overwrite: false,
				};
				object.fb_type = "INBOX";
				this.collection
					.findOneAndUpdate({ fb_id: id }, { $set: object }, options)
					.exec()
					.then((docs) => {
						if (cb) cb(undefined, docs);
						resolve(this.toJSON(docs));
					})
					.catch((err) => {
						if (cb) cb(err, undefined);
						reject(err);
					});
			});
		},
		updateOrInsertFromComment(id, model, cb) {
			return new Promise((resolve, reject) => {
				let object = _.cloneDeep(model);
				object.comment_id = id;
				object.updated_time = object.created_time;
				object.snippet = object.snippet
					? object.snippet
					: object.message;
				object.fb_type = "COMMENT";
				object.message_count = object.comments
					? object.comments.data.length
					: 0;
				delete object.comments;
				delete object.id;
				let options = {
					upsert: true,
					new: true,
					setDefaultsOnInsert: true,
					overwrite: false,
				};
				if (object.from) {
					object.fb_id = object.postId + "_" + object.from.id;
					this.collection
						.findOneAndUpdate(
							{
								//fb_id: id,
								fb_postId: object.postId,
								fb_fromId: object.from.id,
							},
							{ $set: object },
							options
						)
						.populate("comments")
						.populate("messages")
						.populate(
							"recent_seen_users",
							"_id, fullName, username, profile.name, profile.picture, fb_id"
						)
						.populate("customers", "fb_id, name, email")
						.populate("post")
						.populate("assignees")
						.exec()
						.then((docs) => {
							if (cb) cb(undefined, docs);
							resolve(this.toJSON(docs));
						})
						.catch((err) => {
							if (cb) cb(err, undefined);
							reject(err);
						});
				}
			});
		},
		saveOrInsertFromComment(id, model, cb) {
			return new Promise((resolve, reject) => {
				let object = _.cloneDeep(model);
				object.comment_id = id;
				// object.updated_time = object.created_time;
				// object.snippet = object.snippet ? object.snippet : object.message;
				object.fb_type = "COMMENT";
				object.message_count = object.comments
					? object.comments.data.length
					: 0;
				delete object.comments;
				delete object.id;
				let options = {
					upsert: true,
					new: true,
					setDefaultsOnInsert: true,
					overwrite: false,
				};
				if (object.from) {
					object.fb_id = object.postId + "_" + object.from.id;
					this.collection
						.findOneAndUpdate(
							{
								//fb_id: id,
								fb_postId: object.postId,
								fb_fromId: object.from.id,
							},
							{ $set: object },
							options
						)
						.exec()
						.then((docs) => {
							if (cb) cb(undefined, docs);
							resolve(this.toJSON(docs));
						})
						.catch((err) => {
							if (cb) cb(err, undefined);
							reject(err);
						});
				}
			});
		},
		assigEmployee(id, model, cb) {
			return new Promise((resolve, reject) => {
				let options = {
					upsert: true,
					new: true,
					setDefaultsOnInsert: true,
					overwrite: false,
				};
				this.collection
					.findByIdAndUpdate(
						id,
						{ assignee_ids: model.assignee_ids },
						options
					)
					.populate("comments")
					.populate("messages")
					.populate(
						"recent_seen_users",
						"_id, fullName, username, profile.name, profile.picture, fb_id"
					)
					.populate("customers", "fb_id, name, email")
					.populate("post")
					.populate("assignees")
					.exec()
					.then((docs) => {
						if (cb) cb(undefined, docs);
						resolve(this.toJSON(docs));
					})
					.catch((err) => {
						if (cb) cb(err, undefined);
						reject(err);
					});
			});
		},
		/**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 *
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		validateParams(ctx, strictMode) {
			if (strictMode || ctx.hasParam("title"))
				ctx.validateParam("title")
					.trim()
					.notEmpty(ctx.t("app:ConversationTitleCannotBeEmpty"))
					.end();

			if (strictMode || ctx.hasParam("content"))
				ctx.validateParam("content")
					.trim()
					.notEmpty(ctx.t("app:ConversationContentCannotBeEmpty"))
					.end();

			if (ctx.hasValidationErrors())
				throw ctx.errorBadRequest(
					C.ERR_VALIDATION_ERROR,
					ctx.validationErrors
				);
		},
	},

	/**
	 * Check the owner of model
	 *
	 * @param {any} ctx	Context of request
	 * @returns	{Promise}
	 */
	ownerChecker(ctx) {
		return new Promise((resolve, reject) => {
			ctx.assertModelIsExist(ctx.t("app:ConversationNotFound"));

			if (ctx.model.author.code == ctx.user.code || ctx.isAdmin())
				resolve();
			else reject();
		});
	},

	init(ctx) {
		// Fired when start the service
		// this.contactServce = ctx.services("contacts");
		// this.facebookServce = ctx.services("facebook");
		// this.mesasgeServce = ctx.services("messages");
		// Fired when start the service
		// this.personService = ctx.services("people");
		this.context = ctx;

		// Add custom error types
		C.append(["ALREADY_VOTED", "NOT_VOTED_YET"], "ERR");
	},

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		},
	},

	graphql: {
		query: `
		conversations(limit: Int, offset: Int, sort: String): [Conversation]
		conversation(code: String): Conversation
		`,

		types: `
			type Conversation {
				code: String!
				title: String
				content: String
				author: Person!
				views: Int
				votes: Int,
				voters(limit: Int, offset: Int, sort: String): [Person]
				createdAt: Timestamp
				createdAt: Timestamp
			}
		`,

		mutation: `
		conversationCreate(title: String!, content: String!): Conversation
		conversationUpdate(code: String!, title: String, content: String): Conversation
		conversationRemove(code: String!): Conversation

		conversationVote(code: String!): Conversation
		conversationUnvote(code: String!): Conversation
		`,

		resolvers: {
			Query: {
				conversations: "find",
				conversation: "get",
			},

			Mutation: {
				conversationCreate: "create",
				conversationUpdate: "update",
				conversationRemove: "remove",
				conversationVote: "vote",
				conversationUnvote: "unvote",
			},
		},
	},
};

/*
## GraphiQL test ##

# Find all posts
query getPosts {
  posts(sort: "-createdAt -votes", limit: 3) {
    ...postFields
  }
}

# Create a new post
mutation createPost {
  postCreate(title: "New post", content: "New post content") {
    ...postFields
  }
}

# Get a post
query getPost($code: String!) {
  post(code: $code) {
    ...postFields
  }
}

# Update an existing post
mutation updatePost($code: String!) {
  postUpdate(code: $code, content: "Modified post content") {
    ...postFields
  }
}

# vote the post
mutation votePost($code: String!) {
  postVote(code: $code) {
    ...postFields
  }
}

# unvote the post
mutation unVotePost($code: String!) {
  postUnvote(code: $code) {
    ...postFields
  }
}

# Remove a post
mutation removePost($code: String!) {
  postRemove(code: $code) {
    ...postFields
  }
}



fragment postFields on Post {
    code
    title
    content
    author {
      code
      fullName
      username
      avatar
    }
    views
    votes
  	voters {
  	  code
  	  fullName
  	  username
  	  avatar
  	}
}

*/
