"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let db	    		= require("../../../../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../../../../libs/hashids")("devices");
let autoIncrement 	= require("mongoose-auto-increment");

let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	}
};

let FolderSchema = new Schema({
	name: {
		type: String,
		trim: true
	},
	imageCount: {
		type: Number,
		"default": 0
	},
	fb_page: {
		type: String,
	},
	metadata: {}

}, schemaOptions);

FolderSchema.virtual("code").get(function() {
	return this.encodeID();
});

FolderSchema.plugin(autoIncrement.plugin, {
	model: "Folder",
	startAt: 1
});

FolderSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

FolderSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};

let Folder = mongoose.model("Folder", FolderSchema);

module.exports = Folder;
