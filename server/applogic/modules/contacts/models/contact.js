"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let db	    		= require("../../../../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../../../../libs/hashids")("contacts");
let autoIncrement 	= require("mongoose-auto-increment");
let mongoosastic    = require("mongoosastic");

let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	},
	strict: false
};

let ContactSchema = new Schema({
	// _id: {
	// 	type: String,
	// 	trim: true,
	// 	unique: true
	// },
	fb_id: {
		type: String,
		trim: true,
		unique: true,
		sparse: true
	},
	address: {
		type: String,
		trim: true,
		es_indexed:true
	},
	fb_link: {
		type: String,
		trim: true
	},
	fb_pages: [ {
		type: String,
	}],
	banned_pages : [
		{
			type: String,
		}
	],
	type: {
		type: String,
		trim: true,
		"default": "hot"
	},
	name: {
		type: String,
		trim: true,
		es_indexed:true
	},
	phones: [ {
		type: String,
		trim: true
	}],
	emails: [ {
		type: String,
		trim: true
	}],
	crm_phone: {
		type: String,
		trim: true
	},
	crm_email:{
		type: String,
		trim: true
	},
	crm_id: {
		type: String,
		trim: true
	},
	description: {
		type: String,
		trim: true,
		"default": ""
	},
	status: {
		type: Number,
		"default": 1
	},
	lastCommunication: {
		type: Date,
		"default": Date.now
	},
	pic_profile: { type: String },
	crmId: {
		type: String,
		trim: true,
	},
	metadata: {}

}, schemaOptions);

ContactSchema.virtual("code").get(function() {
	return this.encodeID();
});

// ContactSchema.plugin(autoIncrement.plugin, {
// 	model: "Contact",
// 	startAt: 1
// });
//
if(config.elastic && config.elastic.enabled)
	ContactSchema.plugin(mongoosastic, {
		hosts: [config.elastic.host1],
	  customProperties: {
		name: {
		  type: "text",
		},
		address: {
		  type: "text",
		}
	  }
	});

ContactSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

ContactSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};

let Contact = mongoose.model("Contact", ContactSchema);

module.exports = Contact;
