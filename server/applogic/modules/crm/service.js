"use strict";
const intformat = require("biguint-format");
const FlakeId = require("flake-idgen");
const moment = require("moment");
const flakeIdGen = new FlakeId();

let logger = require("../../../core/logger");
let config = require("../../../config");
let C = require("../../../core/constants");

let _ = require("lodash");
let mongoose = require("mongoose");
let CONTEXT = require("../../../core/context");
let Order = require("./models/order");

let SessionId = null;
let UserCRM = null;

const tempRequest = {
	RequestClass: "xBase",
	RequestAction: "Request",
	TotalRequests: 1
};

module.exports = {
	settings: {
		name: "crm",
		version: 1,
		namespace: "crm",
		rest: true,
		ws: true,
		els: config.elastic.enabled,
		graphql: true,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: Order,

		modelPropFilter: null,
		// "code type address name description status lastCommunication createdAt updatedAt"
		modelPopulates: {
			fb_page: "page"
		}
	},

	actions: {
		/**
		 * CRM API
		 */
		getContactCRM: {
			cache: false,
			handler(ctx) {
				return {};
			}
		},
		createContactCRM: {
			cache: false,
			handler(ctx) {
				return ctx.req.body;
			}
		},
		updateContactCRM: {
			cache: false,
			handler(ctx) {
				return ctx.req.body;
			}
		},
		getOrdersCRM: {
			cache: false,
			handler(ctx) {
				return [];
			}
		},
		getSalesCRM: {
			cache: false,
			handler(ctx) {
				return [];
			}
		},
		getProductsCRM: {
			cache: false,
			handler(ctx) {
				return [];
			}
		},
		createOrderCRM: {
			cache: false,
			handler(ctx) {
				return ctx.req.body;
			}
		},

		/**
		 * method for elastic search
		 */
		elsearch: {
			cache: false,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					let terms = ctx.req.body.terms;
					if (!terms || !this.$settings.els)
						resolve({contacts: []});

					this.collection.search(
						{query_string: {query: terms}},
						function (err, results) {
							if (err) logger.error(err);
							resolve({contacts: results.hits.hits});
						}
					);
				});
			}
		},
		search: {
			cache: false,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					let filter = {};
					if (ctx.params && ctx.params.filter) {
						if (ctx.params.filter === "all")
							filter["fb_pages"] = {
								$in: ctx.user.profile.accessPages
							};
						else if (ctx.params.filter === "my")
							filter["assistants"] = {$in: [ctx.user.id]};
						else if (ctx.params.filter.indexOf("=") > -1) {
							let fs = ctx.params.filter.split("=");
							filter[fs[0]] = fs[1];
						} else if (ctx.params.filter.indexOf(":") > -1) {
							let fs = ctx.params.filter.split(":");
							if (fs[1] == "true")
								filter[fs[0]] = {$gt: -Infinity};
						}
					}
					let query = this.collection.find(filter);
					ctx.queryPageSort(query)
						.exec()
						.then(docs => {
							resolve(this.toJSON(docs));
						})
						.catch(err => {
							logger.error(err);
							reject(err);
						});
				});
			}
		},
		find: {
			cache: false,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					let filter = ctx.params ? ctx.params : {};
					let currentUser = ctx.user;
					if (
						currentUser &&
						currentUser.provider === "facebook" &&
						currentUser.passwordLess
					) {
						let cModel = [
							"/me",
							"GET",
							{fields: "id,accounts{id,access_token}"}
						];
						this.pageService
							.callToFb(cModel, currentUser.profile.access_token)
							.then(pages => {
								logger.info("order pages loaded", pages);
								if (pages.accounts && pages.accounts.data) {
									logger.info(filter);
									let pageIds = _.map(
										pages.accounts.data,
										"id"
									);
									filter = _.assign(filter, {
										fb_pages: {$in: pageIds}
									});
									logger.info(filter);
									let query = Order.find(filter);
									ctx.queryPageSort(query)
										.exec()
										.then(docs => {
											console.log(
												"filter doc by pages ",
												docs
											);
											resolve(this.toJSON(docs));
										})
										.catch(err => {
											ctx.assertModelIsExist(
												ctx.t("app:ContactNotFound")
											);
											resolve(ctx.model);
										});
								}
							})
							.catch(err => {
								console.log(err);
								ctx.assertModelIsExist(
									ctx.t("app:ContactNotFound")
								);
								resolve(ctx.model);
							});

					} else {
						ctx.assertModelIsExist(ctx.t("app:ContactNotFound"));
						resolve(ctx.model);
					}
				});
			}
		},
		get: {
			cache: true,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:ContactNotFound"));
				return Promise.resolve(ctx.model);
			}
		},
		create: {
			cache: true,
			handler(ctx) {
				this.validateParams(ctx, true);
				let order = new Order(ctx.params);
				return order
					.save()
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					})
					.then(json => {
						this.notifyModelChanges(ctx, "created", json);
						return json;
					});
			}
		},
		update: {
			cache: true,
			handler(ctx, id) {
				ctx.assertModelIsExist(ctx.t("app:ContactNotFound"));
				let modelID = id ? id : ctx.modelID;
				let params = ctx.params;
				let docs = {};
				_.forOwn(params, function (value, key) {
					if (
						value != null &&
						key != "modelID" &&
						key.indexOf("_") == -1
					) {
						docs[key] = value;
					}
				});
				let options = {
					upsert: true,
					new: true,
					setDefaultsOnInsert: true
				};
				return (
					this.collection
						.findOneAndUpdate(
							{_id: modelID},
							{$set: docs},
							options
						)
						.exec()
						.then(doc => {
							return this.toJSON(doc);
						})
						.then(json => {
							return this.populateModels(json);
						})
						.then(json => {
							this.notifyModelChanges(ctx, "updated", json);
							return json;
						})
						.catch(err => {
							console.log(err);
							ctx.assertModelIsExist(
								ctx.t("app:ContactUpdateErrorFound")
							);
						})
				);
			}
		},
		findAndUpdate: {
			cache: true,
			handler(ctx) {
				let filter = ctx.params.condition;
				let updateFields = ctx.params.updateFields;
				let options = {
					new: true
				};
				//console.log("prepare to save================", modelID, _.cloneDeep(docs))
				return this.collection
					.findOneAndUpdate(filter, updateFields, options)
					.exec()
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						this.notifyModelChanges(ctx, "updated", json);
						return json;
					})
					.catch(err => {
						console.log(err);
						ctx.assertModelIsExist(
							ctx.t("app:ContactUpdateErrorFound")
						);
					});
			}
		},
		findBy: {
			cache: false,
			handler(ctx) {
				let keys,
					isAnd = undefined;
				if (ctx.params.conditions) {
					keys = ctx.params.conditions.keys
						? ctx.params.conditions.keys
						: [];
					isAnd = ctx.params.conditions.isAnd
						? ctx.params.conditions.isAnd
						: true;
				}
				this.validateParams(ctx);
				let searchObj = {};
				if (isAnd) {
					_.forEach(keys, function (key) {
						searchObj[key] = ctx.params[key];
					});
				} else {
					let conditions = {$or: []};
					_.forEach(keys, function (key) {
						let obj = [];
						obj[key] = ctx.params[key];
						conditions.$or.push(obj);
					});
				}
				return this.collection
					.find(searchObj)
					.exec()
					.then(json => {
						return this.populateModels(json);
					})
					.catch(err => {
						ctx.assertModelIsExist(
							ctx.t("app:ContactSearchErrorFound")
						);
					});
			}
		},
		updateBy: {
			cache: false,
			handler(ctx) {
				let keys,
					isAnd = undefined;
				if (ctx.params.conditions) {
					keys = ctx.params.conditions.keys
						? ctx.params.conditions.keys
						: [];
					isAnd = ctx.params.conditions.isAnd
						? ctx.params.conditions.isAnd
						: true;
				}
				this.validateParams(ctx);
				let updateParams = {};
				if (ctx.params.operations) {
					updateParams = ctx.params.operations;
				}
				let searchObj = {};
				if (isAnd) {
					_.forEach(keys, function (key) {
						searchObj[key] = ctx.params[key];
					});
				} else {
					searchObj = {$or: []};
					_.forEach(keys, function (key) {
						let obj = [];
						obj[key] = ctx.params[key];
						searchObj.$or.push(obj);
					});
				}
				return this.updateBy(
					ctx,
					searchObj,
					updateParams,
					undefined,
					ctx.params.push
				);
			}
		},
		updatePhones: {
			cache: true,
			handler(ctx) {
				this.validateParams(ctx);
				if (
					ctx.params.phones &&
					ctx.params.phones.length > 0 &&
					ctx.params.fb_id
				) {
					return this.updatePhones(
						ctx.params.fb_id,
						ctx.params.phones
					);
				} else {
					ctx.assertModelIsExist(ctx.t("app:NotEnoughParams"));
				}
			}
		},
		remove: {
			cache: true,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:ContactNotFound"));

				return Order.remove({_id: ctx.modelID})
					.then(() => {
						return ctx.model;
					})
					.then(json => {
						this.notifyModelChanges(ctx, "removed", json);
						return json;
					});
			}
		}
	},

	methods: {

	},

	init(ctx) {
		// Fired when start the service
		this.pageService = ctx.services("pages");
		this.sessionService = ctx.services("session");
	},

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		}
	},

	graphql: {
		query: `
			orders(limit: Int, offset: Int, sort: String): [Order]
			order(code: String): Order
		`,

		types: `
			type Order {
				code: String!
				address: String
				type: String
				name: String
				description: String
				status: Int
				lastCommunication: Timestamp
			}
		`,

		mutation: `
		orderCreate(name: String!, address: String, type: String, description: String, status: Int): Order
		orderUpdate(code: String!, name: String, address: String, type: String, description: String, status: Int): Order
		orderRemove(code: String!): Order
		`,

		resolvers: {
			Query: {
				orders: "find",
				order: "get"
			},

			Mutation: {
				orderCreate: "create",
				orderUpdate: "update",
				orderRemove: "remove"
			}
		}
	}
};
