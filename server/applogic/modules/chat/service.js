"use strict";

let logger 		= require("../../../core/logger");
let config 		= require("../../../config");
let C 	 		= require("../../../core/constants");

let _			= require("lodash");
let utils		= require("../../libs/serviceUtil");
let chat 		= require("./models/chat");
let comment     = require("./models/comment"); 
let message     = require("./models/messages"); 
let FB = require("fb");
module.exports = {
	settings: {
		name: "chat",
		version: 1,
		namespace: "chat",
		rest: true,
		ws: true,
		graphql: true,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: chat,
		comment: comment,
		message: message,

		modelPropFilter: "code title content author votes voters views createdAt editedAt",

		modelPopulates: {
			"author": "people",
			"voters": "people",
			"fb_page": "page"
		}
	},

	actions: {
		fb :{
			handler(ctx) {
				return this.facebookService.graph(ctx);
			}
		},
		fbConversations : {
			cache: false,
			handler(ctx) {
				return new Promise((resolve, reject) => {					 
					this.facebookService.graph(ctx).then(response=>{
						let fbconversations = response.conversations;
						let pageId = response.id;
						let data = [];
						if(fbconversations) {
							data = fbconversations.data;
							if(data) {
								data.forEach(d=> {										 
									d.fb_page = pageId;											 
									this.updateOrInsert(d.id, d);
								});
										//TODO return conversations offline and optain the fb_id
								resolve(fbconversations);
							}

						} else {
							resolve({data: data});
						}
					}).catch(err=>{
						logger.error(err);
					});
					 
				});
			}
		},
		fbComments : {
			cache: false,
			handler(ctx) {
				return new Promise((resolve, reject) => {					 
					this.facebookService.graphFbModel(ctx).then(response=>{
						let fbconversations = response.posts;
						     console.log("fbComments  ", fbconversations);
						let pageId = ctx.req.pageId;
								
						let data = [];
						if(fbconversations) {

							data = fbconversations.data;
							if(data) {
								data.forEach(d=> {										 
									d.fb_page = pageId;			
									let postId = d.id;
									let comments = d.comments;
									if(comments) comments.data.forEach(c=>{
										this.updateOrInsertComment(c.id, c);
									});
											// const Conversation = mongoose.model('Chat', chat);	
											// const Comment = this.collection.discriminator('Clicked', comment);							 
											
								});
										//TODO return conversations offline and optain the fb_id
								resolve(fbconversations);
							}

						} else {
							resolve({data: data});
						}
					}).catch(err=>{
						logger.error("fbComments", err);
						reject(err);
					});
					 

				});
			}
		},
		find: {
			cache: true,
			handler(ctx) {
				let filter = {};

				if (ctx.params.filter == "my")
					filter.author = ctx.user.id;
				else if (ctx.params.author != null) {
					filter.author = this.personService.decodeID(ctx.params.author);
				}

				let query = collection.find(filter);

				return ctx.queryPageSort(query).exec().then( (docs) => {
					return this.toJSON(docs);
				})
				.then((json) => {
					return this.populateModels(json);
				});
			}
		},

		// return a model by ID
		get: {
			cache: true, // if true, we don't increment the views!
			permission: C.PERM_PUBLIC,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:ConversationNotFound"));

				return collection.findByIdAndUpdate(ctx.modelID, { $inc: { views: 1 } }).exec().then( (doc) => {
					return this.toJSON(doc);
				})
				.then((json) => {
					return this.populateModels(json);
				});
			}
		},

		create: {
			handler(ctx) {
				this.validateParams(ctx, true);

				let conversation = new Chat({
					title: ctx.params.title,
					content: ctx.params.content,
					author: ctx.user.id
				});

				return conversation.save()
				.then((doc) => {
					return this.toJSON(doc);
				})
				.then((json) => {
					return this.populateModels(json);
				})
				.then((json) => {
					this.notifyModelChanges(ctx, "created", json);
					return json;
				});
			}
		},

		update: {
			permission: C.PERM_OWNER,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:ConversationNotFound"));
				this.validateParams(ctx);

				return this.collection.findById(ctx.modelID).exec()
				.then((doc) => {
					if (ctx.params.title != null)
						doc.title = ctx.params.title;

					if (ctx.params.content != null)
						doc.content = ctx.params.content;

					doc.editedAt = Date.now();
					return doc.save();
				})
				.then((doc) => {
					return this.toJSON(doc);
				})
				.then((json) => {
					return this.populateModels(json);
				})
				.then((json) => {
					this.notifyModelChanges(ctx, "updated", json);
					return json;
				});
			}
		},

		remove: {
			permission: C.PERM_OWNER,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:ConversationNotFound"));

				return this.collection.remove({ _id: ctx.modelID })
				.then(() => {
					return ctx.model;
				})
				.then((json) => {
					this.notifyModelChanges(ctx, "removed", json);
					return json;
				});
			}
		},

		vote(ctx) {
			ctx.assertModelIsExist(ctx.t("app:ConversationNotFound"));

			return this.collection.findById(ctx.modelID).exec()
			.then((doc) => {
				// Check user is on voters
				if (doc.voters.indexOf(ctx.user.id) !== -1)
					throw ctx.errorBadRequest(C.ERR_ALREADY_VOTED, ctx.t("app:YouHaveAlreadyVotedThisPost"));
				return doc;
			})
			.then((doc) => {
				// Add user to voters
				return this.collection.findByIdAndUpdate(doc.id, { $addToSet: { voters: ctx.user.id } , $inc: { votes: 1 }}, { "new": true });
			})
			.then((doc) => {
				return this.toJSON(doc);
			})
			.then((json) => {
				return this.populateModels(json);
			})
			.then((json) => {
				this.notifyModelChanges(ctx, "voted", json);
				return json;
			});
		},

		unvote(ctx) {
			ctx.assertModelIsExist(ctx.t("app:ConversationNotFound"));

			return this.collection.findById(ctx.modelID).exec()
			.then((doc) => {
				// Check user is on voters
				if (doc.voters.indexOf(ctx.user.id) == -1)
					throw ctx.errorBadRequest(C.ERR_NOT_VOTED_YET, ctx.t("app:YouHaveNotVotedThisConversationYet"));
				return doc;
			})
			.then((doc) => {
				// Remove user from voters
				return Conversation.findByIdAndUpdate(doc.id, { $pull: { voters: ctx.user.id } , $inc: { votes: -1 }}, { "new": true });
			})
			.then((doc) => {
				return this.toJSON(doc);
			})
			.then((json) => {
				return this.populateModels(json);
			})
			.then((json) => {
				this.notifyModelChanges(ctx, "unvoted", json);
				return json;
			});

		}

	},

	methods: {
		updatePhoneToContact(cfbId, model){
			return new Promise((resolve, reject)=>{
				if(model.messages){
					_.forEach(model.messages.data, cm=>{
						//console.log("update phone number ==============", cm, cm.from, cm.message)
						if(cm.from && cm.message && utils.isPhoneNumber(cm.message)){
							let phone = utils.messageToPhone(cm.message);
							this.contactService.updatePhone(cm.from.id, phone).then(result=>{
								resolve(result);
							}).catch(err=>{
								reject(err);
							});
						}
					});
				}
			});
		},
		updateOrInsert(id, model, cb) {
			return new Promise((resolve, reject)=>{
				let object = _.cloneDeep(model);
				object.fb_id = id;
				let options = { upsert: true, new: true, setDefaultsOnInsert: true,  overwrite: false };
			// if(model.messages){
			// 	_.forEach(model.messages.data, cm=>{
			// 		this.mesasgeService.updateOrInsert(cm.id, cm, id, null);
			// 	})
			// }	
				this.collection.findOneAndUpdate({fb_id: id}, {$set: object} , options).exec().then(docs =>{				
					if(cb)cb(undefined,docs);
					resolve(this.toJSON(docs));
				}).catch(err =>{
					if(cb)cb(err, undefined);
					reject(err);
				});
			});
			
		},
		updateOrInsertMessage(id, model, cb) {
			return new Promise((resolve, reject)=>{
				let object = _.cloneDeep(model);
				object.fb_id = id;
				let options = { upsert: true, new: true, setDefaultsOnInsert: true,  overwrite: false };
			// if(model.messages){
			// 	_.forEach(model.messages.data, cm=>{
			// 		this.mesasgeService.updateOrInsert(cm.id, cm, id, null);
			// 	})
			// }	
				this.message.findOneAndUpdate({fb_id: id}, {$set: object} , options).exec().then(docs =>{				
					if(cb)cb(undefined,docs);
					resolve(this.toJSON(docs));
				}).catch(err =>{
					if(cb)cb(err, undefined);
					reject(err);
				});
			});
			
		},
		updateOrInsertComment(id, model, cb) {
			return new Promise((resolve, reject)=>{
				let object = _.cloneDeep(model);
				delete object.id;
				object.fb_id = id;
				let options = { upsert: true, new: true, setDefaultsOnInsert: true,  overwrite: false };
			// if(model.messages){
			// 	_.forEach(model.messages.data, cm=>{
			// 		this.mesasgeService.updateOrInsert(cm.id, cm, id, null);
			// 	})
			// }	
			//let Comment = chat.dec
		//let cm = new this.coment(object)
				this.coment.findOneAndUpdate({fb_id: id}, {$set: object} , options).exec().then(docs =>{				
					if(cb)cb(undefined,docs);
					resolve(this.toJSON(docs));
				}).catch(err =>{
					if(cb)cb(err, undefined);
					reject(err);
				});
			});
			
		},
		/**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 *
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		validateParams(ctx, strictMode) {
			if (strictMode || ctx.hasParam("title"))
				ctx.validateParam("title").trim().notEmpty(ctx.t("app:ConversationTitleCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("content"))
				ctx.validateParam("content").trim().notEmpty(ctx.t("app:ConversationContentCannotBeEmpty")).end();

			if (ctx.hasValidationErrors())
				throw ctx.errorBadRequest(C.ERR_VALIDATION_ERROR, ctx.validationErrors);
		},
		 

	},

	/**
	 * Check the owner of model
	 *
	 * @param {any} ctx	Context of request
	 * @returns	{Promise}
	 */
	ownerChecker(ctx) {
		return new Promise((resolve, reject) => {
			ctx.assertModelIsExist(ctx.t("app:ConversationNotFound"));

			if (ctx.model.author.code == ctx.user.code || ctx.isAdmin())
				resolve();
			else
				reject();
		});
	},
	
	init(ctx) {
		// Fired when start the service
		this.contactService = ctx.services("contacts");
		this.facebookService = ctx.services("facebook");
		 
		this.mesasgeService = ctx.services("messages");
		// Fired when start the service
		this.personService = ctx.services("people");

		// Add custom error types
		C.append([
			"ALREADY_VOTED",
			"NOT_VOTED_YET"
		], "ERR");
	},

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		}
	},

	graphql: {

		query: `
		conversations(limit: Int, offset: Int, sort: String): [Chat]
		conversation(code: String): Chat
		`,

		types: `
			type Chat {
				code: String!
				title: String
				content: String
				author: Person!
				views: Int
				votes: Int,
				voters(limit: Int, offset: Int, sort: String): [Person]
				createdAt: Timestamp
				createdAt: Timestamp
			}
		`,

		mutation: `
		conversationCreate(title: String!, content: String!): Chat
		conversationUpdate(code: String!, title: String, content: String): Chat
		conversationRemove(code: String!): Conversation

		conversationVote(code: String!): Chat
		conversationUnvote(code: String!): Chat
		`,

		resolvers: {
			Query: {
				conversations: "find",
				conversation: "get"
			},

			Mutation: {
				conversationCreate: "create",
				conversationUpdate: "update",
				conversationRemove: "remove",
				conversationVote: "vote",
				conversationUnvote: "unvote"
			}
		}
	}

};

/*
## GraphiQL test ##

# Find all posts
query getPosts {
  posts(sort: "-createdAt -votes", limit: 3) {
    ...postFields
  }
}

# Create a new post
mutation createPost {
  postCreate(title: "New post", content: "New post content") {
    ...postFields
  }
}

# Get a post
query getPost($code: String!) {
  post(code: $code) {
    ...postFields
  }
}

# Update an existing post
mutation updatePost($code: String!) {
  postUpdate(code: $code, content: "Modified post content") {
    ...postFields
  }
}

# vote the post
mutation votePost($code: String!) {
  postVote(code: $code) {
    ...postFields
  }
}

# unvote the post
mutation unVotePost($code: String!) {
  postUnvote(code: $code) {
    ...postFields
  }
}

# Remove a post
mutation removePost($code: String!) {
  postRemove(code: $code) {
    ...postFields
  }
}



fragment postFields on Post {
    code
    title
    content
    author {
      code
      fullName
      username
      avatar
    }
    views
    votes
  	voters {
  	  code
  	  fullName
  	  username
  	  avatar
  	}
}

*/
