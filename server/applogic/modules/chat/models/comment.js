"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let _ 				= require("lodash");

 
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
 
let chat = require("./chat"); 

let Comment = chat.discriminator("Reply", new Schema({
	fb_id: {
		type: String,
		trim: true,
		unique: true
	},

	title: {
		type: String,
		trim: true
	},
	content: {
		type: String,
		trim: true
	},
	author: {
		type: String,
		//required: "Please fill in an author ID",
		ref: "User"
	},
	views: {
		type: Number,
		default: 0
	},
	voters: [{
		type: String,
		ref: "User"
	}],
	votes: {
		type: Number,
		default: 0
	},
	editedAt: {
		type: Date
	},
	metadata: {}

}));
//let Comment =  mongoose.model("Comment", CommentSchema); //chat.discriminator("Comment", CommentSchema); //

module.exports = Comment;
