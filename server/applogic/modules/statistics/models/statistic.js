"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let db	    		= require("../../../../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../../../../libs/hashids")("contacts");
let autoIncrement 	= require("mongoose-auto-increment");
let mongoosastic    = require("mongoosastic");

let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	},
	strict: false
};

let StatisticSchema = new Schema({
	sent_by_page: {
		type: Boolean,
		required: true
	},

	fb_page_id: {
		type: String,
		required: true
	},

	contact_id: {
		type: Schema.Types.ObjectId,
	},

	user_id: {
		type: Schema.Types.ObjectId
	},

	fb_contact_id: {
		type: String,
		required: true
	},

	conversation_id: {
		type: Schema.Types.ObjectId,
	},

	fb_conversation_id: {
		type: String,
		required: true
	},

	type: {
		type: String,
		enum: ["message", "comment"],
		required: true
	},

	fb_id: {
		type: String,
		required: true
	},

	hour: {
		type: Number,
		min: 0,
		max: 23,
		required: true
	},

	date: {
		type: Number,
		min: 1,
		max: 31,
		required: true
	},
	fb_created_time: {
		type: Date
	},
	month: {
		type: Number,
		min: 1,
		max: 12,
		required: true
	},

	year: {
		type: Number,
		min: 2020,
		max: 3000
	},

	phone_count: {
		type: Number,
		default: 0
	}
}, schemaOptions);

StatisticSchema.index({
	fb_page_id: 1,
	contact_id: 1,
	user_id: 1,
	conversation_id: 1,
	fb_id: 1,
	fb_contact_id: 1,
	fb_conversation_id: 1,
	hour: 1,
	date: 1,
	month: 1,
	year: 1

});

const Statistic = mongoose.model("Statistic", StatisticSchema);

module.exports = Statistic;
