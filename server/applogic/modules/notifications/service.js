"use strict";

let logger = require("../../../core/logger");
let config = require("../../../config");
let C = require("../../../core/constants");
let rabbitConn = require("../../../core/rabbitmq");

let _ = require("lodash");

let Notify = require("./models/notify");
let FB = require("fb");
let context = require("../../../core/context");
const EXCHANGE_CHANEL = "MANTIS_MQ_NOTIFICATION";
const EXCHANGE_QUEUE = "MANTIS_MQ_QUEUE";
const FB_EXCHANGE_QUEUE = "MANTIS_FB_MQ_QUEUE";
module.exports = {
	settings: {
		name: "notifications",
		version: 1,
		namespace: "notifications",
		rest: true,
		ws: true,
		graphql: true,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: Notify,

		modelPropFilter: null,
		// "code title content author votes voters views createdAt editedAt detail",

		modelPopulates: {
			author: "people",
			voters: "people"
		}
	},

	actions: {
		sendNotification: {
			cache : true,
			handler(ctx){
				if(ctx.params.message)
					return new Promise((resolve, reject)=>{
						this.sendQueue(EXCHANGE_CHANEL, EXCHANGE_QUEUE, ctx.params.message).then(result=>{
						// save local notifcaiton
							resolve(result);
						}).catch(err=>{
							reject(err);
						});
					
					});
				else {
					reject(ctx.t("app:NotifyNotFound"));
				}
			}
		},
		fbNotifys: {
			cache: true,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					//logger.info(ctx.req)
					let reqParams = ctx.req.body;

					if (!reqParams) {
						throw ctx.errorBadRequest(
							C.INVALID_FB_PARAM,
							ctx.t("app:IntegrationInvalidateParam")
						);
					}
					//FB.setAccessToken(ctx.user.profile.access_token);
					FB.api(...reqParams, response => {
						if (!response || response.error) {
							this.notifyModelChanges(
								ctx,
								"tokenExpired",
								response.error
							);
							reject(response.error);
						} else {
							//resolve(response);
							console.log(
								"fbNotifys===================",
								response
							);
							//this.notifyModelChanges(ctx, "conversationloaded", response);
							let fbconversations = response.Notifys;
							let pageId = response.id;
							let data = [];
							if (fbconversations) {
								data = fbconversations.data;
								if (data) {
									data.forEach(d => {
										//console.log("conversations ===================", fbconversations.id)
										logger.info(d);
										if (d.comments) {
											let comments = d.comments.data;
											comments.forEach(cm => {
												if (cm.comments) {
													let contacts =
														cm.comments.data;

													contacts.forEach(c => {
														console.log(
															"comment ===================",
															c
														);
														if (c.from) {
															c.fb_pages = [
																pageId
															];
															this.contactServces.updateOrInsert(
																c.from.id,
																c.from,
																function(
																	err,
																	result
																) {
																	if (err)
																		console.log(
																			err
																		);
																}
															);
														}
													});
												}
											});
										}
										if (d.likes) {
											let contacts = d.likes.data;
											contacts.forEach(c => {
												c.fb_pages = [pageId];
												this.contactServces.updateOrInsert(
													c.id,
													c,
													function(err, result) {
														if (err)
															console.log(err);
													}
												);
											});
										}
										d.fb_page = pageId;
										this.updateOrInsert(d.id, d, function(
											err,
											result
										) {
											if (err) {
												console.log(err);
											}
										});
									});
									resolve(fbconversations);
								}
							} else {
								//ctx.assertModelIsExist(ctx.t("app:NotifyNotFound"));
								resolve({ data: data });
							}
						}
					});
				});
			}
		},
		find: {
			cache: true,
			handler(ctx) {
				let filter = {};

				if (ctx.params.filter == "my") filter.author = ctx.user.id;
				else if (ctx.params.author != null) {
					filter.author = this.personService.decodeID(
						ctx.params.author
					);
				}

				let query = Notify.find(filter);

				return ctx
					.queryPageSort(query)
					.exec()
					.then(docs => {
						console.log(docs);
						return this.toJSON(docs);
					})
					.then(json => {
						console.log(json);
						return this.populateModels(json);
					});
			}
		},

		// return a model by ID
		get: {
			cache: true, // if true, we don't increment the views!
			permission: C.PERM_PUBLIC,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:NotifyNotFound"));

				return Notify.findByIdAndUpdate(ctx.modelID, {
					$inc: { views: 1 }
				})
					.exec()
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					});
			}
		},

		create: {
			handler(ctx) {
				this.validateParams(ctx, true);

				let Notify = new Notify(ctx.params);
				Notify.author = ctx.user.id;
				return Notify.save()
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					})
					.then(json => {
						this.notifyModelChanges(ctx, "created", json);
						return json;
					});
			}
		},

		update: {
			permission: C.PERM_OWNER,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:NotifyNotFound"));
				this.validateParams(ctx);

				return this.collection
					.findById(ctx.modelID)
					.exec()
					.then(doc => {
						if (ctx.params.title != null)
							doc.title = ctx.params.title;

						if (ctx.params.content != null)
							doc.content = ctx.params.content;

						doc.editedAt = Date.now();
						return doc.save();
					})
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						return this.populateModels(json);
					})
					.then(json => {
						this.notifyModelChanges(ctx, "updated", json);
						return json;
					});
			}
		},

		remove: {
			permission: C.PERM_OWNER,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:NotifyNotFound"));

				return Notify.remove({ _id: ctx.modelID })
					.then(() => {
						return ctx.model;
					})
					.then(json => {
						this.notifyModelChanges(ctx, "removed", json);
						return json;
					});
			}
		},

		vote(ctx) {
			ctx.assertModelIsExist(ctx.t("app:NotifyNotFound"));

			return this.collection
				.findById(ctx.modelID)
				.exec()
				.then(doc => {
					// Check user is on voters
					if (doc.voters.indexOf(ctx.user.id) !== -1)
						throw ctx.errorBadRequest(
							C.ERR_ALREADY_VOTED,
							ctx.t("app:YouHaveAlreadyVotedThisNotify")
						);
					return doc;
				})
				.then(doc => {
					// Add user to voters
					return Notify.findByIdAndUpdate(
						doc.id,
						{
							$addToSet: { voters: ctx.user.id },
							$inc: { votes: 1 }
						},
						{ new: true }
					);
				})
				.then(doc => {
					return this.toJSON(doc);
				})
				.then(json => {
					return this.populateModels(json);
				})
				.then(json => {
					this.notifyModelChanges(ctx, "voted", json);
					return json;
				});
		},

		unvote(ctx) {
			ctx.assertModelIsExist(ctx.t("app:NotifyNotFound"));

			return this.collection
				.findById(ctx.modelID)
				.exec()
				.then(doc => {
					// Check user is on voters
					if (doc.voters.indexOf(ctx.user.id) == -1)
						throw ctx.errorBadRequest(
							C.ERR_NOT_VOTED_YET,
							ctx.t("app:YouHaveNotVotedThisNotifyYet")
						);
					return doc;
				})
				.then(doc => {
					// Remove user from voters
					return Notify.findByIdAndUpdate(
						doc.id,
						{ $pull: { voters: ctx.user.id }, $inc: { votes: -1 } },
						{ new: true }
					);
				})
				.then(doc => {
					return this.toJSON(doc);
				})
				.then(json => {
					return this.populateModels(json);
				})
				.then(json => {
					this.notifyModelChanges(ctx, "unvoted", json);
					return json;
				});
		}
	},

	methods: {
		broadcastBackToSender(ctx, ev, val){
			this.notifyModelChanges(ctx, "notification", {method: ev, value: val} );
		},
		broadcast(ctx, ev, val){
			ctx.broadcast("notification", {method: ev, value: val});
		},
		broadcastByUser(ctx, ev, val){
			ctx.emitUser("notification", {method: ev, value: val});
		},
		broadcastUserByPage(ctx, pageId, ev, val){
			ctx.emitFbUser("notification", pageId, {method: ev, value: val});
		},
		facebookHook(pageEntry) {
			//console.log("facebookHook ", pageEntry);
			let ctx = context.CreateToServiceInit(this);
			if (pageEntry.changes) {
				let query = {
					type: "notifications",
					template: { $in: _.map(pageEntry.changes, "field") },
					status: 1
				};

				this.settingServces
					.search(query)
					.then(result => {
						console.log(
							"settingServces.search changes ",
							query,
							result
						);
						if (result.length > 0) {
							pageEntry.changes.forEach(change => {
								let notify = new Notify({
									text: "facebook notification",
									type: change.field,
									detail: change.value,
									time: pageEntry.changes.time,
									seenBy: [],
									for: _.map(result, "author")
								});
								notify
									.save()
									.then(doc => {
										return this.toJSON(doc);
									})
									.then(json => {
										return this.populateModels(json);
									})
									.then(json => {
										this.notifyModelChanges(
											ctx,
											"created",
											json
										);
										return json;
									});
							});
						}
					})
					.catch(err => {
						console.log(err);
					});
			} else if (pageEntry.messaging) {
				let query = {
					type: "notifications",
					template: "messaging",
					status: 1
				};
				this.settingServces.search(query).then(result => {
					console.log(
						"settingServces.search messaging",
						query,
						result
					);
					if (result.length > 0) {
						pageEntry.messaging.forEach(messagingEvent => {
							if (messagingEvent.message) {
								let fbMsg = messagingEvent.message;
								let notify = new Notify({
									message: fbMsg,
									sender: messagingEvent.sender,
									recipient: messagingEvent.recipient,
									type: "messaging",
									//detail: messagingEvent,
									time: messagingEvent.timeStemp,
									seenBy: [],
									for: _.map(result, "author")
								})
									.save()
									.then(doc => {
										return this.toJSON(doc);
									})
									.then(json => {
										return this.populateModels(json);
									})
									.then(json => {
										this.notifyModelChanges(
											ctx,
											"created",
											json
										);
										return json;
									});
							}
						});
					}
				});
			} else {
				logger.info(pageEntry);
			}
		},
		updateOrInsert(id, model, cb) {
			let options = {
				upsert: true,
				new: true,
				setDefaultsOnInsert: true,
				overwrite: false
			};
			this.collection
				.findOneAndUpdate(
					{ _id: id },
					{ $set: model },
					options
				)
				.exec()
				.then(result => {
					cb(undefined, result);
				})
				.catch(err => {
					cb(err, undefined);
				});
		},
		/**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 *
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		validateParams(ctx, strictMode) {
			if (strictMode || ctx.hasParam("title"))
				ctx.validateParam("title")
					.trim()
					.notEmpty(ctx.t("app:NotifyTitleCannotBeEmpty"))
					.end();

			if (strictMode || ctx.hasParam("content"))
				ctx.validateParam("content")
					.trim()
					.notEmpty(ctx.t("app:NotifyContentCannotBeEmpty"))
					.end();

			if (ctx.hasValidationErrors())
				throw ctx.errorBadRequest(
					C.ERR_VALIDATION_ERROR,
					ctx.validationErrors
				);
		},
		subscribe(ex, ctx){
			let self = this;
			return new Promise ((resolve, reject)=>{	
				// rabbitConn(function(conn) {
				// 	conn.createChannel(function(err, ch) {
				// 	if (err) {
				// 		console.log("=======================>>> RABBIT MQ ======>> subscribe createChannel ", err);	  
				// 		//throw new Error(err)
				// 		reject(err);
				// 	}
				// 	//var ex = 'chat_ex'
				
				// 	ch.assertExchange(ex, 'fanout', {durable: false})
				// 	ch.assertQueue('', {exclusive: true}, function(err, q) {
				// 		if (err) {
				// 		console.log("=======================>>> RABBIT MQ ======>> subscribe assertQueue ", err);	  	
				// 		//throw new Error(err)
				// 		reject(err);
				// 		}
				// 		ch.bindQueue(q.queue, ex, '')
				// 		ch.consume(q.que, function(msg) {
				// 		self.broadCast(ctx, msg.content.toString());
				// 		//chat.emit('chat', msg.content.toString())
				// 		})
				// 	}, {noAck: true})					
				// 	})
				// 	resolve({status: "success", data: {exchage: ex}});
				// })
			});
			  
		},
		broadCast(ctx, msg){
			console.log("=======================>>> RABBIT MQ ======>> consuming ", msg);
			if(this.context){
				this.context.broadcast("created", msg);
			}
			if(ctx){
				//this.notifyModelChanges(ctx,"created", msg);
				ctx.broadcast("created", msg);
			}
		},
		sendQueue(ex, q, message){
			let msg = JSON.stringify(message);
			return new Promise ((resolve, reject)=>{
				// rabbitConn(function(conn) {
				// 	conn.createChannel(function(err, ch) {
				// 		if (err) {
				// 			//throw new Error(err)
				// 			reject(err);
				// 		}
				// 		//var ex = 'chat_ex'
				// 		//var q = 'chat_q'
				// 		ch.assertExchange(ex, 'fanout', {durable: false})
				// 		ch.publish(ex, '', new Buffer(msg), {persistent: false})
				// 		ch.assertQueue(q, {durable: true})
				// 		ch.sendToQueue(q, new Buffer(msg), {persistent: true})
				// 		ch.close(function() {conn.close()});					
				// 	})
				// 	resolve({status:"success", data: {echange: ex, queue: q,  msg: msg}});
				// })
			});  
		},
		sendDefaultQueue(msg){
			return this.sendQueue(EXCHANGE_CHANEL, EXCHANGE_QUEUE, msg);
		},
		getQueue(q, req, res){
			// rabbitConn(function(conn){
			// 	conn.createChannel(function(err, ch) {
			// 	  if (err) {
			// 		throw new Error(err)
			// 	  }
		  
			// 	  //var q = 'chat_q'
		  
			// 	  ch.assertQueue(q, {durable: true}, function(err, status) {
			// 		if (err) {
			// 		  throw new Error(err)
			// 		}
			// 		else if (status.messageCount === 0) {
			// 		  res.send('{"messages": 0}')
			// 		} else {
			// 		  var numChunks = 0;
		  
			// 		  res.writeHead(200, {"Content-Type": "application/json"})
			// 		  res.write('{"messages": [')
		  
			// 		  ch.consume(q.que, function(msg) {
			// 			var resChunk = msg.content.toString()
		  
			// 			res.write(resChunk)
			// 			numChunks += 1
			// 			numChunks < status.messageCount && res.write(',')
		  
			// 			if (numChunks === status.messageCount) {
			// 			  res.write(']}')
			// 			  res.end()
			// 			  ch.close(function() {conn.close()})
			// 			}
			// 		  })
			// 		}
			// 	  })
			// 	}, {noAck: true})
			//   })
		}
	},

	/**
	 * Check the owner of model
	 *
	 * @param {any} ctx	Context of request
	 * @returns	{Promise}
	 */
	ownerChecker(ctx) {
		return new Promise((resolve, reject) => {
			ctx.assertModelIsExist(ctx.t("app:NotifyNotFound"));

			if (ctx.model.author.code == ctx.user.code || ctx.isAdmin())
				resolve();
			else reject();
		});
	},

	init(ctx) {
		// Fired when start the service
		this.personService = ctx.services("people");
		this.contactServces = ctx.services("contacts");
		this.settingServces = ctx.services("settings");
		// Add custom error types	
		C.append(["ALREADY_VOTED", "NOT_VOTED_YET"], "ERR");
		this.subscribe(EXCHANGE_CHANEL, ctx);
	},

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
			
		}
	},

	graphql: {
		query: `
			Notifys(limit: Int, offset: Int, sort: String): [Notify]
			Notify(code: String): Notify
		`,

		types: `
			type Notify {
				code: String!
				title: String
				content: String
				author: Person!
				views: Int
				votes: Int,
				voters(limit: Int, offset: Int, sort: String): [Person]
				createdAt: Timestamp
				createdAt: Timestamp
			}
		`,

		mutation: `
			NotifyCreate(title: String!, content: String!): Notify
			NotifyUpdate(code: String!, title: String, content: String): Notify
			NotifyRemove(code: String!): Notify

			NotifyVote(code: String!): Notify
			NotifyUnvote(code: String!): Notify
		`,

		resolvers: {
			Query: {
				Notifys: "find",
				Notify: "get"
			},

			Mutation: {
				NotifyCreate: "create",
				NotifyUpdate: "update",
				NotifyRemove: "remove",
				NotifyVote: "vote",
				NotifyUnvote: "unvote"
			}
		}
	}
};

/*
## GraphiQL test ##

# Find all Notifys
query getNotifys {
  Notifys(sort: "-createdAt -votes", limit: 3) {
    ...NotifyFields
  }
}

# Create a new Notify
mutation createNotify {
  NotifyCreate(title: "New Notify", content: "New Notify content") {
    ...NotifyFields
  }
}

# Get a Notify
query getNotify($code: String!) {
  Notify(code: $code) {
    ...NotifyFields
  }
}

# Update an existing Notify
mutation updateNotify($code: String!) {
  NotifyUpdate(code: $code, content: "Modified Notify content") {
    ...NotifyFields
  }
}

# vote the Notify
mutation voteNotify($code: String!) {
  NotifyVote(code: $code) {
    ...NotifyFields
  }
}

# unvote the Notify
mutation unVoteNotify($code: String!) {
  NotifyUnvote(code: $code) {
    ...NotifyFields
  }
}

# Remove a Notify
mutation removeNotify($code: String!) {
  NotifyRemove(code: $code) {
    ...NotifyFields
  }
}



fragment NotifyFields on Notify {
    code
    title
    content
    author {
      code
      fullName
      username
      avatar
    }
    views
    votes
  	voters {
  	  code
  	  fullName
  	  username
  	  avatar
  	}
}

*/
