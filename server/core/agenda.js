"use strict";

let logger = require("./logger");
let config = require("../config");
let mongoose  = require("mongoose");
let moment = require("moment");
let chalk = require("chalk");
let Agenda = require("agenda");
let User = require("../models/user");

let url = process.env.MONGO_URI || "mongodb://dbadmin:123456789@127.0.0.1:27017/mantis_prod?authSource=mantis_prod";
const agenda = new Agenda({
	db: {
		address: url,
		collection: "agendaJobs"
		//options: { user: "dbadmin", pass: "123456789" }
	},
	processEvery: config.agendaTimer || "one minute"
});

agenda.on("fail", function(err, job) {
	return logger.error("Job failed with error: " + err.message);
});

/**
 * Remove unverified account after 24 hours
 */
agenda.define("removeUnverifiedAccounts", function(job, done) {
	logger.debug("Running 'removeUnverifiedAccounts' process...");
	try {
		User.remove(
			{
				createdAt: {
					$lte: moment()
						.subtract(1, "day")
						.toDate()
				},
				verified: false
			},
			(err, count) => {
				if (count > 0)
					logger.warn(
						chalk.bold.red(
							count + " unverified and expired account removed!"
						)
					);

				done();
			}
		);
	} catch (error) {
		logger.error("Job running exception!");
		logger.error(error);
		return done(error);
	}
});




/**
 * Starting agenda
 */
agenda.on("ready", function() {
	if (config.isTestMode()) return;

	// agenda.every("8 hours", "removeUnverifiedAccounts");
	agenda.start();
	logger.info(chalk.yellow("Agenda started!"));
});
agenda.on("start", job => {
	console.log("Job %s starting", job.attrs.name);
});
agenda.on("complete", job => {
	console.log(`Job ${job.attrs.name} finished`);
});
module.exports = agenda;
