"use strict";
let services = require("./services");
let context = require("./context");
let _ = require("lodash");
let Bull = require('bull');
const webhookQueue = new Bull('webhook-queue');
let messageService = services.get("messages");
let ctx = context.CreateToServiceInit(messageService);
let notificationService = services.get("notifications");

let hookServices = {
  conversationsService:services.get("conversations"),
  feedService:services.get("feeds"),
  messageService:services.get("messages"),
};


webhookQueue.process( (job) => {
    // job.res.status(200).send("EVENT_RECEIVED");
    if (job.data.object === "page") {
      job.data.entry.forEach(function(pageEntry) {
        notificationService.facebookHook(pageEntry);
        if (pageEntry.changes) {
          pageEntry.changes.forEach(change => {
            if (change.field) {
               hookServices[change.field+"Service"].handleWebhook(change, pageEntry.id, context);
            }
          });
        }
        else if (pageEntry.messaging) {
          pageEntry.messaging.forEach(messagingEvent => {
            if((messagingEvent.message && (messagingEvent.message.text || messagingEvent.message.attachments)) || messagingEvent.read || messagingEvent.postback || (messagingEvent.referral && messagingEvent.referral.source == "ADS")){
                 hookServices.conversationsService.handleWebhook(messagingEvent, pageEntry.id, context);
                 hookServices.messageService.handleWebhook(messagingEvent, pageEntry.id, context);
            }
          });
        }
      });

    } else {
      // 	res.sendStatus(404);
    }
});
module.exports = webhookQueue;
