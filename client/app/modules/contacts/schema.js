import Vue from "vue";
import moment from "moment";
import { contactTypes } from "./types";
import { validators } from "vue-form-generator";

import { find } from "lodash";

let _ = Vue.prototype._;

module.exports = {

	id: "contacts",
	title: _("Contacts"),

	table: {
		multiSelect: true,
		columns: [
			{
				title: _("ID"),
				field: "code",
				align: "left",
				formatter(value, model) {
					return model ? model.code : "";
				}
			},
			{
				title: _("Type"),
				field: "type",
				formatter(value) {
					let type = find(contactTypes, (type) => type.id == value);
					return type ? type.name : value;
				}
			},
			
			{
				title: _("Name"),
				field: "name"
			},
			{
				title: _("Phones"),
				field: "phones"
			},
			{
				title: _("Email"),
				field: "emails"
			},
			{
				title: _("Address"),
				field: "address"
			},
			{
				title: _("ShipAddress"),
				field: "ship_address"
			},
			{
				title: _("Status"),
				field: "status",
				formatter(value, model, col) {
					return value ? "<i class='fa fa-check'/>" : "<i class='fa fa-ban'/>";
				},
				align: "center"
			},
			{
				title: _("LastCommunication"),
				field: "lastCommunication",
				formatter(value) {
					return moment(value).fromNow();
				}
			}
		],

		rowClasses: function(model) {
			return {
				inactive: !model.status
			};
		}

	},

	form: {
		fields: [
			{
				type: "text",
				label: _("ID"),
				model: "code",
				readonly: true,
				disabled: true,
				multi: false,
				get(model) {
					if (model.code)
						return model.code;
					else
						return _("willBeGenerated");
				}
			},
			{
				type: "select",
				label: _("Type"),
				model: "type",
				required: true,
				values: contactTypes,
				default: "new",
				validator: validators.required

			},	
			{
				type: "text",
				label: _("Name"),
				model: "name",
				featured: true,
				required: true,
				placeholder: _("ContactName"),
				validator: validators.string
			},
			{
				type: "input",
				inputType: "number",
				label: _("Age"),
				model: "age",
				//featured: true,
				//required: true,
				min:1,
				max:999,
				placeholder: _("Age"),
				//validator: validators.number
			},
			{
				type: "input",
				inputType: "file",
				label: _("Avatar"),
				model: "avatar",				 
				//featured: true,
				//required: true,
				// min:1,
				// max:999,
				placeholder: _("Avatar"),
				//validator: validators.number
				buttons: [
					{
						classes: "btn-location",
						label: "Current location",
						onclick: function(model) {
							if (navigator.geolocation) {
								navigator.geolocation.getCurrentPosition(function(pos) {
								  model.address.geo = {
									lat: pos.coords.latitude.toFixed(5),
									lng: pos.coords.longitude.toFixed(5)
								  };
								});
							} else {
								alert("Geolocation is not supported by this browser.");
							}
						}
					}
				]
			},
			// {
			// 	type: "dateTimePicker",
			// 	label:  _("BirthDay"),
			// 	model: "dob",
			// 	//required: true,
			// 	placeholder: _("BirthDay"),
			// 	min: moment("1900-01-01").toDate(),
			// 	//max: moment("2016-01-01").toDate(),
			// 	validator: validators.date,			
			// 	dateTimePickerOptions: {
			// 		format: "DD-MM-YYYY"
			// 	},            
			
			// 	onChanged: function(model, newVal, oldVal, field) {
			// 		model.age = moment().year() - moment(newVal).year();
			// 	}
			// },
			// {
			// 	type: "checklist",
			// 	label: _("Assistants"),
			// 	model: "assistants",
			// 	//listBox: true,
			// 	checklistOptions: {
			// 		value: "id" 
			// 	},
			// 	values: function() {
			// 		//console.log(data)
			// 		//if(this.data && this.data.assistants) return this.data.assistants;
			// 		return [{id: "1231212", name: "pham tuan"}]
			// 	}
			// },
			{
				type: "text",
				label: _("Emails"),
				model: "emails",
				featured: true,
				multi: true,
				//required: true,
				placeholder: _("Emails"),
				validator: validators.string
			},
			{
				type: "text",
				label: _("Phones"),
				model: "phones",
				featured: true,
				multi: true,
				//required: true,
				placeholder: _("Phones"),
				validator: validators.string
			},
			{
				type: "textArea",
				label: _("Address"),
				model: "address",
				placeholder: _("AddressOfContact"),
				validator: validators.string
			},
			{
				type: "textArea",
				label: _("ShipAddress"),
				model: "ship_address",
				placeholder: _("ShipAddress"),
				validator: validators.string
			},
			{
				type: "textArea",
				label: _("Description"),
				model: "description",
				featured: false,
				required: false,
				validator: validators.string
			},	
			
			{
				type: "label",
				label: _("LastCommunication"),
				model: "lastCommunication",
				get(model) { return model && model.lastCommunication ? moment(model.lastCommunication).fromNow() : "-"; }
			},
			{
				type: "switch",
				label: _("Status"),
				model: "status",
				multi: true,
				default: 1,
				textOn: _("Active"),
				textOff: _("Inactive"),
				valueOn: 1,
				valueOff: 0
			}
		]
	},

	options: {
		searchable: true,


		enableNewButton: true,
		enabledSaveButton: true,
		enableDeleteButton: true,
		enableCloneButton: false,

		validateAfterLoad: false, // Validate after load a model
		validateAfterChanged: false, // Validate after every changes on the model
		validateBeforeSave: true // Validate before save a model
	},	 
	events: {
		onSelect: null,
		onNewItem: null,
		onCloneItem: null,
		onSaveItem: null,
		onDeleteItem: null,
		onChangeItem: null,
		onValidated(model, errors, schema) {
			if (errors.length > 0)
				console.warn("Validation error in page! Errors:", errors, ", Model:", model);
		}
	},	
	resources: {
		addCaption: _("AddNewContact"),
		saveCaption: _("Save"),
		cloneCaption: _("Clone"),
		deleteCaption: _("Delete")
	}

};