import Vue from "vue";
import toastr from "../../../core/toastr";
import { LOAD, ADD, SELECT, CLEAR_SELECT, UPDATE, REMOVE, CHANGE_SORT , CHANGE_VIEWMODE,  FETCHING, NO_MORE_ITEMS, CLEAR} from "./types";
import axios from "axios";
import Service from "../../../core/service";
import {LOAD_MORE, POST_LOAD} from "../../posts/store/types";
export const NAMESPACE = "/api/contacts";
//let service = window.contactService;
//let userService = window.peopleService

export const selectRow = ({ commit }, row, multiSelect) => {
	commit(SELECT, row, multiSelect);
};

export const elsearch = function  ({ commit }, condition) {
	 return window.contactService.rest("elsearch", condition).then(contact=>{
		 console.log(contact);
	 });
};
export const getAssistants = function (){
	return window.peopleService
      .rest("findByPage", {
	fields: { id: true, username: true, fullName: true },
	filter: {}
});
};
export const getEmployee = function ({commit, state}, filter){
	return window.peopleService.rest("find", filter);
};
export const getAvatar = function ({commit, state}, filter){
	return window.contactService.rest("getAvatar", filter);
};
export const clearSelection = ({ commit }) => {
	commit(CLEAR_SELECT);
};
export const clear = ({ commit }) => {
	commit(CLEAR);
};
export const find = function ({commit, state}, filter) {
	return window.contactService.rest("find", filter);
};
export const findAndUpdate = function ({commit, state}, params) {
	return window.contactService.rest("findAndUpdate", params);
};
export const query = function ({commit, state}, loadMore) {
	//let loadMore  = condition.loadMore;
	commit(FETCHING, true);
	//.query(gql`query($code: String!) {post(code: $code) { code title } }`, { code: "Jk8Pqb5MAN" })
	window.contactService.query(gql`{contacts(limit: 5){name}}`).then(result =>{
		console.log("contacts",result.contacts);
	}).catch(err=>{	console.log(err);});
};
export const searchRows = function ({commit, state}, loadMore) {
	//let loadMore  = condition.loadMore;
	commit(FETCHING, true);
	window.contactService.rest("search", { filter: state.viewMode, sort: state.sort, limit: 100, offset: state.offset  }).then(data=>{

		if (data.length == 0)
			commit(NO_MORE_ITEMS);
		else
			commit(loadMore ? LOAD_MORE : LOAD, data);
	}).catch(err=>{
		toastr.error(err.message);
	}).then(()=>{
		commit(FETCHING, false);
	});

};

export const getRows = function ({commit, state}, loadMore) {
	commit(FETCHING, true);
	return window.contactService.rest("find", { filter: state.viewMode, sort: state.sort, limit: 100, offset: state.offset }).then((data) => {
		if (data.length == 0)
			commit(NO_MORE_ITEMS);
		else
			commit(loadMore ? LOAD_MORE : LOAD, data);

	}).catch((err) => {
		toastr.error(err.message);
	}).then(() => {
		commit(FETCHING, false);
	});
};

export const downloadRows = ({ commit }) => {
	axios.get(NAMESPACE).then((response) => {
		console.log(response);
		let res = response.data;
		if (res.status == 200 && res.data)
			commit(LOAD, res.data);
		else
			console.error("Request error!", res.error);

	}).catch((response) => {
		console.error("Request error!", response.statusText);
	});

};
export const loadMoreRows = function(context) {
	return getRows(context, true);
};

export const changeSort = function(store, sort) {
	store.commit(CHANGE_SORT, sort);
	searchRows(store);
};

export const changeViewMode = function(store, viewMode) {
	store.commit(CHANGE_VIEWMODE, viewMode);
	searchRows(store);
};
export const saveRow = ({ commit }, model) => {
	axios.post(NAMESPACE, model).then((response) => {
		let res = response.data;

		if (res.status == 200 && res.data)
			created({ commit }, res.data, true);
	}).catch((response) => {
		if (response.data.error)
			toastr.error(response.data.error.message);
	});
};

export const created = ({ commit }, row, needSelect) => {
	commit(ADD, row);
	if (needSelect)
		commit(SELECT, row, false);
};

export const updateRow = ({ commit }, row) => {
	axios.put(NAMESPACE + "/" + row.code, row).then((response) => {
		let res = response.data;
		if (res.data)
			commit(UPDATE, res.data);
	}).catch((response) => {
		if (response.data.error)
			toastr.error(response.data.error.message);
	});
};

export const updateRowBy = ({ commit }, row) => {
	return new Promise((resolve, reject)=>{
		return window.contactService.rest("updateBy", row).then((data) => {
			resolve(data);
		}).catch((err) => {
			toastr.error(err.msg);
			reject(err.msg);
		});
	});
};
export const findRowBy = ({ commit }, row) => {
	return new Promise((resolve, reject)=>{
		return window.contactService.rest("findBy", row).then((data) => {
			resolve(data);
		}).catch((err) => {
			toastr.error(err.msg);
			reject(err.msg);
		});
	});
};

export const updatePhones = ({ commit }, row) => {
	return new Promise((resolve, reject)=>{
		return window.contactService.rest("updatePhones", row).then((data) => {
			resolve(data);
		}).catch((err) => {
			toastr.error(err.msg);
			reject(err.msg);
		});
	});
};

export const updated = ({ commit }, row) => {
	commit(UPDATE, row);
};

export const removeRow = ({ commit }, row) => {
	axios.delete(NAMESPACE + "/" + row.code).then((response) => {
		commit(REMOVE, row);
	}).catch((response) => {
		if (response.data.error)
			toastr.error(response.data.error.message);
	});
};

export const removed = ({ commit }, row) => {
	commit(REMOVE, row);
};
