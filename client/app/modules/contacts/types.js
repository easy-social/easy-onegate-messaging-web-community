module.exports = {
	
	contactTypes: [
		{ id: "hot", name: "Hot" },
		{ id: "new", name: "New" },
		{ id: "cold", name: "Cold" },
		{ id: "store", name: "InStore" },
		{ id: "old", name: "Old" }		
	]
};