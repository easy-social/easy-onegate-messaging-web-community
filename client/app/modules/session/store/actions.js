import Vue from "vue";
import axios from "axios";
import { ADD_MESSAGE, ADD_NOTIFICATION, SET_USER, SEARCH, CLEAR_NOTIFICATION , CLEAR_MESSAGE, SET_SESSION_CRM, CLEAR_SESSION_CRM} from "./types";
import Service from "../../../core/service";
import X2JS from "x2js";
import _ from "lodash";

export const NAMESPACE= "/api/session";
//let service = new Service("session", this);
export const getSessionUser = ({ commit }) => {
	axios.get(NAMESPACE + "/me").then((response) => {
		let res = response.data;
		if (res.status == 200)
			commit(SET_USER, res.data);
		else
			console.error("Request error!", res.error);

	}).catch((response) => {
		console.error("Request error!", response.statusText);
	});
};
export const getOnlineUsers = function ({ commit }) {
	return window.sessionService.rest("onlineUsers");
};

export const addMessage = ({ commit }, item) => {
	commit(ADD_MESSAGE, item);
};

export const addNotification = ({ commit }, item) => {
	commit(ADD_NOTIFICATION, item);
};
export const sendNotification = ({ commit }, params) => {
	return window.notificationService.rest("sendNotification", params);
};

export const searching = ({ commit }, text) => {
	commit(SEARCH, text);
};
export const markAllAsRead = ({commit}, isReadAll) => {
	commit(CLEAR_NOTIFICATION, isReadAll);
};
export const markAllMessageAsRead = ({commit}, isReadAll) => {
	commit(CLEAR_MESSAGE, isReadAll);
};

export const crmPost = ({commit}, params) => {
	return new Promise((resolve, reject)=>{
		let sessionId = localStorage.getItem("CRM_SESSION");
		let server = localStorage.getItem("CRM_URL");
		if(server && sessionId){
			let request = params;
			request.SessionId = sessionId;
			window.sessionService.rest("crmcall", { request: request, server : server}).then(response=>{
				resolve(response);
			}).catch(err=>{
				reject(err);
			});
		} else {
			commit(CLEAR_SESSION_CRM);
			reject({err: "emptySession"});
		}
	});
};

export const crmPlaceOrder = ({commit}, params) => {

};
export const getCRMSetting= ({commit}, params) => {

};
export const crmLinkAccount = ({commit}, params) => {

};
export const getCrmCustomer = ({commit}, params) => {

};
export const crmLogout = ({commit}, params) => {
	let req = {

		RequestClass: "BPM",
		RequestAction: "SignIn"

	};
	return new Promise((resolve, reject)=>{
		window.sessionService.rest("crmcall", { request: req, server : undefined}).then(response=>{
			console.log(response);
			commit(CLEAR_SESSION_CRM);
			resolve(response);
		}).catch(err=>{
			commit(CLEAR_SESSION_CRM);
			reject(err);
		});
	});

};
export const crmLogin = ({commit}, params) => {
	//console.log(params)
	let req = {
		//	TotalRequests: 1,
		HostURL:"",
		DeviceType:"",
		RequestClass: "BPM",
		RequestAction: "SignIn",
		Account: params.userName,
		Password: params.userPass
	};
	return new Promise((resolve, reject)=>{
		if(params.SessionId){
			getCRMUser({commit}, params).then(data=>{
				commit(SET_SESSION_CRM, data.Account);
				localStorage.setItem("CRM_SESSION", data.Account.SessionId);
				localStorage.setItem("CRM_ACCOUNT", JSON.stringify(data.Account));
				resolve(data);
			}).catch(err=>{
				commit(CLEAR_SESSION_CRM);
				localStorage.removeItem("CRM_SESSION");
    			localStorage.removeItem("CRM_ACCOUNT");
				reject(err);
			});
		} else {
			window.sessionService.rest("crmcall", { request: req, server : params.server}).then(response=>{
				//console.log(response)
				params.SessionId = response.Data;
				getCRMUser({commit}, params).then(data=>{
					commit(SET_SESSION_CRM, data.Account);
					localStorage.setItem("CRM_URL", params.server);
					localStorage.setItem("CRM_SESSION", data.Account.SessionId);
					localStorage.setItem("CRM_ACCOUNT", JSON.stringify(data.Account));
					resolve(data);
				}).catch(err=>{
					commit(CLEAR_SESSION_CRM);
    				localStorage.removeItem("CRM_URL");
					localStorage.removeItem("CRM_SESSION");
    				localStorage.removeItem("CRM_ACCOUNT");
					reject(err);
				});
			}).catch(err=>{
				commit(CLEAR_SESSION_CRM);
				localStorage.removeItem("CRM_URL");
				localStorage.removeItem("CRM_SESSION");
    			localStorage.removeItem("CRM_ACCOUNT");
				reject(err);
			});
		}

	});

};
export const getCRMUser = ({commit}, params) => {
	return new Promise((resolve, reject)=>{
		let valid = {
			RequestAction: "GetUserInformation",
			RequestClass: "BPM",
			SessionId: params.SessionId
		};
		window.sessionService.rest("crmcall", { request: valid, server : params.server}).then(response=>{

			let data = response;
			if(response.UserDS && response.UserDS.User) {
				let user = data.UserDS.User;
				let x2js = new X2JS();
				let json = x2js.xml2js(user.Data);
				delete user.Data;
				user = _.assign(user, json.Document.Data.Dynamic);
				user = _.assign(user, json.Document.Data.Static);
				let session = {
					success: true,
					Account: {User: user, Groups: data.UserDS.Group, Roles: {}, SessionId: valid.SessionId},
					Message: data.msg
				};
				resolve(session);
			} else {
				reject(data);
			}
		}).catch(err=>{
			reject(err);
		});
	});
};

