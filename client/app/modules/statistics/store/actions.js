import Vue from "vue";
import _ from "lodash";
import toastr from "../../../core/toastr";
import Service from "../../../core/service";
import utils from "../../../core/utils";
import prod from "../../../../../server/config/prod";

const gmt = "Asia/Ho_Chi_Minh";

export const NAMESPACE = "/api/statistics";

export function reset({ state, commit }, data) {
	if (
		data.pageIds && !_.isEqual(data.pageIds, state.pageIds)
	) {
		Object.keys(state).forEach(k => {
			commit("SET_DATA", {
				key: k,
				value: null
			});
		});

		commit("SET_DATA", {
			key: "pageIds",
			value: data.pageIds
		});

		return true;
	}

	return false;
}

export function getOverviewData({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.statisticsService
			.rest("getOverviewData", {
				pageIds: data.pageIds,
				gmt,
			})
			.then(data => {
				commit("SET_DATA", {
					key: "overview",
					value: data
				});
				resolve();
			})
			.catch(err => {
				toastr.error(err.message || err);
				reject(err);
			});
	});
}

export function getRecentlyMessageAndContactCount({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.statisticsService
			.rest("getContactAndMessageRecently", {
				pageIds: data.pageIds,
				gmt,
			})
			.then(data => {
				commit("SET_DATA", {
					key: "recentlyMessageAndContactCount",
					value: data
				});
				resolve();
			})
			.catch(err => {
				toastr.error(err.message || err);
				reject(err);
			});
	});
}

export function getMessageAndCommentByDate({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.statisticsService
			.rest("getMessageAndCommentByDate", {
				pageIds: data.pageIds,
				gmt
			})
			.then(data => {
				commit("SET_DATA", {
					key: "messageAndCommentTodayCount",
					value: data
				});
				resolve();
			}).catch(err => {
				toastr.error(err.message || err);
				reject(err);
			});
	});
}

export function getTopUserInteraction({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.statisticsService
			.rest("getTopUserInteraction", {
				pageIds: data.pageIds,
				gmt
			})
			.then(data => {
				commit("SET_DATA", {
					key: "topUserInteraction",
					value: data
				});
				resolve();
			})
			.catch(err => {
				toastr.error(err.message || err);
				reject(err);
			});
	});
}

export function getCustomerInteractionRecently({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.statisticsService
			.rest("getCustomerInteractionRecently", {
				pageIds: data.pageIds,
				gmt
			})
			.then(data => {
				commit("SET_DATA", {
					key:"customerInteractionRecently",
					value: data
				});
				resolve();
			})
			.catch(err => {
				toastr.error(err.message || err);
				reject(err);
			});
	});
}

export function getNewStatistics({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.statisticsService
			.rest("getNewStatistics", {
				pageIds: data.pageIds,
				gmt
			})
			.then(data => {
				commit("SET_DATA", {
					key: "newStatistics",
					value: data
				});
				resolve();
			})
			.catch(err => {
				toastr.error(err.message || err);
				reject(err);
			});
	});
}

export function getPageStatisticsByDate({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.statisticsService
			.rest("getPageStatisticsByDate", {
				pageIds: data.pageIds,
				gmt,
				from: data.from,
				to: data.to
			})
			.then(data => {
				commit("SET_DATA", {
					key: "pageStatistics",
					value: data
				});
				resolve();
			})
			.catch(err => {
				toastr.error(err.message || err);
				reject(err);
			});
	});
}

export function getStaffs({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.statisticsService
			.rest("getStaffs", {
				pageIds: data.pageIds
			})
			.then(data => {
				commit("SET_DATA", {
					key: "staffs",
					value: data
				});
				resolve();
			})
			.catch(err => {
				toastr.error(err.message || err);
				reject(err);
			});
	});
}

export function getStaffStatistics({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.statisticsService
			.rest("getStaffStatistics", {
				pageIds: data.pageIds,
				gmt,
				from: data.from,
				to: data.to,
				staffIds: data.staffIds
			})
			.then(data => {
				commit("SET_DATA", {
					key: "staffStatistics",
					value: data
				});
				resolve();
			})
			.catch(err => {
				toastr.error(err.message || err);
				reject(err);
			});
	});
}

export function getInteractionStatistics({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.statisticsService
			.rest("getInteractionStatistics", {
				pageIds: data.pageIds,
				gmt,
				from: data.from,
				to: data.to,
				staffIds: data.staffIds
			})
			.then(data => {
				commit("SET_DATA", {
					key: "interactionStatistics",
					value: data
				});
				resolve();
			})
			.catch(err => {
				toastr.error(err.message || err);
				reject(err);
			});
	});
}

export function getTotalInteractionStatistics({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.statisticsService
			.rest("getTotalInteractionStatistics", {
				pageIds: data.pageIds,
				gmt,
				from: data.from,
				to: data.to,
				staffIds: data.staffIds
			})
			.then(data => {
				commit("SET_DATA", {
					key: "totalInteractionStatistics",
					value: data
				});
				resolve();
			})
			.catch(err => {
				toastr.error(err.message || err);
				reject(err);
			});
	});
}

export function getTags({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.statisticsService
			.rest("getTags", {
				pageIds: data.pageIds
			})
			.then(data => {
				commit("SET_DATA", {
					key: "tags",
					value: data
				});
				resolve();
			})
			.catch(err => {
				toastr.error(err.message || err);
				reject(err);
			});
	});
}

export function getTagStatistics({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.statisticsService
			.rest("getTagStatistics", {
				pageIds: data.pageIds,
				gmt,
				from: data.from,
				to: data.to,
				tagCodes: data.tagCodes,
				unit: data.unit
			})
			.then(data => {
				commit("SET_DATA", {
					key: "tagStatistics",
					value: data
				});
				resolve();
			})
			.catch(err => {
				toastr.error(err.message || err);
				reject(err);
			});
	});
}

export function getTodayTagStatistics({ commit }, data) {
	return new Promise((resolve, reject) => {
		window.statisticsService
			.rest("getTagStatistics", {
				pageIds: data.pageIds,
				gmt,
				from: data.from,
				to: data.to,
				tagCodes: data.tagCodes,
				unit: data.unit
			})
			.then(data => {
				commit("SET_DATA", {
					key: "todayTagStatistics",
					value: data
				});
				resolve();
			})
			.catch(err => {
				toastr.error(err.message || err);
				reject(err);
			});
	});
}
