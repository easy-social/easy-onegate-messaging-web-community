export const LOAD 			= "LOAD";
export const LOAD_MORE		= "LOAD_MORE";
export const ADD 			= "ADD";
export const UPDATE			= "UPDATE";
export const REMOVE			= "REMOVE";

export const FETCHING		= "FETCHING";
export const NO_MORE_ITEMS	= "NO_MORE_ITEMS";
export const CLEAR 			= "CLEAR";

export const CHANGE_SORT	 = "CHANGE_SORT";
export const CHANGE_VIEWMODE = "CHANGE_VIEWMODE";

export const VOTE			= "VOTE";
export const UNVOTE			= "UNVOTE";

export const SEND			= "SEND";

export const CONTACT_SELECT 			= "CONTACT_SELECT";
export const CONTACT_LOAD 			= "CONTACT_LOAD";
export const CONTACT_LOAD_MORE		= "CONTACT_LOAD_MORE";
export const CONTACT_ADD 			= "CONTACT_ADD";
export const CONTACT_UPDATE			= "CONTACT_UPDATE";
export const CONTACT_REMOVE			= "CONTACT_REMOVE";

export const CONTACT_FETCHING		= "CONTACT_FETCHING";
export const CONTACT_NO_MORE_ITEMS	= "CONTACT_NO_MORE_ITEMS";
export const CONTACT_CLEAR 			= "CONTACT_CLEAR";

export const CONTACT_CHANGE_SORT	 = "CONTACT_CHANGE_SORT";
export const CONTACT_CHANGE_VIEWMODE = "CONTACT_CHANGE_VIEWMODE";

export const LOAD_PAGE_TOKEN = "LOAD_PAGE_TOKEN";

export const CLEAR_MESSAGE = "CLEAR_MESSAGE";
