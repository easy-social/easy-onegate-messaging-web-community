import toastr from "../../../core/toastr";
import Service from "../../../core/service";
import {
	CLEAR,
	LOAD,
	LOAD_MORE,
	ADD,
	UPDATE,
	VOTE,
	UNVOTE,
	REMOVE,
	NO_MORE_ITEMS,
	FETCHING,
	CHANGE_SORT,
	CHANGE_VIEWMODE,
	SEND,
	CONTACT_LOAD,
	CONTACT_LOAD_MORE,
	CONTACT_ADD,
	CONTACT_SELECT,
	CONTACT_CLEAR_SELECT,
	CONTACT_UPDATE,
	CONTACT_REMOVE,
	CONTACT_CLEAR,
	CONTACT_NO_MORE_ITEMS,
	CONTACT_FETCHING,
	CONTACT_CHANGE_SORT,
	CONTACT_CHANGE_VIEWMODE,
	LOAD_PAGE_TOKEN,
	CLEAR_MESSAGE
} from "./types";


export const NAMESPACE = "/api/messages";

export const clear = function({ commit, state }) {
	commit(CLEAR);
};
export const clearchat = function({ commit, state }) {
	commit(CLEAR_MESSAGE);
};
export const download = function({ commit, state }, data) {
	 return window.messageService
	 .rest("download", data);
};
export const getFiles = function({commit, state}, params) {
	return window.messageService.rest("loadAttachFiles", params);
};
export const deleteAttachment = function({commit, state}, params) {
	return window.messageService.rest("removeFile", params);
};
export const search = function({ commit, state }, loadMore, plugin) {
	commit(FETCHING, true);
	return window.messageService
		.rest(plugin, {
			filter: state.viewMode,
			sort: state.sort,
			limit: 10,
			offset: state.offset
		})
		.then(data => {
			if (data.length == 0) commit(NO_MORE_ITEMS);
			else commit(loadMore ? LOAD_MORE : LOAD, data);
		})
		.catch(err => {
			toastr.error(err.message);
		})
		.then(() => {
			commit(FETCHING, false);
		});
};
export const send = function({ commit, store }, model, plugin) {
	return new Promise((resolve, reject) => {
		window.messageService
			.rest("facebook", model)
			.then(data => {
				commit(ADD, data);
				resolve(data);
			})
			.catch(err => {
				toastr.error(err.message);
				reject(err);
			});
	});
};

export const sendAndSave = function({ commit, store }, model, plugin) {
	 return	window.messageService.rest("sendAndSave", model);
};

export const upload = function({ commit, state }, data) {
	 return window.messageService
	 .rest("upload", data);
};

export const updateFile = function({ commit, state }, data) {
	 return window.messageService
	 .rest("updateFile", data);
};

export const sendToUser = function({ commit, store }, model, plugin) {
	 return	window.messageService.rest("send", model);
};

export const sendAndSaveComment = function({ commit, store }, model, plugin) {
	 return	window.messageService.rest("sendAndSaveComment", model);
};
export const sendMediaMessage = function(
	{ commit, store },
	model,
	plugin
) {
	return window.messageService.rest("sendMediaMessage", model);
};

export const sendMediaComment = function(
	{ commit, store },
	model,
	plugin
) {
	return window.messageService.rest("sendMediaComment", model);
};

export const loadAvatar = function({ commit, store }, params) {
	return window.facebookService.rest("graphAvatar", params);
};

export const searchMessage = function({ commit, store }, params) {
	return window.facebookService.rest("graphMessagesByConversation", params);
};
export const searchComment = function({ commit, store }, params) {
	return window.facebookService.rest("graphPostsByPage", params);
};
export const getPageToken = function({ commit, store }, model) {
	return new Promise((resolve, reject) => {
		window.messageService
			.rest("facebook", model)
			.then(data => {
				if (data.access_token) {
					commit(LOAD_PAGE_TOKEN, data.access_token);
					resolve(data);
				}
			})
			.catch(err => {
				toastr.error(err.message);
				reject(err);
			});
	});
};
export const getConversations = function({ commit, state }, params) {

	return window.facebookService
		.rest("graphConversationsByPage", params);

};

export const getConversationById = function({ commit, state }, params) {
	return window.facebookService.rest("graphConversationsByPage", params);
};

export const getMessages = function({ commit, store }, model) {
	return new Promise((resolve, reject) => {
		window.messageService
			.rest("facebookExt", model)
			.then(data => {
				if (data.conversations != undefined) {
					commit(LOAD, data.messages.data);
				}
				resolve(data);
			})
			.catch(err => {
				toastr.error(err.message);
				reject(err);
			});
	});
};
export const getRows = function({ commit, state }, params) {
	return window.messageService.rest("find", params);
};

export const hideOrUnhideComment = function({ commit, state }, params) {
	return window.messageService.rest("hideOrUnhideComment", params);
};

export const likeOrUnlikeComment = function({ commit, state }, params) {
	return window.messageService.rest("likeOrUnlikeComment", params);
};

export const loadMoreRows = function(context) {
	return getRows(context, true);
};

export const changeSort = function(store, sort) {
	store.commit(CHANGE_SORT, sort);
	getRows(store);
};

export const changeViewMode = function(store, viewMode) {
	store.commit(CHANGE_VIEWMODE, viewMode);
	getRows(store);
};

export const saveRow = function(store, model) {
	window.messageService
		.rest("create", model)
		.then(data => {
			created(store, data);
		})
		.catch(err => {
			toastr.error(err.message);
		});
};

export const updateRow = function(store, model) {
	window.messageService
		.rest("update", model)
		.then(data => {
			updated(store, data);
		})
		.catch(err => {
			toastr.error(err.message);
		});
};
export const updateCon = function(store, model) {
	return window.messageService
		.rest("updateConversation", model);
};
export const removeRow = function(store, model) {
	window.messageService
		.rest("remove", { code: model.code })
		.then(data => {
			removed(store, data);
		})
		.catch(err => {
			toastr.error(err.message);
		});
};

export const vote = function(store, model) {
	window.messageService
		.rest("vote", { code: model.code })
		.then(data => {
			updated(store, data);
		})
		.catch(err => {
			toastr.error(err.message);
		});
};

export const unVote = function(store, model) {
	window.messageService
		.rest("unvote", { code: model.code })
		.then(data => {
			updated(store, data);
		})
		.catch(err => {
			toastr.error(err.message);
		});
};

export const created = function({ commit }, model) {
	commit(ADD, model);
};

export const updated = function({ commit }, model) {
	commit(UPDATE, model);
};

export const removed = function({ commit }, model) {
	commit(REMOVE, model);
};
export const sent = function({ commit }, model) {
	commit(SEND, model);
};
