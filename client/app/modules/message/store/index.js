import {
	LOAD, LOAD_MORE, ADD, SELECT, CLEAR_SELECT, UPDATE, REMOVE,
	CLEAR, NO_MORE_ITEMS, FETCHING,
	CHANGE_SORT, CHANGE_VIEWMODE, SEND, CONTACT_LOAD, CONTACT_LOAD_MORE, CONTACT_ADD, CONTACT_SELECT, CONTACT_CLEAR_SELECT, CONTACT_UPDATE, CONTACT_REMOVE,
	CONTACT_CLEAR, CONTACT_NO_MORE_ITEMS, CONTACT_FETCHING,
	CONTACT_CHANGE_SORT, CONTACT_CHANGE_VIEWMODE, LOAD_PAGE_TOKEN, CLEAR_MESSAGE
} from "./types";

import { each, find, findIndex, assign, remove, isArray, cloneDeep } from "lodash";

const state = {
	ptoken: "",
	conversations:[],
	posts:[],
	paging:{},
	next:"",
	messages:[],
	rows: [],
	offset: 0,
	hasMore: true,
	fetching: false,
	sort: "-updated_date",
	viewMode: "all"
};

const mutations = {
	[LOAD_PAGE_TOKEN] (state, token) {
		 state.ptoken = token;
	},

	[CONTACT_LOAD] (state, models) {
		state.conversations.splice(0);
		state.conversations.push(...models.conversations.data);
		// state.posts.push(...models.posts.data);
		let list = {};
		models.posts.data.forEach(post => {
			if(post.comments) {
				post.comments.data.forEach(comment => {
					if(comment.from) {
						list[post.id+comment.from.id] = cloneDeep(comment);
						list[post.id+comment.from.id].post = post;
					}
					// if(list == "" || (list!= "" &&  list.split(";").indexOf(post.id+comment.id) == -1)) {
						// state.posts.push(comment);
						// list += post.id + comment.id + ";"
					// }
				});
			}
		});
		state.posts = Object.values(list);
		state.offset = state.conversations.length;
		console.log("models.paging", models);
		state.paging = models.paging;
		state.next = models.next;
	},
	[CONTACT_FETCHING] (state, status) {
		state.fetching = status;
	},
	[LOAD] (state, models) {
		state.rows.splice(0);
		state.rows.push(...models);
		state.offset = state.rows.length;
	},

	[LOAD_MORE] (state, models) {
		state.rows.push(...models);
		state.offset = state.rows.length;
	},

	[CONTACT_LOAD_MORE] (state, models){
		//state.conversations.splice(0);
		state.conversations.push(...models.data);
		state.offset = state.conversations.length;
		state.paging = models.paging;
		state.next = models.next;
	},

	[CLEAR] (state) {
		state.offset = 0;
		state.hasMore = true;
		state.conversations.splice(0);
		state.posts.splice(0);
		state.messages.splice(0);
	},

	[CLEAR_MESSAGE] (state) {
		//state.offset = 0;
		//state.hasMore = true;
		//state.conversations.splice(0);
		state.messages.splice(0);
	},

	[FETCHING] (state, status) {
		state.fetching = status;
	},

	[CHANGE_SORT] (state, sort) {
		state.sort = sort;
		mutations[CLEAR](state);
	},

	[CHANGE_VIEWMODE] (state, mode) {
		state.viewMode = mode;
		mutations[CLEAR](state);
	},

	[NO_MORE_ITEMS] (state) {
		state.hasMore = false;
	},

	[CONTACT_NO_MORE_ITEMS] (state){
		state.hasMore = false;
	},
	[ADD] (state, model) {
		let found = find(state.rows, (item) => item.id == model.id);
		if (!found)
			state.rows.unshift(model);
	},

	[UPDATE] (state, model) {
		each(state.rows, (item) => {
			if (item.id == model.id) {
				assign(item, model);
			}
		});
	},

	[REMOVE] (state, model) {
		state.rows = state.rows.filter(item => item.code != model.code);
	},
	[SEND] (state, model) {
		let found = find(state.rows, (item) => item.code == model.code);
		if (!found)
			state.rows.unshift(model);
	}
};

import * as getters from "./getters";
import * as actions from "./actions";

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
