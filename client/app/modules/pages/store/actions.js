import Vue from "vue";
import toastr from "../../../core/toastr";
import Service from "../../../core/service";
import {
	LOAD,
	LOAD_MORE,
	ADD,
	UPDATE,
	VOTE,
	UNVOTE,
	REMOVE,
	NO_MORE_ITEMS,
	FETCHING,
	CHANGE_SORT,
	CHANGE_VIEWMODE,
	CLEAR,
} from "./types";

export const NAMESPACE = "/api/pages";

//var service = new Service("pages", this)//window.pageService;
//var facebookService = new Service("facebook", this)//window.facebookService;

export const query = function ({ commit, state }, fbmodel) {
	window.pageService
		.query(
			gql`
				{
					pages(limit: 5) {
						name
					}
				}
			`
		)
		.then((result) => {
			// console.log("pages", result.pages);
		})
		.catch((err) => {
			console.log(err);
		});
};
export const fb = function ({ commit, state }, fbmodel) {
	return window.pageService.rest("fb", fbmodel);
};
export const fbPages = function ({ commit, state }, params) {
	return window.facebookService.rest("graphPages", params);

	// commit(FETCHING, true);
	// let fbModel = params.fbModel;
	// let loadMore = params.loadMore;

	// if (loadMore && state.paging.cursors.after && fbModel[2])
	// 	fbModel[2].after = state.paging.cursors.after;
	// return window.facebookService
	// 	.rest("graphPages", params)
	// 	.then(pages => {
	// 		console.log(pages);
	// 		if (!pages.paging.next) commit(NO_MORE_ITEMS);
	// 		commit(loadMore ? LOAD_MORE : LOAD, pages);
	// 	})
	// 	.catch(err => {
	// 		toastr.error(err.message);
	// 	})
	// 	.then(() => {
	// 		console.log("finising loading");
	// 		commit(FETCHING, false);
	// 	});

	//commit(FETCHING, true);
	//return service.rest("fbPages" , fbmodel);
};
export const getPageById = function ({ commit, store }, params) {
	return window.facebookService.rest("graphPageById", params);
};

export const activePageById = function ({ commit, store }, params) {
	return window.facebookService.rest("activePage", params);
};
export const resyncPage = function ({ commit, store }, params) {
	return window.facebookService.rest("resyncPage", params);
};

export const reSyncMessage = function ({ commit, store }, params) {
	return window.facebookService.rest("reSyncMessage", params);
};

export const syncPageData = function ({ commit, store }, params) {
	return window.facebookService.rest("syncPageData", params);
};

export const getConversations = function ({ commit, state }, params) {
	return window.facebookService.rest("graphConversationsByPage", params);
};
export const getRows = function ({ commit, state }, params) {
	let loadMore = params ? params.loadMore : false;
	let fbPages = params ? params.fbPages : [];
	commit(FETCHING, true);
	return window.pageService
		.rest("find", {
			filter: state.viewMode,
			condition: { fb_id: { $in: fbPages } },
		})
		.then((data) => {
			//console.log(data)
			if (!data.paging || !data.paging.next) commit(NO_MORE_ITEMS);
			commit(loadMore ? LOAD_MORE : LOAD, data);
			commit(FETCHING, false);
			return Promise.resolve(data);
		})
		.catch((err) => {
			return Promise.reject(data);
			//toastr.error(err.message);
		});
};

export const loadMoreRows = function (context) {
	return getRows(context, true);
};

export const changeSort = function (store, sort) {
	store.commit(CHANGE_SORT, sort);
	getRows(store);
};

export const changeViewMode = function (store, viewMode) {
	store.commit(CHANGE_VIEWMODE, viewMode);
	getRows(store);
};

export const saveRow = function (store, model) {
	window.pageService
		.rest("create", model)
		.then((data) => {
			created(store, data);
		})
		.catch((err) => {
			toastr.error(err.message);
		});
};

export const updateRow = function (store, model) {
	window.pageService
		.rest("update", model)
		.then((data) => {
			updated(store, data);
		})
		.catch((err) => {
			toastr.error(err.message);
		});
};

export const updateRowBy = function (store, model) {
	return window.pageService
		.rest("updateBy", model)
		.then((data) => {
			updated(store, data);
			return Promise.resolve(data);
		})
		.catch((err) => {
			// toastr.error(err.message);
			return Promise.resolve(null);
		});
};

export const removeRow = function (store, model) {
	window.pageService
		.rest("remove", { code: model.code })
		.then((data) => {
			removed(store, data);
		})
		.catch((err) => {
			toastr.error(err.message);
		});
};

export const vote = function (store, model) {
	window.pageService
		.rest("vote", { code: model.code })
		.then((data) => {
			updated(store, data);
		})
		.catch((err) => {
			toastr.error(err.message);
		});
};

export const unVote = function (store, model) {
	window.pageService
		.rest("unvote", { code: model.code })
		.then((data) => {
			updated(store, data);
		})
		.catch((err) => {
			toastr.error(err.message);
		});
};

export const created = function ({ commit }, model) {
	commit(ADD, model);
};

export const updated = function ({ commit }, model) {
	commit(UPDATE, model);
};

export const removed = function ({ commit }, model) {
	commit(REMOVE, model);
};
