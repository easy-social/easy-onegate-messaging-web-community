export function contacts(state) {
	return state.rows;
}
export function users(state) {
	return state.rows;
}
export function selected(state) {
	return state.selected;
}

export function fetching(state) {
	return state.fetching;
}
export function sort(state) {
	return state.sort;
}

export function viewMode(state) {
	return state.viewMode;
}
