import Vue from "vue";
import toastr from "../../../core/toastr";
import Service from "../../../core/service";
import { LOAD, LOAD_MORE, ADD, UPDATE, VOTE, UNVOTE, REMOVE,
	NO_MORE_ITEMS, FETCHING, CHANGE_SORT, CHANGE_VIEWMODE, LOAD_PAGE_TOKEN, POST_LOAD, CLEAR, ADDCOMMENT ,UPDATECOMMENT, REMOVECOMMENT} from "./types";

export const NAMESPACE	 	= "/api/posts";



export const clear = function({commit, store}) {
	commit(CLEAR);
};

export const getPageById = function ({commit, store}, params) {
	return window.facebookService.rest("graphPageById", params);
};
export const getPosts = function ({commit, store}, params) {
	let loadMore = params.loadMore;
	commit(FETCHING, true);
	return window.facebookService.rest("graphFeedsByPage", params).then((data) => {
		console.log("fbPosts", data);
		// if (data.length == 0)
		// 	commit(CONTACT_NO_MORE_ITEMS);
		// else
		// console.log(data)
		// commit(POST_LOAD, data.posts.data);
		if (!data.posts || !data.posts.paging || !data.posts.paging.next)
			commit(NO_MORE_ITEMS);
		commit(loadMore ? LOAD_MORE : POST_LOAD, data.posts);
	}).catch((err) => {
		toastr.error(err.message);
	}).then(() => {
		commit(FETCHING, false);
	});
};

export const getRows = function ({commit, state}, params) {
	let condition = params.condition;
	return window.postService.rest("find", {
		filter: state.viewMode,
		condition,
		sort: state.sort,
		limit: 100,
		offset: state.offset
	});
};

export const loadMoreRows = function(context) {
	return getPosts(context, true);
};

export const changeSort = function(store, sort) {
	store.commit(CHANGE_SORT, sort);
	getRows(store);
};

export const changeViewMode = function(store, viewMode) {
	store.commit(CHANGE_VIEWMODE, viewMode);
	getRows(store);
};

export const saveRow = function(store, {model, token}) {
	if(model.detail){
	    var opts = {
	        message : model.content,
	        // name : '',
	        // link : 'http://www....',
	        // description : 'Description here',
	        url : model.detail,
	        access_token: token
	    };
	    var actionPost = "photos";
	  } else {
	    var opts = {
	        message : model.content,
	        // name : '',
	        // link : 'http://www....',
	        // description : 'Description here',
	        // picture : 'http://domain.com/pic.jpg'
	        access_token: token
	    };
	    var actionPost = "feed";
	  }
	  window.facebookService.rest("/graph", ["/me/"+actionPost, "POST", opts]).then((data) => {
	  	if(data.id){
	  		let postId = data.post_id ? data.post_id : data.id;
	  		window.facebookService.rest("/graph", ["/" + postId, "GET", {access_token: token, fields: "message,full_picture,comments{message,comments,comment_count,from,created_time},created_time,from,permalink_url,shares, likes{can_post,id,link,name,pic,pic_large,pic_crop,pic_small,pic_square,profile_type,username,picture}, attachments"}]).then((addData) => {
	  			created(store, addData);
	  		});
	  	}
	}).catch((err) => {
		toastr.error(err.message);
	});
};

export const updateRow = function(store, model) {
	window.postService.rest("update", model).then((data) => {
		updated(store, data);
	}).catch((err) => {
		toastr.error(err.message);
	});
};

export const removeRow = function(store, model) {
	window.postService.rest("remove", { code: model.code }).then((data) => {
		removed(store, data);
	}).catch((err) => {
		toastr.error(err.message);
	});
};

export const saveComment = function(store, {model, token, item}) {
	if(model.detail){
	    var opts = {
	        message : model.content,
	        // name : '',
	        // link : 'http://www....',
	        // description : 'Description here',
	        url : model.detail,
	        access_token: token
	    };
	    // var actionPost = "photos";
	  } else {
	    var opts = {
	        message : model.content,
	        // name : '',
	        // link : 'http://www....',
	        // description : 'Description here',
	        // picture : 'http://domain.com/pic.jpg'
	        access_token: token
	    };
	    // var actionPost = "feed";
	  }
	  window.facebookService.rest("/graph", ["/" + item.id + "/comments", "POST", opts]).then((data) => {
	  	if(data.id){
	  		let postId = data.post_id ? data.post_id : data.id;
	  		facebook.rest("/graph", ["/" + postId, "GET", {access_token: token, fields: "message,comments,comment_count,from,created_time"}]).then((addData) => {
	  			// model.content = undefined
	  			createdComment(store, {data: addData, item: item});
	  			// created(store, addData);
	  		});
	  	}
	}).catch((err) => {
		toastr.error(err.message);
	});
};

export const editComment = function(store, {model, token, item, parent, postId}) {
	if(item.attachment && item.attachment.type!= "share"){
	    var opts = {
	        message : model.content,
	        // name : '',
	        // link : 'http://www....',
	        // description : 'Description here',
	        attachment_id : item.attachment.target.id,
	        access_token: token
	    };
	    // var actionPost = "photos";
	  } else {
	    var opts = {
	        message : model.content,
	        // name : '',
	        // link : 'http://www....',
	        // description : 'Description here',
	        // picture : 'http://domain.com/pic.jpg'
	        access_token: token
	    };
	    // var actionPost = "feed";
	  }
	  window.facebookService.rest("/graph", ["/" + item.id, "POST", opts]).then((data) => {
	  	if(data.success){
	  		// var postId = data.post_id ? data.post_id : data.id;
	  		// facebook.rest("/graph", ['/' + postId, 'GET', {access_token: token, fields: "message,comments,comment_count,from,created_time"}]).then((addData) => {
	  			// model.content = undefined
	  			// createdComment(store, {data: addData, item: item});
	  			delete item.message_tags;
	  			updateComment(store, {data: model, item: item,  parent: parent, postId: postId});
	  			// created(store, addData);
	  		// })
	  	}
	}).catch((err) => {
		toastr.error(err.message);
	});
};

export const deleteComment = function(store, {model, token, parent, postId}) {

	window.facebookService.rest("/graph", ["/" + model.id, "DELETE", {access_token: token}]).then((data) => {
	  	if(data.success){
	  		// var postId = data.post_id ? data.post_id : data.id;
	  		// facebook.rest("/graph", ['/' + postId, 'GET', {access_token: token, fields: "message,comments,comment_count,from,created_time"}]).then((addData) => {
	  			// console.log(data)
	  			// model.content = undefined
	  			// createdComment(store, {data: addData, item: item});
	  			removeComment(store, {data: model, parent: parent, postId: postId});
	  			// created(store, addData);
	  		// })
	  	}
	}).catch((err) => {
		toastr.error(err.message);
	});
};

export const vote = function(store, model) {
	window.postService.rest("vote", { code: model.code }).then((data) => {
		updated(store, data);
	}).catch((err) => {
		toastr.error(err.message);
	});
};

export const unVote = function(store, model) {
	window.postService.rest("unvote", { code: model.code }).then((data) => {
		updated(store, data);
	}).catch((err) => {
		toastr.error(err.message);
	});
};

export const created = function({ commit }, model) {
	commit(ADD, model);
};

export const updated = function({ commit }, model) {
	commit(UPDATE, model);
};

export const removed = function({ commit }, model) {
	commit(REMOVE, model);
};

export const createdComment = function({ commit }, model) {
	commit(ADDCOMMENT, model);
};

export const updateComment = function({ commit }, model) {
	commit(UPDATECOMMENT, model);
};

export const removeComment = function({ commit }, model) {
	commit(REMOVECOMMENT, model);
};
