import { 
	LOAD, LOAD_MORE, ADD, SELECT, CLEAR_SELECT, UPDATE, REMOVE, 
	CLEAR, NO_MORE_ITEMS, FETCHING,
	CHANGE_SORT, CHANGE_VIEWMODE , LOAD_PAGE_TOKEN , POST_LOAD , ADDCOMMENT, UPDATECOMMENT, REMOVECOMMENT
} from "./types";

import { each, find, assign, remove, isArray, forOwn } from "lodash";

const state = {
	rows: [],
	offset: 0,
	posts:[],
	hasMore: true,
	fetching: false,
	sort: "-createdAt",
	viewMode: "all",
	paging: {},
	commentText: {},
	CommentAble: {},
	ReplyAble: {},
	EditAble: {},
	updateText: {},
};

const mutations = {
	// [LOAD_PAGE_TOKEN] (state, token) {
	// 	console.log(token)
	// 	 state.ptoken = token
	// },
	[POST_LOAD] (state, models) {
		console.log("models",models);
		state.posts.splice(0);
		state.posts.push(...models.data);
		state.paging = models.paging;
		state.offset = state.posts.length;
	},
	[LOAD] (state, models) {
		state.rows.splice(0);
		state.rows.push(...models);
		state.offset = state.rows.length;
	},

	[LOAD_MORE] (state, models) {
		state.posts.push(...models.data);
		state.offset = state.posts.length;
		state.paging = models.paging;
	},

	[CLEAR] (state) {
		state.posts = [];
		state.offset = 0;
		state.hasMore = true;
	},

	[FETCHING] (state, status) {
		state.fetching = status;
	},	

	[CHANGE_SORT] (state, sort) {
		state.sort = sort;
		mutations[CLEAR](state);
	},

	[CHANGE_VIEWMODE] (state, mode) {
		state.viewMode = mode;
		mutations[CLEAR](state);
	},

	[NO_MORE_ITEMS] (state) {
		state.hasMore = false;
	},

	[ADD] (state, model) {
		let found = find(state.posts, (item) => item.id == model.id);
		if (!found)
			state.posts.unshift(model);
	},

	[UPDATE] (state, model) {
		each(state.rows, (item) => {
			if (item.code == model.code) {
				assign(item, model);
			}
		});
	},

	[REMOVE] (state, model) {
		state.rows = state.rows.filter(item => item.code != model.code);
	},

	[ADDCOMMENT] (state, model) {
		console.log(model);
		let found = find(state.posts, (item) => item.id == model.item.id);
		// if(model.item.parent) {
		// 	let parentfound = find(state.posts, (item) => item.id == model.item.parent.id);
		// }
		if (found){
			console.log(found);
			if(!found.comments) {
				found.comments = {data: [], paging: {}};
			}
			found.comments.data.push(model.data);
			delete state.commentText[found.id];
			state.CommentAble[found.id] = false;
			console.log(state.commentText);
			// state.posts.unshift(model);

		} else {
			state.posts.forEach( post=>{
				if(post.comments) {
					let parent = find(post.comments.data, (item) => item.id == model.item.id);
					if (parent){
						if(!parent.comments) {
							parent.comments = {data: [], paging: {}};
						}
						parent.comments.data.push(model.data);
						delete state.commentText[parent.id];
						state.ReplyAble[parent.id] = false;
						console.log(state.commentText);
					}
				}
			});
		}
	},

	[UPDATECOMMENT] (state, model) {
		let found = find(state.posts, (item) => item.id == model.parent);
		let parentfound = undefined;
		if(model.postId) {
			parentfound = find(state.posts, (item) => item.id == model.postId);
		}
		if (found){
			// if(!found.comments) {
			// 	found.comments = {data: [], paging: {}}
			// }
			found = model.item;
			// forOwn(found.comments.data, (value, key) =>{
			// 	if(model.data.id === value.id){
			// 		found.comments.data[key] = model.item;
			// 	}
			// })
			delete state.updateText[found.id];
			state.EditAble[found.id] = false;
			// console.log(state.updateText)
			// state.posts.unshift(model);

		} else if(model.postId && parentfound){
			// if(!found.comments) {
			// 	found.comments = {data: [], paging: {}}
			// }
			parentfound = model.item;
			// each(parentfound.comments.data, (val) =>{
			// 	if(val.comments){
			// 		forOwn(val.comments.data, (value, key) =>{
			// 			if(model.data.id === value.id){
			// 				val.comments.data[key] = model.item;
			// 			}
			// 		})
			// 	}
			// })
			delete state.updateText[parentfound.id];
			state.EditAble[parentfound.id] = false;
		}
	},

	[REMOVECOMMENT] (state, model) {
		let found = find(state.posts, (item) => item.id == model.parent);
		let parentfound = undefined;
		if(model.postId) {
			parentfound = find(state.posts, (item) => item.id == model.postId);
		}
		if (found){
			forOwn(found.comments.data, (value, key) =>{
				if(model.data.id === value.id){
					found.comments.data.splice(key, 1);
				}
			});

		} else if(model.postId && parentfound){
			each(parentfound.comments.data, (val) =>{
				if(val.comments){
					forOwn(val.comments.data, (value, key) =>{
						if(model.data.id === value.id){
							val.comments.data.splice(key, 1);
						}
					});
				}
			});
		}
	},		
};

import * as getters from "./getters";
import * as actions from "./actions";

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};