module.exports = {
	
	contactTypes: [
		{ id: "account", name: "Account" },
		{ id: "conversation", name: "Conversation" },
		{ id: "post", name: "Post" },
		{ id: "message", name: "Message" },
	]
};