"use strict";

import Vue from "vue";
import VueRouter from "vue-router";

import Home from "../modules/home";
import Contacts from "../modules/contacts";
import FSetting from "../modules/fsetting";
import Counter from "../modules/counter";
import Devices from "../modules/devices";
import Posts from "../modules/posts";
import Conversations from "../modules/conversations";
import Profile from "../modules/profile";
import Facebook from "../modules/facebook";
import Chat from "../modules/message";
import Page from "../modules/pages";
import Setting from "../modules/settings";
import Users from "../modules/users";
import Statistics from "../modules/statistics";
import StatisticsOverview from "../modules/statistics/StatisticsOverview.vue";
import StatisticsPage from "../modules/statistics/StatisticsPage.vue";
import StaffStatistics from "../modules/statistics/StaffStatistics";
import InteractionStatistics from "../modules/statistics/InteractionStatistics";
import TagStatistic from "../modules/statistics/TagStatistic.vue";
import Config from "../modules/configurations";
Vue.use(VueRouter);

export default new VueRouter({
	mode: "hash",
	routes: [
		// { path: "/", component: Page },
		{ path: "/", component: Home },
		{ path: "/devices", component: Devices },
		{ path: "/posts/:item?", component: Posts },
		{ path: "/counter", component: Counter },
		{ path: "/profile", component: Profile },
		{ path: "/facebook", component: Facebook },
		{ path: "/contacts", component: Contacts },
		{ path: "/fsetting", component: FSetting },
		{ path: "/conversations/:id?", component: Conversations },
		{ path: "/chat/:item?/:cid?", component: Chat},
		{ path: "/pages", component: Page },
		{ path: "/settings", component: Setting },
		{ path: "/users", component: Users },
		{
			path: "/statistics/:item",
			component: Statistics,
			redirect: "/statistics/:item/overview",
			children: [
				{
					path: "overview",
					component: StatisticsOverview
				},
				{
					path: "page",
					component: StatisticsPage
				},
				{
					path: "staff",
					component: StaffStatistics
				},
				{
					path: "interaction",
					component: InteractionStatistics
				},
				{
					path: "tags",
					component: TagStatistic
				}
			]
		},
		{ path: "/configurations/:item?/:tid?", component: Config }
		// { path: "/users", component: User, meta: { needRole: "admin" } },
		//{ path: "*", component: NotFound }
	]
});
