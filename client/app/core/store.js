import Vue from "vue";
import Vuex from "vuex";

import session from "../modules/session/store";
import devices from "../modules/devices/store";
import contacts from "../modules/contacts/store";
import fsetting from "../modules/fsetting/store";
import posts from "../modules/posts/store";
import conversations from "../modules/conversations/store";
import counter from "../modules/counter/store";
import profile from "../modules/profile/store";
import messages from "../modules/message/store";
import pages from "../modules/pages/store";
import settings from "../modules/settings/store";
import users from "../modules/users/store";
import configurations from "../modules/configurations/store";
import folders from "../modules/folders/store";
import crm from "../modules/crm/store";
import statistics from "../modules/statistics/store";

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		session,
		counter,
		devices,
		contacts,
		fsetting,
		posts,
		conversations,
		profile,
		messages,
		pages,
		settings,
		users,
		configurations,
		folders,
		crm,
		statistics
	}
});
